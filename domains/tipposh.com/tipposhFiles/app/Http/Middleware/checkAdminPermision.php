<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Route;

class checkAdminPermision
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $uri = $request->getRequestUri();
        $explode = explode('/', $uri);
        $action = $request->route()->getActionMethod();

        if (isset($explode[2]))
            $permission = $explode[2] . '_' . $action;
        else
            $permission = 'dashboard_links';

        $admin = Auth::guard('admin')->user();

        if ($admin->checkAdminPermision($permission) == false)
            return redirect()->back();

        return $next($request);
    }
}
