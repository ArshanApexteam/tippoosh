<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Order::orderBy('id', 'desc')->paginate(10);
        return view('backend.pages.order.all', compact('all'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Order::find($id)->update(['status' => 1]);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Order::find($id)->update(['status' => 1]);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Order::find($id);
        if (file_exists($item->photo))
            unlink($item->photo);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');

    }

    /*********************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * **********************************************     Front
     */

    public function order($product_id)
    {
        return view('frontend.pages.order' , compact('product_id'));
    }
    public function orderSave(Request $request)
    {
        $request->validate(
            [
                'photo' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);


        if ($request->hasFile('photo')) {
            $fileType = $request->photo->getClientOriginalExtension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/order/$rond.$fileType";
            $image = Image::make($request->file('photo'));
            $image->save($url , 100);


            $user_id = 0;
            if (Auth::guard('web')->user())
                $user_id = Auth::guard('web')->user()->id;

            if($url){
                Order::create([
                    'user_id' =>$user_id,
                    'cookie_key' =>$this::userOrderCookieKey(),
                    'product_id' => $request->product_id,
                    'photo' => $url,
                    'status' => 0,
                ]);
            }

            return redirect()->back()->with('prossesOk', 'سفارش شما با موفقیت انجام شد در اسرع وقت با شما تماس خواهیم گرفت');
        }

        return redirect()->back();
    }


    //######################################################################################      userOrderCookieKey
    function userOrderCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userOrderCookieKey']) && $_COOKIE['userOrderCookieKey'] != null)
            $key = json_decode($_COOKIE['userOrderCookieKey'], true);
        else
            setcookie('userOrderCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }

        
}
