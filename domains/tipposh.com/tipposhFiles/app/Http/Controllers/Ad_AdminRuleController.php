<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Ad_Admin_Rule;
use App\Models\Ad_Permissios;
use App\Models\Admin;

class Ad_AdminRuleController extends Controller
{

    public function __construct()
    {
        //$this->middleware('adminAuth')->except('show');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Ad_Admin_Rule::paginate(20);
        return view('backend.pages.admin_rule.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actions = Ad_Permissios::all();
        $admins = Admin::all();
        return view('backend.pages.admin_rule.new' , compact('actions' ,'admins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'admin_id'        => 'required',
                'rule'            => 'required',
            ],
            [
                'admin_id.required' => 'فیلد مدیر است' ,
                'rule.required' => 'فیلد مجوز است' ,
            ]
        );


        foreach($request->rule as $rule){
            Ad_Admin_Rule::create([
                'admin_id'    => $request->admin_id,
                'action'      => $rule,
            ]);
        }

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actions = Ad_Permissios::all();
        $admins = Admin::all();
        $admin_rule = Ad_Admin_Rule::find($id);

        return view('backend.pages.admin_rule.edit', compact('admin_rule' , 'actions' ,'admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'admin_id'        => 'required',
                'rule'            => 'required',
            ],
            [
                'admin_id.required' =>  'فیلد مدیر است' ,
                'rule.required'     =>  'فیلد مجوز است' ,
            ]
        );

        Ad_Admin_Rule::find($id)->update([
            'admin_id'    => $request->admin_id,
            'action'      => $request->rule,
        ]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ad_Admin_Rule::destroy($id);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }
}
