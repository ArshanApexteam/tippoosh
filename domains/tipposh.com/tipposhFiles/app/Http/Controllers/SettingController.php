<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Image;

use App\Models\Setting;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Setting::all();
        return view('backend.pages.settings.all', compact('all'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.pages.settings.single');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.settings.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Setting::create(['key' => $request->key])->first()) {
            $setting = Setting::create(['key' => $request->key , 'type' => $request->type]);

            if ($request->type == "photo") {
                if ($request->hasFile('photo')) {
                    $fileType = $request->photo->extension();
                    $rondName = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 10);
                    $imgFullName = "assets/images/$rondName.$fileType";
                    $image = Image::make($request->file('photo'));
                    $image->save($imgFullName, 100);

                    $setting->update(['value' => $imgFullName]);
                }
            } elseif ($request->type == "des")
                $setting->update(['value' => $request->description]);
            else
                $setting->update(['value' => $request->summary]);
        }

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::find($id);
        return view('backend.pages.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Setting::find($id)->update(['key' => $request->key , 'type' => $request->type]);
        if ($request->type == "photo") {
            if ($request->hasFile('photo')) {
                $fileType = $request->photo->extension();
                $rondName = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 10);
                $imgFullName = "assets/images/$rondName.$fileType";
                $image = Image::make($request->file('photo'));
                $image->save($imgFullName, 100);

                Setting::find($id)->update(['value' => $imgFullName]);
            }
        } elseif ($request->type == "des")
            Setting::find($id)->update(['value' => $request->description]);
        else
            Setting::find($id)->update(['value' => $request->summary]);


        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Setting::find($id);
        if (file_exists($item->value))
            unlink($item->value);

        $delete = $item->delete();
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');

    }

}
