<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Auth;
use Image;
use Hash;

use App\Models\Basket;
use App\Models\Product;
use App\Models\ProductDiscount;
use App\Models\User;

class BasketController extends Controller
{

    //######################################################################################      userCookieKey
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


    //######################################################################################      basket page
    public function index(Request $request)
    {
        $userCookieKey = $this::userCookieKey();

        $allProduct = Basket::with(['product' => function ($query) {
            $query->where('status', '1')->with('gallery');
        }])->where('cookie_key', $userCookieKey)->get();


        if (Auth::guard('web')->user()) {
            $allProduct = Basket::with(['product' => function ($query) {
                $query->where('status', '1');
            }])->where('cookie_key', $userCookieKey)
                ->orWhere('user_id', Auth::guard('web')->user()->id)->get();
        }


        return view('frontend.pages.checkout.basket', compact('allProduct'));
    }


    //######################################################################################      checkout
    public function checkout(Request $request)
    {
        return view('frontend.pages.checkout.checkout');
    }

    //######################################################################################      all
    public function all(Request $request)
    {
        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        $user_id = 0;
        if (Auth::guard('web')->user())
            $user_id = Auth::guard('web')->user()->id;

        //$allProduct = Basket::where('cookie_key', $userCookieKey)->orWhere('user_id', $user_id)->get()->count();
        $allProduct = Basket::where('cookie_key', $userCookieKey)->get()->count();

        return [
            'status' => 1,
            'count' => $allProduct,
        ];
    }


    //######################################################################################      add
    public function add(Request $request)
    {


        $product = Product::findOrFail($request->product_id);
        if ($product->store == 0)
            return array('status' => 0, 'error' => ' محصول به پایان رسیده است.');

        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        //  اگر کاربر احرازهویت شده باشد شناسه آن را بر میگرداند
        $user_id = 0;
        if (Auth::guard('web')->user())
            $user_id = Auth::guard('web')->user()->id;


        //   ذخیره شناسه کاربر بعد از احرازهویت در سبد خرید
        if (Basket::where('cookie_key', $userCookieKey)->where('user_id', 0)->first() && $user_id != 0)
            Basket::where('cookie_key', $userCookieKey)->update(['user_id' => $user_id]);


        //   اضافه کردن محصول به سبد خرید
        $userBasket = Basket::where('cookie_key', $userCookieKey)->where('product_id', $request->product_id)->first();

        $count = 1 ;
        if($request->count)
            $count = $request->count;

        if ($userBasket) {
            $userBasket = Basket::where('cookie_key', $userCookieKey)
                ->where('product_id', $request->product_id)
                ->first()->update([
                    'count' =>  $request->count,
                    'color' => $request->color,
                    'size' => $request->size,

                ]);
        } else {
            Basket::create([
                'cookie_key' => $userCookieKey,
                'user_id' => $user_id,
                'product_id' => $request->product_id,
                'count' => $count,
                'size' => $request->size,
                'color' => $request->color,
            ]);
        }

        return redirect()->route('basket.index');
        dd($product);
        // $allProduct = Basket::where('cookie_key', $userCookieKey)->orWhere('user_id', $user_id)->get()->count();

        return [
            'all' => $allProduct,
            'status' => 1,
            'msg' => "محصول با موفقیت به سبد شما اضافه گردید"
        ];
    }


    //######################################################################################      addOrRemove
    public function addOrRemove(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        if ($product->store == 0)
            return array('status' => 0, 'error' => ' محصول به پایان رسیده است.');

        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        //  اگر کاربر احرازهویت شده باشد شناسه آن را بر میگرداند
        $user_id = 0;
        if (Auth::guard('web')->user())
            $user_id = Auth::guard('web')->user()->id;


        //   ذخیره شناسه کاربر بعد از احرازهویت در سبد خرید
        if (Basket::where('cookie_key', $userCookieKey)->where('user_id', 0)->first() && $user_id != 0)
            Basket::where('cookie_key', $userCookieKey)->update(['user_id' => $user_id]);


        //   اضافه کردن محصول به سبد خرید

        Basket::create([
            'cookie_key' => $userCookieKey,
            'user_id' => $user_id,
            'product_id' => $request->product_id,
            'count' => 1,
        ]);

        $allProduct = Basket::where('cookie_key', $userCookieKey)->orWhere('user_id', $user_id)->get()->count();

        return ['status' => 1, 'all' => $allProduct];

    }


    //######################################################################################      سفارشی سازی محصول
    public function order($product_id)
    {
        return view('frontend.pages.order' , compact('product_id'));
    }

    //######################################################################################      orderSave
    public function orderSave(Request $request)
    {
        $request->validate(
            [
                'photo' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);


        if ($request->hasFile('photo')) {
            $fileType = $request->photo->getClientOriginalExtension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/order/$rond.$fileType";
            $image = Image::make($request->file('photo'));
            $image->save($url , 100);


            $user_id = 0;
            if (Auth::guard('web')->user())
                $user_id = Auth::guard('web')->user()->id;

            if($url){

                Basket::create([
                    'cookie_key' => $this::userCookieKey(),
                    'user_id' => $user_id,
                    'product_id' => $request->product_id,
                    'photo' => $url,
                    'count' => 1,
                    'size' => null,
                    'color' => null,
                ]);
            }

            return redirect()->route('basket.index');
        }

        return redirect()->back()->with('prossesError', 'لطفا دوباره تلاش کنید');
    }


    //######################################################################################      remove
    public function remove(Request $request)
    {
        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        //   اضافه کردن محصول به سبد خرید
        Basket::destroy($request->basket_id);

        return [
            'status' => 1,
            'msg' => "محصول با موفقیت از سبد شما حذف گردید"
        ];
    }

    //######################################################################################      update
    public function update(Request $request)
    {
        Basket::find($request->id)->update(['count' => $request->value]);
        return [
            'status' => 1,
            'msg' => "سبد با موفقیت بروزرسانی گردید",
        ];
    }

    //######################################################################################      addDiscountCode
    public function addDiscountCode(Request $request)
    {
        if(ProductDiscount::whereRaw("BINARY `code`= ?",[$request->code])->where('status' , 0)->first())
        {
            Basket::find($request->id)->update(['discount_code' => $request->code]);

            ProductDiscount::where('code' , $request->code)->update([
                'status' => 1,
                'product_id' => $request->id ,
            ]);

            return [
                'status' => 1,
                'msg' => "کد تخفیف با موفقیت اعمال شد",
            ];

        }

        return [
            'status' => 2,
            'msg' => "کد وارد شده نامعتبر می باشد",
        ];
    }


    //######################################################################################      factor
    public function factor(Request $request)
    {
        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        $data['basket'] = Basket::with(['product' => function ($query) {
            $query->where('status', '1');
        }])->where('cookie_key', $userCookieKey)->get();

        if(count($data['basket']) < 1)
            return redirect()->back();

        if (Auth::guard('web')->user()) {
            $data['basket'] = Basket::with(['product' => function ($query) {
                $query->where('status', '1');
            }])->where('cookie_key', $userCookieKey)
                ->orWhere('user_id', Auth::guard('web')->user()->id)->get();
        }

        $data['salePrice'] = 0;
        $data['offerPrice'] = 0;
        $data['totalPrice'] = 0;
        $data['discount'] = 0;
        foreach ($data['basket'] as $item) {
            $data['salePrice'] += $item->count * $item->product->price;
            $data['offerPrice'] += $item->count * ($item->product->price * $item->product->offer / 100);
            if(ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first())
                $data['discount'] += ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first()->price;
        }

        $data['totalPrice'] = $data['salePrice'] - $data['offerPrice'] - $data['discount'] ;

        return view('frontend.pages.checkout.factor', compact('data'));
    }

    //######################################################################################      updateBasketAddress
    public function updateBasketAddress(Request $request)
    {
        $userCookieKey = $this::userCookieKey();

        if (User::where('phone', $request->phone)->first())
            $user = User::where('phone', $request->phone)->first();
        else
            $user = User::create(['phone' => $request->phone]);


        Basket::where('cookie_key', $userCookieKey)->update(['address' => $request->address , 'user_id'=>$user->id ,'phone' =>  $request->phone ]);

        return [
            'status' => 1,
            'msg' => "اطلاعات شما با موفقیت دریافت گردید",
        ];
    }

    //######################################################################################      factorCompare
    public function factorCompare(Request $request)
    {
        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = $this::userCookieKey();

        $basket = Basket::with(['product' => function ($query) {
            $query->where('status', '1');
        }])->where('cookie_key', $userCookieKey)->get();

        $salePrice = 0;
        $offerPrice = 0;
        $totalPrice = 0;
        $discount = 0;
        foreach ($basket as $item) {
            $salePrice += $item->count * $item->product->price;
            $offerPrice += $item->count * ($item->product->price * $item->product->offer / 100);
            if(ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first())
                $discount += ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first()->price;
        }

        $totalPrice = $salePrice - $offerPrice - $discount;


        return [
            'status' => 1,
            'salePrice' => $salePrice,
            'offerPrice' => $offerPrice,
            'totalPrice' => $totalPrice,
            'discount' => $discount,
        ];
    }
}
