<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\UsersVerify;
use App\Models\ServiceRequests;
use App\Notifications\User\Auth\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use Image;
use DB;

use App\Models\User;
use App\Models\Product;
use App\Models\Sale;
use App\Models\ProductComment;
use App\Models\PasswordResets;

class UserController extends Controller
{

    static function SMS($number, $code)
    {
        $username = "tipposh";
        $password = 'twohost8608';
        $from = "+98100020400";
        $pattern_code = "arg0a8txgu";
        $to = array($number);
        $input_data = array("code" => $code);
        $url = "https://ippanel.com/patterns/pattern?username=" . $username . "&password=" . urlencode($password) . "&from=$from&to=" . json_encode($to) . "&input_data=" . urlencode(json_encode($input_data)) . "&pattern_code=$pattern_code";
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handler, CURLOPT_POSTFIELDS, $input_data);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handler);
        $javab = $response;
    }

    //######################################################################################      login
    public function loginTakePhone(Request $request)
    {
        if (strlen($request->phone) != 11) {
            session()->put('userErrorInPhone', 1);
            return redirect()->route('index')->with("prossesError", "طول شماره تلفن نباید بیشتر یا کمتر از 11 باشد ");
        }

        $code = rand(100000, 999999);
        $verify = UsersVerify::create([
            'code' => $code,
            'phone' => $request->phone
        ]);

        $this::SMS($request->phone, $code);
        session()->put('UsersVerifyId', $verify->id);
        session()->put('userRequestLogin', 1);
        return redirect()->route('index');

    }

    //######################################################################################      login with username
    public function loginWithUsername(Request $request)
    {
        $request->validate(
            [
                'username' => ['required'],
                'password' => ['required'],
            ]
        );

        $username = Request()->username;
        $password = Request()->password;
        $remember = 1;
        if (Auth::guard('web')->attempt(['username' => $username, 'password' => $password], $remember))
            return redirect()->route('user.profile');
        else
        {
            session()->put('loginWithUsername', 1);
            return redirect()->back()->with('prossesError', 'نام کاربری یا رمزعبور اشتباه است');
        }
           
    }

    //######################################################################################      authentication
    public function loginVerifyCode(Request $request)
    {
        if (!(session()->get('UsersVerifyId'))) {
            session()->put('userErrorInPhone', 1);
            return redirect()->route('index')->with("prossesError", "دوباره اقدام به ارسال شماره خود فرمایید");
        }

        $verify = UsersVerify::find(session()->get('UsersVerifyId'));
        if ($verify->code != $request->code) {
            session()->put('userRequestLogin', 1);
            return redirect()->back()->with('prossesError', "کد ارسال شده اشتباه می باشد.");
        }

        if (User::where('phone', $verify->phone)->first())
            $user = User::where('phone', $verify->phone)->first();
        else
            $user = User::create(['phone' => $verify->phone]);

        Auth::guard('web')->login($user, true);

        $this::setCookieTooID();

        UsersVerify::destroy(session()->get('UsersVerifyId'));

        session()->forget('UsersVerifyId');

        return redirect()->route('user.profile');
    }


    //######################################################################################      password
    public function password()
    {
        return view('frontend.pages.user.password');
    }


    //#####################################################################################       passwordUpdate
    public function passwordUpdate(Request $request)
    {
        $request->validate(
            [
                'newPassword' => ['required'],
                'rePassword' => ['required'],
            ], [
                'newPassword.required' => "رمز عبور جدید نمیتواند خالی باشد",
                'rePassword.required' => "تکرار رمز عبور نمیتواند خالی باشد",
            ]
        );

        $user = Auth::guard('web')->user();

        $security = $this::securityInput($request->all());
        if ($security == false)
            return redirect()->back()->with('securityError', '1');

        if ($request->rePassword == $request->newPassword) {
            $user->password = Hash::make($request->newPassword);
            $user->save();
            return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
        }

        return redirect()->back()->with('prossesError', 'تکرار رمزعبور اشتباه است');
    }



    //######################################################################################      logout
    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('index');
    }


    //######################################################################################      Profile
    public function profile()
    {
        $user = Auth::guard('web')->user();
        return view('frontend.pages.user.profile', compact('user'));
    }


    //#####################################################################################      profileUpdate
    public function profileUpdate(Request $request)
    {
        if($request->username){
            $userTb = User::where('username' , $request->username)->first();
            if ($userTb){
                if ($userTb->id == Auth::guard('web')->user()->id) {
                    Auth::guard('web')->user()->update(['username' => $request->username]);

                }else{
                    return redirect()->back()->with('prossesError', "نام کاربری وارد شده متعلق به فرد دیگری می باشد . لطفا از نام کاربری دیگری استفاده کنید.");
                }
            }
            else
                Auth::guard('web')->user()->update(['username' => $request->username]);
        }



        $security = $this::securityInput($request->all());
        if ($security == false)
            return redirect()->back()->with('securityError', '1');

        $user = Auth::guard('web')->user();

        if ($request->hasFile('imageProfile')) {
            $rond = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 20);
            $imageName = $rond . '.' . $request->imageProfile->extension();
            $photoUrl = "assets/images/user/users/$imageName";
            $image = Image::make($request->file('imageProfile'));
            $image->resize(150, 150)->save($photoUrl, 100);

            $user->imageProfile = $photoUrl;
        }


        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->save();

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }



    //#####################################################################################       like
    public function myLike()
    {
        $products = [];
        if (isset($_COOKIE['userProductLikes']) && $_COOKIE['userProductLikes'] != null) {
            $products = json_decode($_COOKIE['userProductLikes'], true);
        }

        $allProduct = Product::whereIn('id', $products)->get();

        return view('frontend.pages.user.likes', compact('allProduct'));
    }


    //#####################################################################################       basket
    public function basket()
    {
        $allProduct = Basket::where('user_id', Auth::guard('web')->user()->id)->get();
        return view('frontend.pages.user.basket', compact('allProduct'));
    }

    //#####################################################################################       sale
    public function sale()
    {
        $allProduct = Sale::where('user_id', Auth::guard('web')->user()->id)->get();
        return view('frontend.pages.user.sale', compact('allProduct'));
    }

    //#####################################################################################       requests
    public function requests()
    {
        $allService = ServiceRequests::where('user_id', Auth::guard('web')->user()->id)->get();
        return view('frontend.pages.user.requests', compact('allService'));
    }

    //#####################################################################################       comment
    public function comments()
    {
        $allComments = ProductComment::where('user_id', Auth::user()->id)->get();
        return view('frontend.pages.user.comments', compact('allComments'));
    }

    public function removeComments(Request $request)
    {
        ProductComment::destroy($request->comment_id);
        return ([
            'status' => 1,
            'msg' => "نظر شما با موفقیت حذف گردید"
        ]);
    }


    //#####################################################################################       commentDelete
    public function commentDelete(Request $request)
    {
        ProductComment::destroy($request->comment_id);
        return ['status' => 1, 'msg' => 'نظر با موفقیت حذف گردید'];
    }


    //#####################################################################################         script Security
    public function securityInput($arr)
    {
        foreach ($arr as $item) {
            $check = explode("script", $item);
            if (isset($check[1]))
                return false;
        }
        return true;
    }

    //#####################################################################################       userCookieKey
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }

    //######################################################################################      setCookieTooID
    public function setCookieTooID()
    {
        $userCookieKey = $this::userCookieKey();

        Basket::where('cookie_key', $userCookieKey)->update(['user_id' => Auth::guard('web')->user()->id]);
        ProductComment::where('cookie_key', $userCookieKey)->update(['user_id' => Auth::guard('web')->user()->id]);
    }
    
    
    
    //######################################################################################################################
    ########################################################################################################################
    ####################################################   Backend

    public function index()
    {
        $all = User::orderBy('id', 'desc')->paginate(80);
        return view('backend.pages.user.all', compact('all'));
    }
    public function edit($id)
    {
        $user = User::find($id);
        return view('backend.pages.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'phone' => ['min:11', 'max:11'],
            ]);

        $user = User::find($id);

        $userAddress = $user->address;

        if(is_array($userAddress) || $userAddress !== 'b:0;'){
            $userAddress = unserialize($userAddress);
        }

        $userAddress[] = $request->address;
        $address = serialize($userAddress);



        $user->name = Request()->name;
        $user->phone = Request()->phone;
        $user->address = $address;

        if (isset($request->password))
            $user->password = Hash::make(Request()->password);

        $user->save();


        if ($request->hasFile('file')) {
            $fileType = $request->file->extension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/user/users/$rond.$fileType";
            $image = Image::make($request->file('file'));
            $image->resize(100, 100)->save($url, 100);
            User::find($id)->update(['imageProfile' => $url]);
        }

        return redirect()->back()->with('prossesOk', 'کاربر با موفقیت ویرایش گردید');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id);
        return redirect()->back()->with('prossesOk', 'کاربر با موفقیت   حذف گردید');
    }

}
