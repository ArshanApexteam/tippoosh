<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\Skills;
use App\Models\Blog;
use App\Models\BlogComment;
use App\Models\BlogImages;
use App\Models\BlogTag;
use App\Models\Category;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Blog::orderBy('id', 'desc')->paginate(10);
        return view('backend.pages.blog.all', compact('all'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.pages.blog.single');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategory = Category::where('parent_id' , 0)->get();
        return view('backend.pages.blog.new', compact('allCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'               => 'required',
                'description'         => 'required',
                'main_photo'          => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'index_photo'         => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

        $mainPhotoUrl = $indexPhotoUrl = "";
        if ($request->hasFile('index_photo')) {
            $fileType = $request->index_photo->extension();
            $rond = random_int(10000000000, 99999999999);
            $indexPhotoUrl = "assets/images/blog/$rond.$fileType";
            $image = Image::make($request->file('index_photo'));
            $image->resize(270, 176)->save($indexPhotoUrl , 100);
        }

        if ($request->hasFile('main_photo')) {
            $fileType = $request->main_photo->extension();
            $rond = random_int(10000000000, 99999999999);
            $mainPhotoUrl = "assets/images/blog/$rond.$fileType";
            $image = Image::make($request->file('main_photo'));
            $image->resize(870, 425)->save($mainPhotoUrl , 100);
        }

        
        $status = 0;
        if ($request->status)
            $status = 1;

        $slug = str_replace(' ','-' ,$request->title);
        $countThisSlug = Blog::where('title' , $request->title)->count();
        if($countThisSlug > 1)
            $slug = $slug . '-' . $countThisSlug;

        $new = new Blog();
        $new->title         = $request->title;
        $new->description   = $request->description;
        $new->summary       = $request->summary;
        $new->iamge_title   = $request->iamge_title;
        $new->key_word      = $request->key_word;
        $new->image_alt     = $request->image_alt;
        $new->category_id   = $request->category;
//        $new->admin_id      = Auth::guard('admin')->user()->id;
        $new->admin_id      = 1;
        $new->status        = $status;
        $new->main_photo    = $mainPhotoUrl;
        $new->index_photo   = $indexPhotoUrl;
        $new->slug          = $slug;

        $new->save();

        if ($request->blog_id)
            BlogTag::where('blog_id',  $request->blog_id)->update(['blog_id' =>  $new->id]);

        if ($request->blog_images_id)
            BlogImages::where('blog_id',  $request->blog_images_id)->update(['blog_id' =>  $new->id]);



        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allCategory = Category::all();
        $blog = Blog::find($id);
        $allBlogTags = BlogTag::where('blog_id', $id)->get();
        return view('backend.pages.blog.edit', compact('blog', 'allCategory', 'allBlogTags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'title'        => 'required',
                'description'  => 'required'
            ]
        );

        if ($request->hasFile('index_photo')) {
            $fileType = $request->index_photo->getClientOriginalExtension();
            $rond = random_int(10000000000, 99999999999);
            $indexPhotoUrl = "assets/images/blog/$rond.$fileType";
            $image = Image::make($request->file('index_photo'));
            $image->resize(270, 176)->save($indexPhotoUrl , 100);
        }

        if ($request->hasFile('main_photo')) {
            $fileType = $request->main_photo->getClientOriginalExtension();
            $rond = random_int(10000000000, 99999999999);
            $mainPhotoUrl = "assets/images/blog/$rond.$fileType";
            $image = Image::make($request->file('main_photo'));
            $image->resize(870, 427)->save($mainPhotoUrl , 100);
        }

        $status = 0;
        if ($request->status)
            $status = 1;

        $slug = str_replace(' ','-' ,$request->title);
        $countThisSlug = Blog::where('title' , $request->title)->count();
        if($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;


        $edit = Blog::find($id);
        $edit->title         = $request->title;
        $edit->description   = $request->description;
        $edit->summary       = $request->summary;
        $edit->iamge_title   = $request->iamge_title;
        $edit->key_word      = $request->key_word;
        $edit->image_alt     = $request->image_alt;
        $edit->category_id   = $request->category;
        $edit->status        = $status;
        $edit->slug          = $slug;

        if(isset($mainPhotoUrl))
            $edit->main_photo    = $mainPhotoUrl;
        if(isset($indexPhotoUrl))
            $edit->index_photo   = $indexPhotoUrl;
        $edit->save();

        if ($request->blog_id)
            BlogTag::where('blog_id',  $request->blog_id)->update(['blog_id' =>  $id]);


        if ($request->blogMoreImage)
            BlogImages::where('blog_id',  $request->blogMoreImage)->update(['blog_id' =>  $id]);


        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Blog::find($id);
        if (file_exists($item->main_photo))
            unlink($item->main_photo);
        if (file_exists($item->index_photo))
            unlink($item->index_photo);
        $delete = $item->delete();
        if ($delete) {
            return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
        }
    }

        /**
     *  add blog tag
     */
    public function addTag(Request $request)
    {
        $blog_id = mt_rand(100000000, 999999999);

        if ($request->blog_id)
            $blog_id = $request->blog_id;

        $new = BlogTag::create([
            'caption' => $request->caption,
            'blog_id' => $blog_id,
            'status'  => 1
        ]);
        if ($new)
            return ['status' => true, 'newTag' => $new];
        else
            return ['status' => false];
    }

    /**
     *  removeTag blog tag
     */
    public function removeTag(Request $request)
    {
        BlogTag::destroy($request->tag_id);
        return ['status' => true];
    }
    /**
     *  removeTag blog tag
     */
    public function removImage(Request $request)
    {
        $item = BlogImages::find($request->image_id);
        if (file_exists($item->photo))
            unlink($item->photo);
        $item->delete();
        return ['status' => true];
    }

    public function images(Request $request)
    {
        $imageId = mt_rand(100000000, 999999999);

        if ($request->imageId)
            $imageId = $request->imageId;

        if ($request->hasFile('file')) {
            $fileType = $request->file->getClientOriginalExtension();
            $rond = random_int(10000000000, 99999999999);
            $photoUrl = "assets/images/blog/$rond.$fileType";
            $image = Image::make($request->file('file'));
            $image->save($photoUrl , 100);

            $new = new BlogImages();
            $new->blog_id   = $imageId;
            $new->photo     = $photoUrl;
            $new->save();

            return $new;
        }

        return "errorInSave";
    }



    public function commentStore(Request $request)
    {
        if(!Auth::guard('web')->user() &&  !Auth::guard('taskmaster')->user() )
            return ['status' => 0, 'msg' => "برای نظر دادن باید وارد حساب کاربری خود شوید"];

        if (!$request->comment)
            return ['status' => 0, 'msg' => "فیلد نظر اجباری می یاشد"];

        $parent_id = 0;
        if ($request->parent_id)
            $parent_id = $request->parent_id;

        if(Auth::guard('web')->user())
            $user_id = Auth::guard('web')->user()->id;
        else
            $user_id = Auth::guard('taskmaster')->user()->id;
            
        BlogComment::create([
            'blog_id'     => $request->blog_id,
            'user_id'     => $user_id,
            'parent_id'   => $parent_id,
            'comment'     => $request->comment,
            'status'      => 0
        ]);
        return  ['status' => 1, 'msg' => "نظر با موفقیت ثبت گردید"];
    }





    /*********************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * **********************************************     Front
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $blogs =  Blog::query();
        $blogs = $blogs->where('status', 1);
        $blogs = $blogs->orderBy('id', 'desc');
        $blogs = $blogs->paginate(24);

        return view('frontend.pages.blog.blog', compact('blogs'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($slug)
    {
        $blog = Blog::where('slug' ,$slug)->first();
        return view('frontend.pages.blog.details', compact('blog'));
    }

    public function categoryPosts($id)
    {
        $blogs = Blog::where('skill_id', $id)->where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.pages.blog.categoryPosts', compact('blogs'));
    }
}
