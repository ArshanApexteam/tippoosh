<?php

namespace App\Http\Controllers;

use App\Models\ServiceRequests;
use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\Category;

class ServiceUserRequestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = ServiceRequests::orderBy('id', 'desc')->paginate(20);
        return view('backend.pages.serviceRequestUser.all', compact('all'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.pages.serviceRequestUser.single');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = ServiceRequests::find($id);

        return view('backend.pages.serviceRequestUser.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ServiceRequests::find($id)->update(['status' => $request->status]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceRequests::find($id);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');

    }


    /*********************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * **********************************************     Front
     */


    public function add(Request $request)
    {
        $userCookieKey = $this::userCookieKey();

        $user_id = 0;
        if (Auth::guard('web')->user())
            $user_id = Auth::guard('web')->user()->id;

        if (!ServiceRequests::where('user_id', $user_id)->where('service_id', $request->service_id)->first())
            ServiceRequests::create([
                'user_id' => $user_id,
                'cookie_key' => $userCookieKey,
                'mail' => $request->mail,
                'phone' => $request->phone,
                'service_id' => $request->service_id,
                'name' => $request->name,
                'summary' => $request->summary
            ]);

        return ([
            'status' => true,
            'msg' => "درخواست شما با موفقیت ثبت گردید در اسرع وقت با شما تماس خواهیم گرفت"
        ]);
    }

    //   تعریف کلمه کلیدی برای کاربر
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


}
