<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

use Auth;
use App\Models\ContactUs;
use App\Models\Category;
use App\Models\Blog;
use App\Models\Service;
use App\Models\Carousel;
use App\Models\Team;
use App\Models\NewsLetter;
use App\Models\Product;

// use PhpParser\Node\Expr\PreDec;

class FrontController extends Controller
{


    public function index(Request $request ,  Route $route)
    {

//        $data['brand'] = Brand::where('status', 1)->get();
        $data['service'] = Service::where('status', 1)->orderBy('id' , 'desc')->get()->take(3);
        $data['carousel'] = Carousel::where('status', 1)->get();

        $product = new Product();
        $data['newProducts'] = $product->newProducts();
        $data['topProducts'] = $product->topProducts();
        $data['catman'] = $product->catman();
        $data['catwman'] = $product->catwman();
        $data['mazhabi'] = $product->mazhabi();
        $data['lash'] = $product->lash();
        $data['productOffer'] = $product->productOffer();

        $blog = new Blog();
        $data['blog'] = $blog->newBlogs();

        return view('frontend.pages.index', compact('data'));
    }

    public function category(Request $request , $slug)
    {
        $category10 = $category = $data['curentCategory'] = Category::where('slug' , $slug)->first();
		
        $data['archive'] =[];
        while($category->parent()){
            array_unshift($data['archive'],$category->parent());
            $category =  $category->parent();
        }
        
        $data['archive'][] = $data['curentCategory'];

        $maxPrice = explode(' ' ,$request->amount_max );
        $minPrice = explode(' ' ,$request->amount_min );


        $data['maxPrice'] = null;
        $data['minPrice'] = null;

        $data['isStore'] = 1;
        $data['sort_key'] = 1;
            
            
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {


            $data['allProducts'] = Product::query();
            $data['allProducts'] = $data['allProducts']->where('category_id', $category10->id)->where('status', 1);

            if (isset($request->sort_key)) {
                if ($request->sort_key == 1)
                    $data['allProducts'] = $data['allProducts']->orderBy('id', 'desc');

                if ($request->sort_key == 2)
                    $data['allProducts'] = $data['allProducts']->orderBy('id', 'asc');

                if ($request->sort_key == 3)
                    $data['allProducts'] = $data['allProducts']->orderBy('price', 'desc');

                if ($request->sort_key == 4)
                    $data['allProducts'] = $data['allProducts']->orderBy('price', 'asc');

                $data['sort_key'] = $request->sort_key;
            }

            if($request->isStore == 0 )
                $data['isStore'] = 0;
            else
                $data['allProducts'] = $data['allProducts']->where('store' , '>' , 0);

            $data['allProducts'] = $data['allProducts']->where('price' , '<' , $maxPrice[0]);
            $data['allProducts'] = $data['allProducts']->where('price' , '>' , $minPrice[0]);


            $data['maxPrice'] = $maxPrice[0] ;
            $data['minPrice'] = $minPrice[0] ;

            if($request->color){
                $data['allProducts'] = $data['allProducts']->whereHas('color' , function ($query)  use ($request){
                    $query->where('color' ,'=' , $request->color );
                });
            }


        }
        else {
            $data['allProducts'] = Product::where('category_id', $category10->id)->where('status', 1);
        }


        $data['paginate'] = 24;
        if($request->paginate)
            $data['paginate'] = $request->paginate;
		
        $data['allProducts'] = $data['allProducts']->paginate($data['paginate']);

        $data['count'] = $data['allProducts']->total();
        $data['firstItem'] = $data['allProducts']->firstItem();
        $data['lastItem'] = $data['allProducts']->lastItem();
        
        return view('frontend.pages.category' , compact('data'));
    }

    public function search(Request $request)
    {
        $data['curentSearch'] = $request->serach;

        
        $maxPrice = explode(' ' ,$request->amount_max );
        $minPrice = explode(' ' ,$request->amount_min );


        $data['maxPrice'] = null;
        $data['minPrice'] = null;

        $data['isStore'] = 1;
        $data['sort_key'] = 1;



        $data['allProducts'] = Product::query();
        $data['allProducts'] = $data['allProducts']
            ->where('status', 1)
            ->where('name_fa', 'LIKE', '%' . $request->serach . '%');
//            ->orWhere('name_en', 'LIKE', '%' . $request->serach . '%');

        if (isset($request->sort_key)) {
            if ($request->sort_key == 1)
                $data['allProducts'] = $data['allProducts']->orderBy('id', 'desc');

            if ($request->sort_key == 2)
                $data['allProducts'] = $data['allProducts']->orderBy('id', 'asc');

            if ($request->sort_key == 3)
                $data['allProducts'] = $data['allProducts']->orderBy('price', 'desc');

            if ($request->sort_key == 4)
                $data['allProducts'] = $data['allProducts']->orderBy('price', 'asc');

            $data['sort_key'] = $request->sort_key;
        }

        if($request->isStore == 0 )
            $data['isStore'] = 0;
        else
            $data['allProducts'] = $data['allProducts']->where('store' , '>' , 0);

        $data['allProducts'] = $data['allProducts']->where('price' , '<' , $maxPrice[0]);
        $data['allProducts'] = $data['allProducts']->where('price' , '>' , $minPrice[0]);


        $data['maxPrice'] = $maxPrice[0] ;
        $data['minPrice'] = $minPrice[0] ;

        if($request->color){
            $data['allProducts'] = $data['allProducts']->whereHas('color' , function ($query)  use ($request){
                $query->where('color' ,'=' , $request->color );
            });
        }




        $data['paginate'] = 24;
        if($request->paginate)
            $data['paginate'] = $request->paginate;


        $data['allProducts'] = $data['allProducts']->paginate($data['paginate']);

        $data['count'] = $data['allProducts']->total();
        $data['firstItem'] = $data['allProducts']->firstItem();
        $data['lastItem'] = $data['allProducts']->lastItem();
        return view('frontend.pages.search' , compact('data'));
    }


    public function aboutUs()
    {
        return view('frontend.pages.aboutUs');
    }


    public function contactUs()
    {
        return view('frontend.pages.contactUs');
    }

    public function help()
    {
        return view('frontend.pages.help');
    }


    public function userContactUs(Request $request, ContactUs $contact)
    {
        $contact->create($request->all());
        return ([
            'status' => 1 ,
            'msg' => 'اطلاعات شما با موفقیت ثبت گردید در اسرع وقت با شما تماس خواهیم گرفت .'
        ]);
    }


    public function newsLetter(Request $request)
    {
        NewsLetter::create($request->all());
        return ([
            'status' => 1 ,
            'msg' => 'شما با موفقیت به خبرنامه ما اضافه شدید'
        ]);
    }

    public function payment()
    {
        
    }

    public function paymentCallback()
    {
        
    }
}
