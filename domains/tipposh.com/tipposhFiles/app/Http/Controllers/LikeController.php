<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Auth;


use App\Models\Product;

class LikeController extends Controller
{

    //   تعریف کلمه کلیدی برای کاربر
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userProductLikes']) && $_COOKIE['userProductLikes'] != null)
            $key = json_decode($_COOKIE['userProductLikes'], true);
        else
            setcookie('userProductLikes', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


    //***************************************          اضافه کردن به مورد علاقه ها
    public function likeAndDislike(Request $request)
    {
        // بررسی اینکه آیا مورد علاقه ها از قبل درست شده است یا نه
        $products = [];
        if (isset($_COOKIE['userProductLikes']) && $_COOKIE['userProductLikes'] != null) {
            $products = json_decode($_COOKIE['userProductLikes'], true);
        }


        //  بررسی اینکه آیا محصول در مورد علاقه ها کاربر وجود دارد یا نه
        foreach ($products as $item) {
            if ($item == $request->product_id) {
                //******************************* */
                //*******       remove from likes
                //****** */  
                $key = array_search($request->product_id, $products);
                unset($products[$key]);

                setcookie('userProductLikes', json_encode($products), time() + (840000 * 10) * 30, '/');
                return [
                    'status'   => 0,
                    'msg'      => 'محصول با موفقیت از موردعلاقه های شما حذف گردید',
                ];
            }
        }
        //******************************* */
        //*******      add to likes
        //****** */ 
        $products[] = $request->product_id;
        setcookie('userProductLikes', json_encode($products), time() + 8400000 * 10, '/');
        return [
            'status'   => 1,
            'msg'      => 'محصول با موفقیت به مورد علاقه های شما اضافه گردید',
        ];
    }



    public function myLikeItems()
    {
        $products = [];
        if (isset($_COOKIE['userProductLikes']) && $_COOKIE['userProductLikes'] != null) {
            $products = json_decode($_COOKIE['userProductLikes'], true);
        }

        $allProduct = Product::whereIn('id',$products)->get();

        return [
            'status'   => 1,
            'locale'   => session()->get('localization'),
            'data'     => $allProduct
        ];
    }
}
