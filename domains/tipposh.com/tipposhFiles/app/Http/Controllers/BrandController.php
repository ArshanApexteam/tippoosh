<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;
use Auth;

use App\Models\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Brand::orderBy('id', 'desc')->paginate(10);
        return view('backend.pages.brand.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.brand.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'caption'            => 'required',
                'photo'              => 'required',
            ]
        );

        if ($request->hasFile('photo')) {
            $rond = random_int(10000000000, 99999999999);
            $fileType = $request->photo->extension();
            $photoUrl = "assets/images/$rond.$fileType";
            $image = Image::make($request->file('photo'));
            $image->resize(150 , 100)->save($photoUrl , 100);
        }

        if(!isset($photoUrl))
            $photoUrl = "";

            Brand::create([
            'photo' => $photoUrl,
            'caption' =>  $request->caption,
        ]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('backend.pages.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'caption'            => 'required',
            ]
        );

        if ($request->hasFile('photo')) {
            $rond = random_int(10000000000, 99999999999);
            $fileType = $request->photo->extension();
            $photoUrl = "assets/images/$rond.$fileType";
            $image = Image::make($request->file('photo'));
            $image->resize(150 , 100)->save($photoUrl , 100);

            Brand::find($id)->update(['photo' => $photoUrl,]);

        }

        Brand::find($id)->update(['caption' =>  $request->caption,]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (file_exists(Brand::find($id)->photo))
            unlink(Brand::find($id)->photo);
        Brand::destroy($id);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }
}
