<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;

use App\Models\Carousel;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Carousel::orderBy('id', 'desc')->get();
        return view('backend.pages.carousel.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.carousel.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'            => 'required',
                'photo'            => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );
        $status = 0;
        if ($request->status)
            $status = 1;

        $photo = "";
        if ($request->hasFile('photo')) {
            $photo = $this->upload_image($request->file('photo'));
        }


        Carousel::create([
            'name_fa'          =>  $request->name_fa,
            'name_en'          =>  $request->name_en,
            'price'            =>  $request->price,
            'title'            =>  $request->title,
            'description'      =>  $request->description,
            'photo'            =>  $photo,
            'status'           =>  $status,
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carousel = Carousel::find($id);
        return view('backend.pages.carousel.edit', compact('carousel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'title'            => 'required',
            ]
        );
        $status = 0;
        if ($request->status)
            $status = 1;

        if ($request->hasFile('photo')) {
            if (file_exists(Carousel::find($id)->photo))
                unlink(Carousel::find($id)->photo);

            $photo = $this->upload_image($request->file('photo'));
            Carousel::find($id)->update([ 'photo' =>  $photo ]);
        }


        Carousel::find($id)->update([
            'name_fa'          =>  $request->name_fa,
            'name_en'          =>  $request->name_en,
            'price'            =>  $request->price,
            'title'            =>  $request->title,
            'description'      =>  $request->description,
            'status'           =>  $status,
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (file_exists(Carousel::find($id)->photo))
            unlink(Carousel::find($id)->photo);
        Carousel::destroy($id);
        return redirect()->back();
    }

    public function upload_image($file){
        $fileType = $file->extension();
        $rondName = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 10);
        $imgFullName = "assets/images/$rondName.$fileType";
        $image = Image::make($file);
        $image->resize(330, 440)->save($imgFullName , 100);
        return $imgFullName;
    }
}
