<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Ad_Permissios;

class Ad_PermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('adminAuth')->except('show');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Ad_Permissios::paginate(40);
        return view('backend.pages.permissions.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.permissions.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'action'        => 'required'
            ],
            [
                'action.required' => 'فیلد مجوز الزامی است'
            ]
        );

        Ad_Permissios::create(['action' =>$request->action ]);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Ad_Permissios::find($id);
        return view('backend.pages.permissions.edit', compact('permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'action'        => 'required'
            ],
            [
                'action.required' => 'فیلد مجوز الزامی است'
            ]
        );

        Ad_Permissios::find($id)->update(['action' =>$request->action ]);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ad_Permissios::destroy($id);
        if (file_exists('assets/images/permissions/' . $id . '.jpg'))
            unlink('assets/images/permissions/' . $id . '.jpg');

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }
}
