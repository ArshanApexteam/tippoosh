<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Models\ProductDiscount;

class ProductDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = ProductDiscount::orderBy('id', 'desc')->paginate(100);
        return view('backend.pages.productDiscount.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.productDiscount.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'price' => 'required',
            ]
        );

        ProductDiscount::create([
            'code' => 'Tipposh-' . substr(str_shuffle('0123456789'), 1, 6),
            'price' => $request->price,
            'status' => 0,
            'product_id' => 0,
            'user_id' => 0
        ]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productDiscount = ProductDiscount::find($id);
        return view('backend.pages.productDiscount.edit', compact('productDiscount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'price' => 'required',
            ]
        );

        ProductDiscount::find($id)->update([
            'price' => $request->price,
        ]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductDiscount::destroy($id);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');;
    }
}
