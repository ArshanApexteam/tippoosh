<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;


use App\Models\Category;

class CategoryController extends Controller
{

    public function __construct()
    {
        //$this->middleware('adminAuth')->except('show');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Category::orderBy('id', 'desc')->where('parent_id' , 0)->get();
        return view('backend.pages.category.all', compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.category.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name'        => 'required',
            ]
        );

        $status = 0;
        if ($request->status)
            $status = 1;

        $slug = str_replace(' ','-' ,$request->name);
        $countThisSlug = Category::where('name' , $request->name)->count();
        if($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;

        Category::create([
            'name'            =>  $request->name,
            'parent_id'       =>  $request->parent_id,
            'status'          =>  $status,
            'slug'            =>  $slug,
        ]);

        alert()->success('ایجاد دسته بندی جدید',"دسته بندی جدید با موفقیت ایجاد گردید");
        return redirect()->route('admin.category.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('backend.pages.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name'        => 'required',
            ]
        );

        $status = 0;
        if ($request->status)
            $status = 1;

        $slug = str_replace(' ','-' ,$request->name);
        $countThisSlug = Category::where('name' , $request->name)->count();
        if($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;

        Category::find($id)->update([
            'name'            =>  $request->name,
            'parent_id'       =>  $request->parent_id,
            'status'          =>  $status,
            'slug'            =>  $slug,
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect()->back();
    }
}
