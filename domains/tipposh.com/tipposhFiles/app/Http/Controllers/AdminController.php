<?php

namespace App\Http\Controllers;

use App\Models\Ad_Permissios;
use App\Models\Ad_Admin_Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Admin;
use Auth;
use Image;


class AdminController extends Controller
{

    public function __construct()
    {
        //$this->middleware('adminAuth');
        // $this->middleware('checkAdminstrator')->except('dashboard');;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Admin::orderBy('id', 'desc')->paginate(10);
        return view('backend.pages.admin.all', compact('all'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actions = Ad_Permissios::all();
        return view('backend.pages.admin.new', compact('actions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'sometimes', 'string', 'email', 'max:255', 'unique:admins'],
                'password' => ['required', 'string', 'min:8'],
                'phone' => ['min:11', 'max:11'],
            ]);

        $url = "";
        if ($request->hasFile('file')) {
            $fileType = $request->file->extension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/user/admins/$rond.$fileType";
            $image = Image::make($request->file('file'));
            $image->resize(100, 100)->save($url, 100);
        }


        $new = new Admin();
        $new->name = $request->name;
        $new->email = $request->mail;
        $new->password = Hash::make($request->password);
        $new->phone = $request->phone;
        $new->address = $request->address;
        $new->imageProfile = $url;

        $new->save();

        if ($request->rule && isset($new->id)) {
            foreach ($request->rule as $rule) {
                Ad_Admin_Rule::create([
                    'admin_id' => $new->id,
                    'permission_id' => $rule,
                ]);
            }
        }

        return redirect()->route('admin.admins.index')->with('prossesOk', 'مدیر  با موفقیت   اضافه گردید');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find($id);
        $actions = Ad_Permissios::all();
        return view('backend.pages.admin.edit', compact('admin', 'actions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'phone' => ['min:11', 'max:11'],
            ]);

        $admin = Admin::find($id);
        $admin->name = Request()->name;
        $admin->phone = Request()->phone;
        $admin->address = Request()->address;

        if (isset($request->password))
            $admin->password = Hash::make(Request()->password);

        $admin->save();


        if ($request->hasFile('file')) {
            $fileType = $request->file->extension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/user/admins/$rond.$fileType";
            $image = Image::make($request->file('file'));
            $image->resize(100, 100)->save($url, 100);
            Admin::find($id)->update(['imageProfile' => $url]);
        }


        if ($request->rule) {
            foreach ($request->rule as $rule) {
                if (!Ad_Admin_Rule::where('admin_id', $id)->where('permission_id', $rule)->first())
                    Ad_Admin_Rule::create([
                        'admin_id' => $id,
                        'permission_id' => $rule,
                    ]);
            }
        }

        return redirect()->back()->with('prossesOk', 'مدیر با موفقیت ویرایش گردید');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::find($id);
        $checkdelete = $admin->delete();
        if ($checkdelete)
            return redirect()->back()->with('prossesOk', 'مدیر با موفقیت   حذف گردید');

        return redirect()->back()->with('prossesError', 'خطا!');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }




    //######################################################################################      forgetPassForm
    public function forgetPassForm(Request $request)
    {
        return view('backend.auth.forgetPass');
    }

    //######################################################################################      forgetPass
    public function forgetPass(Request $request)
    {
        $token = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 50);
        if(Admin::where('email', $request->mail)->first()){
            Admin::where('email', $request->mail)->first()->notify(new ResetPassword($token));

            PasswordResets::create([
                'email' => $request->mail,
                'token' => $token
            ]);


            return redirect()->back()->with('prossesOk', 'ایمیل تغییر رمز عبور برای شما راسال گردید.');
        }
        return redirect()->back()->with('prossesError', 'ایمیل وارد شده فاقد اعتبار می باشد');
    }


    //######################################################################################      forgetPassForm
    public function changePassForm(Request $request, $token, $email)
    {
        $resetPassword = ResetPassword::where('email', $email)->where('token', $token)->first();
        if (isset($resetPassword)) {
            if (strtotime ($resetPassword->created_at) + 3600 > time())
                $data['userEmil'] = $email;
            else
                return redirect()->route('admin.login');
        } else
            return redirect()->route('admin.login');

        return view('backend.auth.changePass', compact('data'));
    }

    //######################################################################################      forgetPass
    public function changePass(Request $request)
    {
        $user = Admin::where('email' , $request->email)->first();
        if(isset($user)){
            $user->password = Hash::make($request['password']);

            Auth::login($user);
            return redirect()->route('admin.profile')->with('prossesOk', 'تغییر رمزعبور شما با موفقیت انجام شد');
        }

        return redirect()->route('admin.login');
    }

}
