<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Larabookir\Gateway\Gateway;
use Exception;
use Auth;

use App\Models\Sale;
use App\Models\Basket;
use App\Models\ProductDiscount;
use App\Models\Product;

class PaymentController extends Controller
{

    public function saleAll(){
        $all = Sale::orderBy('id', 'desc')->paginate(50);
        return view('backend.pages.sale.all' , compact('all'));
    }
    public function payment()
    {
        $userCookieKey = $this::userCookieKey();

        if(Basket::where('cookie_key', $userCookieKey)->first()->phone == "" &&
            Basket::where('cookie_key', $userCookieKey)->first()->address == "" )
            return redirect()->back()->with('userNotLogin' ,'وارد کردن شماره تلفن و آدرس اجباری می باشد');

        $price = $this::paymentPrice() * 10;
        session()->put('totalPriceWithDiscount' , $this::paymentPrice());

        try {
            $gateway = \Gateway::zarinpal();
            $gateway->setCallback('https://tipposh.com/paymentCallback');
            $gateway->price( (int)$price )->ready();
            $refId =  $gateway->refId();
            $transID = $gateway->transactionId();
            return $gateway->redirect();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function paymentCallback(Request $request)
    {
        $userCookieKey = $this::userCookieKey();

        $takhfifanItem =[];

        try {
            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();

            $basketIds =  session()->get('basketSolldIds');

            foreach ( $basketIds as $id){
                $item = Basket::find($id);
                if($item){
                    Sale::create([
                        'user_id'  => $item->user_id,
                        'product_id' => $item->product_id,
                        'count' => $item->count,
                        'address' => $item->address,
                        'size' => $item->size,
                        'phone' => $item->phone,
                        'color' => $item->color,
                        'photo' => $item->photo,
                        'discount_code' => $item->discount_code,
                        'status' =>1
                    ]);

                    $takhfifanItem[] = (object) array
                    (
                        "category"=> $item->product->category->name,
                        "product_id"=> $item->product_id,
                        "price" => $item->product->price,
                        "quantity"=> $item->count,
                        "name"=> $item->product->name
                    );
                }
            }
            session()->forget('basketSolldIds');

            Basket::where('cookie_key', $userCookieKey)->delete();


            $this::takhfifanCookie();
            if($this::takhfifanCookie()){

                //The url you wish to send the POST request to
                $url = "https://analytics.takhfifan.com/track/purchase";

                //The data you want to send via POST
                $fields = [
                    'token'            => $this::takhfifanCookie(),
                    "transaction_id"   => $gateway->trackingCode(),
                    "revenue"          => session()->get('totalPriceWithDiscount'),
                    "shipping"         => 0,
                    "tax"              => 0,
                    "discount"         => session()->get('totalPrice') - session()->get('totalPriceWithDiscount'),
                    "new_customer"     => "false",
                    "affiliation"      => "takhfifan",
                    "items"            => $takhfifanItem
                ];

                session()->forget('totalPrice');
                session()->forget('totalPriceWithDiscount');

                //url-ify the data for the POST
                $fields_string = http_build_query($fields);

                //open connection
                $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                //So that curl_exec returns the contents of the cURL; rather than echoing it

                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

                //execute post
                $result = curl_exec($ch);
                echo $result;
            }

            return redirect()->route('index')->with('paymentOk', '1');


        } catch (Exception $e) {
            session()->put('paymentError' , $e->getMessage());
            return redirect()->route('basket.factor');
        }
    }


    function paymentPrice()
    {
        $userCookieKey = $this::userCookieKey();

        $userBasket = Basket::where('cookie_key', $userCookieKey)->get();

        $salePrice = 0;
        $offerPrice = 0;
        $totalPrice = 0;
        $discount = 0;
        $id= [];
        foreach ($userBasket as $item) {
            $id [] = $item->id;
            $salePrice += $item->count * $item->product->price;
            $offerPrice += $item->count * (($item->product->offer * $item->product->price) / 100);
            if(ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first())
                $discount += ProductDiscount::whereRaw("BINARY `code`= ?",[$item->discount_code])->first()->price;
        }

        session()->put('basketSolldIds' , $id);
        session()->put('totalPrice' , $salePrice);

		if($salePrice - $offerPrice > 1000)
            $totalPrice = $salePrice - $offerPrice - $discount;
        else
            $totalPrice =1000;

        return $totalPrice;
    }


    //######################################################################################      takhfifanCookie
    function takhfifanCookie()
    {
        if (isset($_COOKIE['takhfifan']) && $_COOKIE['takhfifan'] != null)
            $key= $_COOKIE['takhfifan'];
        return $key;
    }

    //######################################################################################      userCookieKey
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;

    }


}
