<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Hash;
use Image;

class BackendController extends Controller
{

    public function dashboard()
    {
        return view('backend.pages.dashboard');
    }


    //*************************************     authentication Section
    public function login()
    {
        if (Auth()->guard('admin')->check())
            return redirect()->route('admin.dashboard');

        return view('backend.auth.login');
    }

    public function authentication(Request $request)
    {
        if (Auth::guard('admin')->attempt(['email' => Request()->email, 'password' => Request()->password])) {
            return redirect()->route('admin.dashboard');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
    }

    //******************************************************


    public function profile()
    {
        $admin = Auth::guard('admin')->user();
        return view('backend.pages.admin.profile', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function profileUpdate(Request $request)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'phone' => ['min:11', 'max:11'],
            ]);

        $admin = Auth::guard('admin')->user();
        $admin->name = Request()->name;
        $admin->phone = Request()->phone;
        $admin->address = Request()->address;

        if (isset($request->password))
            $admin->password = Hash::make(Request()->password);

        $admin->save();

        if ($request->hasFile('file')) {
            $fileType = $request->file->extension();
            $rond = random_int(10000000000, 99999999999);
            $url = "assets/images/user/admins/$rond.$fileType";
            $image = Image::make($request->file('file'));
            $image->resize(100, 100)->save($url, 100);
            Admin::find($admin->id)->update(['imageProfile' => $url]);
        }

        return redirect()->back()->with('prossesOk', 'اطلاعات شما با موفقیت بروزرسانی شد');
    }
}
