<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\ProductTag;
use App\Models\Setting;
use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\Product;
use App\Models\ProductComment;
use App\Models\ProductGallery;
use App\Models\Category;
use App\Models\ProductColor;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('adminPermision')
            ->except('addTag','removeTag','removImage','commentStore','all','removeColor' ,'show' ,'index');
    }


    //   تعریف کلمه کلیدی برای کاربر
    static function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 50);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Product::orderBy('id', 'desc')->paginate(20);
        return view('backend.pages.product.all', compact('all'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all($id)
    {
//        $category = Category::find($id);
        return view('frontend.pages.product.all', compact('category'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', $slug)->first();

//        $category = $product->category;
//        $data['archive'] = [];
//        while($category->parent){
//            array_unshift($data['archive'],$category->parent);
//            $category =  $category->parent;
//        }


        return view('frontend.pages.product', compact('product'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategory = Category::where('parent_id', 0)->get();
        return view('backend.pages.product.new', compact('allCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name_fa' => 'required',
                'name_en' => 'required',
                'store' => 'required',
                'price' => 'required',
                'metakeywords' => 'required',
                'metaDescription' => 'required',
                'description' => 'required',
            ]
        );

        $slug = str_replace(' ', '-', $request->name_fa);
        $countThisSlug = Product::where('name_fa', $request->name_fa)->count();
        if ($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;


        $status = 0;
        if ($request->status)
            $status = 1;

        $new = new Product();
        $new->name_fa = $request->name_fa;
        $new->name_en = $request->name_en;
        $new->store = $request->store;
        $new->price = $request->price;
        $new->category_id = $request->category_id;
        $new->offer = $request->offer;
        $new->description = $request->description;
        $new->summery = $request->summery;
        $new->material = $request->material;
        $new->metakeywords = $request->metakeywords;
        $new->metaDescription = $request->metaDescription;

        $new->slug = $slug;
        $new->status = $status;
        $new->save();


        if ($request->color) {
            foreach ($request->color as $color) {
                ProductColor::create([
                    'product_id' => $new->id,
                    'color' => $color
                ]);
            }
        }


        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {

                $rond = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 20);
                $imageName = $rond . '.' . $file->getClientOriginalExtension();

                $image = Image::make($file);
                $photoUrl = "assets/images/product/$imageName";
                $image->save($photoUrl, 100);

                $newImage = [];
                $newImage['product_id'] = $new->id;
                $newImage['photo'] = $photoUrl;
                ProductGallery::create($newImage);

            }
        }

        if ($request->product_id)
            ProductTag::where('product_id', $request->product_id)->update(['product_id' => $new->id]);


        return redirect()->route('admin.product.edit', $new->id)->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allCategory = Category::all();
        $product = Product::find($id);
        return view('backend.pages.product.edit', compact('product', 'allCategory'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name_fa' => 'required',
                'name_en' => 'required',
                'store' => 'required',
                'price' => 'required',
                'metakeywords' => 'required',
                'metaDescription' => 'required',
                'description' => 'required',
            ]
        );


        $slug = str_replace(' ', '-', $request->name_fa);
        $countThisSlug = Product::where('name_fa', $request->name_fa)->where('id', '!=', $id)->count();
        if ($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;


        $status = 0;
        if ($request->status)
            $status = 1;

        $product = Product::find($id);
        $product->name_fa = $request->name_fa;
        $product->name_en = $request->name_en;
        $product->store = $request->store;
        $product->price = $request->price;
        $product->category_id = $request->category_id;
        $product->offer = $request->offer;
        $product->summery = $request->summery;
        $product->material = $request->material;
        $product->description = $request->description;
        $product->metakeywords = $request->metakeywords;
        $product->metaDescription = $request->metaDescription;
        $product->status = $status;
        $product->slug = $slug;
        $product->save();


        if ($request->color) {
            foreach ($request->color as $color) {
                ProductColor::create([
                    'product_id' => $id,
                    'color' => $color
                ]);
            }
        }


        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {

                $rond = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 20);
                $imageName = $rond . '.' . $file->getClientOriginalExtension();

                $image = Image::make($file);
                $photoUrl = "assets/images/product/$imageName";
                $image->save($photoUrl, 100);

                $newImage = [];
                $newImage['product_id'] = $id;
                $newImage['photo'] = $photoUrl;
                ProductGallery::create($newImage);

            }
        }

        if ($request->product_id)
            ProductTag::where('product_id', $request->product_id)->update(['product_id' => $id]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function removImage(Request $request)
    {
        $item = ProductGallery::find($request->photo_id);
        if (file_exists($item->small))
            unlink($item->small);
        if (file_exists($item->medium))
            unlink($item->medium);
        if (file_exists($item->big))
            unlink($item->big);

        $item->delete();

        return (['status' => true]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function removeColor(Request $request)
    {
        ProductColor::destroy($request->color_id);
        return (['status' => true]);
    }


    public function commentStore(Request $request)
    {
        $cookie = $this::userCookieKey();
        if (!$request->comment)
            return ['status' => 0, 'msg' => "فیلد نظر اجباری می یاشد"];

        $parent_id = 0;
        if ($request->parent_id)
            $parent_id = $request->parent_id;

        $id = null;
        if(Auth::guard('web')->user())
            $id = Auth::guard('web')->user()->id;

        ProductComment::create([
            'product_id'     => $request->product_id,
            'user_id'        =>$id,
            'parent_id'      => $parent_id,
            'comment'        => $request->comment,
            'status'         => 0,
            'user_name'      => $request->user_name,
            'user_mail'      => $request->user_mail,
            'cookie_key'     => $cookie,
            'tarahi'          => $request->tarahi,
            'pricing'        => $request->pricing,
            'rezaiatMoshtari'  => $request->rezaiatMoshtari,
            'quentity'        => $request->quentity,

        ]);
        return  ['status' => 1, 'msg' => "نظر با موفقیت ثبت گردید"];
        
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        foreach ($product->gallery as $image) {
            $item = ProductGallery::find($image->id);

            if (file_exists($item->small))
                unlink($item->small);
            if (file_exists($item->medium))
                unlink($item->medium);
            if (file_exists($item->big))
                unlink($item->big);

            $item->delete();
        }

        Product::destroy($id);

        return redirect()->back();
    }


    /**
     *  add blog tag
     */
    public function addTag(Request $request)
    {
        $product_id = mt_rand(100000000, 999999999);

        if ($request->$product_id)
            $product_id = $request->$product_id;

        $new = ProductTag::create([
            'caption' => $request->caption,
            'product_id' => $product_id,
            'status' => 1
        ]);
        if ($new)
            return ['status' => true, 'newTag' => $new];
        else
            return ['status' => false];
    }

    /**
     *  removeTag blog tag
     */
    public function removeTag(Request $request)
    {
        ProductTag::destroy($request->tag_id);
        return ['status' => true];
    }
}
