<?php

namespace App\Http\Controllers;

use App\Models\ServiceRequests;
use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\Skills;
use App\Models\Service;
use App\Models\ServiceComment;
use App\Models\ServiceImages;
use App\Models\ServiceTag;
use App\Models\Category;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Service::orderBy('id', 'desc')->paginate(10);
        return view('backend.pages.service.all', compact('all'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.pages.service.single');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.service.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title' => 'required',
                'price' => 'required',
                'summary' => 'required',
                'description' => 'required',
                'icon' => 'required',
                'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );

        $status = 0;
        if ($request->status)
            $status = 1;


        if ($request->hasFile('image')) {
            $image_medium = $this->upload_image($request->file('image'), 'medium');
            $image_big = $this->upload_image($request->file('image'), 'big');
        }


        $slug = str_replace(' ', '-', $request->title);
        $countThisSlug = Service::where('title', $request->title)->count();
        if ($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;

        Service::create([
            'title' => $request->title,
            'description' => $request->description,
            'summary' => $request->summary,
            'image_medium' => $image_medium,
            'image_big' => $image_big,
            'icon' => $request->icon,
            'price' => $request->price,
            'status' => $status,
            'slug' => $slug,
        ]);


        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        return view('backend.pages.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'title' => 'required',
                'summary' => 'required',
                'description' => 'required',
                'icon' => 'required',
            ]
        );

        $status = 0;
        if ($request->status)
            $status = 1;


        if ($request->hasFile('image')) {
            if (file_exists(Service::find($id)->image_medium))
                unlink(Service::find($id)->image_medium);

            if (file_exists(Service::find($id)->image_big))
                unlink(Service::find($id)->image_big);

            $image_medium = $this->upload_image($request->file('image'), 'medium');
            $image_big = $this->upload_image($request->file('image'), 'big');

            Service::find($id)->update([
                'image_medium' => $image_medium,
                'image_big' => $image_big,
            ]);
        }


        $slug = str_replace(' ', '-', $request->title);
        $countThisSlug = Service::where('title', $request->title)->count();
        if ($countThisSlug > 0)
            $slug = $slug . '-' . $countThisSlug;

        Service::find($id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'summary' => $request->summary,
            'icon' => $request->icon,
            'price' => $request->price,
            'status' => $status,
            'slug' => $slug,
        ]);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (file_exists(Service::find($id)->image_medium))
            unlink(Service::find($id)->image_medium);

        if (file_exists(Service::find($id)->image_big))
            unlink(Service::find($id)->image_big);

        Service::find($id);

        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');

    }


    public function upload_image($file, $status)
    {
        if ($status == "big") {
            $width = 780;
            $heghit = 554;
        }
        if ($status == "medium") {
            $width = 400;
            $heghit = 261;
        }
        $rand = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 20);
        $file_name = $rand . "." . $file->extension();
        $path = "assets/images/$file_name";
        $image = Image::make($file);
        $image->resize($width, $heghit)->save($path, 100);
        return $path;
    }


    /*********************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * *******************************************************************************************************
     * **********************************************     Front
     */
    public function services()
    {
        $data['service'] = Service::where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.pages.service.service', compact('data'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function details($slug)
    {
        $all = Service::where('status', 1)->orderBy('id', 'desc')->get();
        $service = Service::where('slug', $slug)->first();
        return view('frontend.pages.service.details', compact('service', 'all'));
    }

    //   تعریف کلمه کلیدی برای کاربر
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


}
