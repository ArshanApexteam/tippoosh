<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Product;
use App\Models\PageStatic;

use Illuminate\Http\Request;


class SitemapController extends Controller
{

    public function sitemap()
    {
        return response()->view('frontend.pages.sitemap.sitemap')->header('Content-Type','text/xml');
    }

    public function statics()
    {
        return response()->view('frontend.pages.sitemap.statics')->header('Content-Type','text/xml');
    }

    public function category()
    {
        $query=Category::all();
        return response()->view('frontend.pages.sitemap.category',compact('query'))->header('Content-Type','text/xml');
    }

    public function post()
    {
        $query=Blog::with('blogImages')->orderBy('id','desc')->get();
        return response()->view('frontend.pages.sitemap.post',compact('query'))->header('Content-Type','text/xml');
    }
    public function product()
    {
        $query=Product::with('gallery')->orderBy('id','desc')->get();
        return response()->view('frontend.pages.sitemap.product',compact('query'))->header('Content-Type','text/xml');
    }
}