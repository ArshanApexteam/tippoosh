<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\Models\ProductComment;
use App\Models\User;

class ProductCommentController extends Controller
{
    public function index()
    {
        $users = ProductComment::query();

        //   مرتب بندی براساس کاربران
        $users = $users->get()->groupBy('user_id');

        // معکوس کردن
        $users = $users->reverse();

        //   انتخاب تعدادی از کاربان
        $users = $users->take(100);

        $all = [];
        foreach ($users as $item) {
            $all[] = $item->last();
        }

        return view('backend.pages.productComments.all', compact('all'));
    }

    //  تاریخچه
    public function userHistory(Request $request, $user_id)
    {
        $all = ProductComment::orderBy('id', 'asc')->where('user_id', $user_id)->where('parent_id' , 0)->get();
        return view('backend.pages.productComments.replay', compact('all'));
    }

    public function replay(Request $request)
    {
        $comment = ProductComment::find($request->comment_id);
        ProductComment::create([
            'product_id' => $comment->product_id,
            'user_id' => Auth::guard('admin')->user()->id,
            'parent_id' => $comment->id,
            'comment' => $request->text,
            'user_is_admin' => 1,
            'status' => 1
        ]);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }


    //   تغییر وضعیت
    public function changeStatus(Request $request, $id)
    {
        $productComment = ProductComment::find($id);

        if ($productComment->status == 0)
            $productComment->status = 1;
        else
            $productComment->status = 0;
        $productComment->save();
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }


    public function destroy($id)
    {
        ProductComment::destroy($id);
        return redirect()->back()->with('prossesOk', 'عملیات  با موفقیت انجام شد ');
    }
}
