<?php

namespace App\Notifications\Admin\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param string $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        return (new MailMessage)
            ->from('info@tipposh.com/', 'تی پوش')
            ->greeting('تی پوش!')
            ->subject(Lang::get('ایمیل تغییر رمز عبور'))
            ->line(Lang::get('شما این ایمیل را دریافت کرده اید زیرا ما یک درخواست فراموشی رمز عبور از حساب کاربری شما دریافت نموده ایم'))
            ->action(Lang::get('تغییر رمز عبور'),
                url('http://tipposh.com/' .
                    route('user.changePassForm',
                        ['token' => $this->token, 'email' => $notifiable->getEmailForPasswordReset()], false)))
            ->line(Lang::get('این لینک تغییر رمز عبور به صورت خودکار منقضی خواهد شد بعد از  :دقیقه.', ['count' => config('auth.passwords.admin.expire')]))
            ->line(Lang::get('اگر شما این درخواست را ارسال نکرده اید ، نگران نباشید خود به خود منقضی میگردد'));
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
