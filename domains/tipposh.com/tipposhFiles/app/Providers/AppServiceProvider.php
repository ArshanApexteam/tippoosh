<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Category;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //   sql  settings
        \Schema::defaultStringLength(191);

        //  همه دسته بندی ها
        $allCategoreis = Category::where('parent_id' , 0)->get();
        View::share('allCategoreis', $allCategoreis);

    }
}
