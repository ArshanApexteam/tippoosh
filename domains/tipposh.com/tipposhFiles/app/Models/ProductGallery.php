<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class ProductGallery extends Model
{
    protected $table = 'product_gallery';
    public $timestamps = false;
    protected $fillable = ['product_id', 'photo'];


    protected $hidden = [
        'product_id',
    ];

}
