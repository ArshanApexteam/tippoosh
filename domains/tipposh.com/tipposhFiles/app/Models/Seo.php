<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Seo extends Model
{
    protected $table = 'seo';
    public $timestamps = false;
    protected $fillable = [
        'text', 
        'status' //  1 => keywords       2 => description
    ];
}
