<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class UsersVerify extends Model
{
    protected $table = 'users_verify';
    public $timestamps = false;
    protected $fillable = [
        'code',
        'phone'
    ];

}




