<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class ProductDiscount extends Model
{
    protected $table = 'product_discont_code';
    public $timestamps = false;
    protected $fillable = [
        'code',
        'price',
        'status', // 0 باز  1 استفاده شده
        'product_id',   //  برای چه محصولی استفاده شده
        'user_id'  //  چه کاربری استفاده کرده
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
