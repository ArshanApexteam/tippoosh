<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $table = 'basket';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'user_id',
        'cookie_key',
        'product_id',
        'count',
        'address',
        'size',
        'color',
        'phone',
        'photo',
        'discount_code'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'cookie_key'
    ];


    /**
     * user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * sendMethod
     */
    public function sendMethod()
    {
        return $this->belongsTo(ProductSendMethod::class, 'send_method_id');
    }

    /**
     * packags
     */
    public function package()
    {
        return $this->belongsTo(ProductPackage::class, 'package_id');
    }
}
