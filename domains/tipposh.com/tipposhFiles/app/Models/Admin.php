<?php

namespace App\Models;

use App\Notifications\Admin\Auth\ResetPassword;
use App\Notifications\Admin\Auth\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


use Auth;


class Admin extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone' , 'address' ,'imageProfile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function checkAdminPhoto()
    {
        if (file_exists("assets/images/user/admins/$this->id.jpg"))
            return 'true';
    }

          //  مجوز ها
    public function permision()
    {
        return $this->hasMany(Ad_Admin_Rule::class, 'admin_id');
    }


      //  بررسی مجوز 
    public function checkAdminPermision($action)
    {
        if(Ad_Permissios::where('action' , $action)->first()){
            $permission_id = Ad_Permissios::where('action' , $action)->first()->id;
            $admin_rule = Ad_Admin_Rule::where('permission_id' , $permission_id)->where('admin_id' , $this->id)->first();
            if(!$admin_rule)
                return false;
            return true;
        }
        return false;
    }


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
    public function advertises(){
        return $this->hasMany(Advertise::class,'customer_id','id');
    }


}
