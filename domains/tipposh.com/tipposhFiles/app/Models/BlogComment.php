<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $table = 'blog_comment';
    // public $timestamps = false;
    protected $fillable = [
        'blog_id',
        'user_id',
        'parent_id',
        'comment',
        'status'
    ];

    //  Auther
    public function user()
    {
        if (User::find($this->user_id))
            return $this->belongsTo(User::class);
        else if(Taskmaster::find($this->user_id))
            return $this->belongsTo(Taskmaster::class, 'user_id');
        else
            return $this->belongsTo(Admin::class, 'user_id');

    }

    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(__CLASS__, 'parent_id', 'id');
    }

    /**
     *
     */
    public function Childs()
    {
        return $this->hasMany(BlogComment::class , 'parent_id')->where('status', 1);
    }

    public function allComments()
    {
        return $this->where('parent_id', 0)->where('status', 1)->get();
    }
}
