<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad_Admin_Rule extends Model
{
    //  جدول تعریف سطح دسترسی برای مدیران ، در این جدول برای هر مدیر سطوح دسترسی را مشخص میکنیم
    protected $table = 'ad_admin_rule';
    public $timestamps = false;
    protected $fillable = ['permission_id' , 'admin_id'];


    public function admin(){
        return $this->belongsTo(Admin::class, 'admin_id');
    }


    public function action(){
        return $this->belongsTo(Ad_Permissios::class, 'permission_id');
    }

}
