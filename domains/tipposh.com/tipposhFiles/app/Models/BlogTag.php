<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    protected $table = 'blog_tag';
    public $timestamps = false;
    protected $fillable = ['blog_id', 'caption', 'status'];
}
