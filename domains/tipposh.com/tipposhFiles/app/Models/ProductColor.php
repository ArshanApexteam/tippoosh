<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class ProductColor extends Model
{
    protected $table = 'product_color';
    public $timestamps = false;
    protected $fillable = ['product_id', 'color'];


    protected $hidden = [
        'product_id',
    ];
}
