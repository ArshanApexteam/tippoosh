<?php

declare(strict_types=1);

namespace App\Models;

use Auth;
use App\GraphQL\Types\Product as TypesProduct;
use App\Models\ProductDiscount;
use App\Models\Sale;
use App\Models\Featuer;
use App\Models\BasketMeta;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Product extends Model
{
    protected $table = 'product';
    public $timestamps = false;
    protected $fillable = [
        'name_fa',
        'name_en',
        'status',
        'store',
        'price',
        'category_id',
        'offer',
        'description',
        'metakeywords',
        'metaDescription',
        'slug',
        'summery',
        'material',
    ];


    /**
     ****    new Products
     */
    public function newProducts()
    {
        return $this::with(['gallery'])->where('status', 1)->orderBy('id', 'desc')->take(20)->get();
    }


    /**
     ****    top Products
     */
    public function topProducts()
    {
        /* گروه بندی  جدول فروش
        $sale = Sale::all()->groupBy('product_id');

        //   مرتب سازی براساس بیشترین فروش
        $sortByDesc = $sale->sortByDesc(function ($item, $key) {
            return count($item);
        });

        // استخراج شناسه جدول محصول از خروجی جدول فروش
        $id = [];
        foreach ($sortByDesc as $proc)
            $id[] = $proc[0]->product_id;

        //  پیدا کردن محصولات از جدول محصولات
        $products = [];
        for ($i = 0; $i < count($id) && $i < 30; $i++) {
            $products[] = Product::with(['gallery'])->find($id[$i]);
        }*/
        $products = Product::orderBy('id', 'desc')->take(10)->get();
        return $products;
    }
    public function mazhabi()
    {
        
        $catman = Product::where('category_id', '73')->get();
        return $catman;
    }
    public function lash()
    {
        
        $catman = Product::where('category_id', '72')->get();
        return $catman;
    }
    public function catman()
    {
        
        $catman = Product::where('category_id', '64')->get();
        return $catman;
    }
    public function catwman()
    {
        
        $catwman = Product::where('category_id', '70')->get();
        return $catwman;
    }

    /**
     ****    check Product Is New
     */
    public function checkProductIsNew()
    {
        $all = Product::orderBy('id', 'desc')->take(15)->get();
        foreach ($all as $item) {
            if ($item->id == $this->id)
                return true;
        }
        return false;
    }

    /**
     ****    check Product IN Basket
     */
    public function checkProductInBasket()
    {
        $userCookieKey = $this::userCookieKey();

        $user_id = 0;
        if (Auth::guard('web')->user())
            if (Basket::where('product_id', $this->id)
            ->Where('user_id', Auth::guard('web')->user()->id)
            ->first()) {
            return true;
        }
        
        if (Basket::where('cookie_key', $userCookieKey)
            ->where('product_id', $this->id)
            ->first()) {
            return true;
        }
        
        return false;
    }


    /**
     ****    product Offer
     */
    public function productOffer()
    {
        return $this->where('offer' , '>' , 0 )->orderBy('offer', 'desc')->take(6)->get();
    }


    /**
     ****    related Products
     */
    public function relatedProducts()
    {
        return Product::where('category_id', $this->category_id)
            ->where('id', '!=', $this->id)->where('status', 1)
            ->orderBy('id', 'desc')->take(15)
            ->get();
    }


    /**
     ****    result Price
     */
    public function totalPrice()
    {
        //   پیدا کردن کلمه کلید کاربر
        $userCookieKey = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $userCookieKey = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($userCookieKey), time() + 8400000 * 10, '/');

        $time = time();
        $offer = 0;
        if (ProductDiscount::where('start_date', '<', $time)->where('end_date', '>', $time)->where('product_id', $this->id)->first())
            $offer = ProductDiscount::where('start_date', '<', $time)->where('end_date', '>', $time)->where('product_id', $this->id)->first()->offer;


        if (Basket::where('cookie_key', $userCookieKey)->where('product_id', $this->id)->first()) {
            $bascket = Basket::where('cookie_key', $userCookieKey)->where('product_id', $this->id)->first();
            $basketMetas = BasketMeta::where('basket_id', $bascket->id)->get();
            $x = 0;
            foreach ($basketMetas as $item) {
                $x += $item->productFeatuer->price;
            }
            $sendMethod = 0;
            if (isset($bascket->sendMethod->price))
                $sendMethod = $bascket->sendMethod->price;

            $package = 0;
            if (isset($bascket->package->price))
                $package = $bascket->package->price;

            return ($this->price + $x + $sendMethod + $package - $offer);
        }

        return ($this->price - $offer);
    }


    /**
     ****    liked
     */
    public function liked()
    {
        if (isset($_COOKIE['userProductLikes'])) {
            $products = json_decode($_COOKIE['userProductLikes'], true);
            foreach ($products as $item) {
                if ($item == $this->id) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     ****   product Price featuer for frontend
     */
    public function featuersPrice()
    {
        return Featuer::with(['productFeatuer' => function ($query) {
            $query->where('product_id', $this->id);
        }])->get();
    }


    /**
     ****    Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     ****    gallery
     */
    public function gallery()
    {
        return $this->hasMany(ProductGallery::class);
    }

    /**
     ****    color
     */
    public function color()
    {
        return $this->hasMany(ProductColor::class);
    }


    /**
     ****    Rate
     */
    public function rate()
    {
        $productRate = ProductComment::where("product_id", $this->id)->get();
        if (isset($productRate[0])) {
            $sumRate = 0;
            foreach ($productRate as $item) {
                $sumRate += (($item->tarahi + $item->pricing + $item->rezaiatMoshtari + $item->quentity) / 4);
            }
            return (int)($sumRate / count($productRate));
//            return ($sumRate / count($productRate) * 100 / 5);
        } else
            return false;
    }

    public function rateCount()
    {
        return ProductComment::where("product_id", $this->id)->count();
    }


    /**
     ****    comments
     */
    public function comments()
    {
        return $this->hasMany(ProductComment::class)->where('status', 1)->where('parent_id', 0);
    }


    /**
     ****    tags
     */
    public function tags()
    {
        return $this->hasMany(ProductTag::class);
    }


    //######################################################################################      userCookieKey
    function userCookieKey()
    {
        $key = substr(str_shuffle('abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 30);
        if (isset($_COOKIE['userCookieKey']) && $_COOKIE['userCookieKey'] != null)
            $key = json_decode($_COOKIE['userCookieKey'], true);
        else
            setcookie('userCookieKey', json_encode($key), time() + 8400000 * 10, '/');
        return $key;
    }


}
