<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class Blog extends Model
{
    protected $table = 'blog';
    protected $fillable = [
        'title',
        'slug',
        'admin_id',
        'description',
        'summary' ,
        'status',
        'category_id',
        'image_alt',
        'iamge_title',
        'key_word',
        'index_photo',
        'main_photo'
    ];


    /**
     ****    related Products
     */
    public function relatedPost()
    {
        return Blog::where('category_id', $this->skill_id)
            ->where('id', '!=', $this->id)->where('status', 1)
            ->orderBy('id', 'desc')->take(10)
            ->get();
    }


    //    نظرات پست
    public function blogComment()
    {
        return $this->hasMany(BlogComment::class)->where('status',1)->where('parent_id' , 0);
    }

    //    عکسهای پست
    public function blogImages()
    {
        return $this->hasMany(BlogImages::class);
    }

    //   پست های اخیر     
    public function newBlogs()
    {
        return $this->where('status', 1)->orderBy('id', 'asc')->take(10)->get();
    }

    //  برچسب های پست
    public function blogTag()
    {
        return $this->hasMany(BlogTag::class);
    }

    //    ناشر
    public function auther()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }

    //    دسته بندی
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
