<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingItems extends Model
{
    protected $table = 'settings_items';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fa_name',
        'en_name',
    ];
}
