<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Brand extends Model
{
    protected $table = 'brand';
    public $timestamps = false;
    protected $fillable = ['caption', 'photo'];
}
