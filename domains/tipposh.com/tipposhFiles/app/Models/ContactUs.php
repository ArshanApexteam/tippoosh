<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class ContactUs extends Model
{
    protected $table = 'contactus';
    protected $fillable = [
        'user_name',
        'user_email',
        'user_phone',
        'user_msg' ,
    ];

}
