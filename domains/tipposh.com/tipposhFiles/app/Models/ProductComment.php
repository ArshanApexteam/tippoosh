<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $table = 'product_comment';
    public $timestamps = false;
    protected $fillable = [
        'product_id',
        'user_id',
        'user_name',
        'user_mail',
        'cookie_key',
        'parent_id',
        'comment',
        'status',
        'user_is_admin',
        
        'tarahi',
        'pricing',
        'rezaiatMoshtari',
        'quentity',

    ];


    //  Auther
    public function user()
    {
        if ($this->user_is_admin == 0)
            return $this->belongsTo(User::class);
        else
            return $this->belongsTo(Admin::class, 'user_id');

    }

    //  product
    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(__CLASS__, 'parent_id', 'id');
    }

    /**
     *   
     */
    public function Childs()
    {
        return $this->hasMany(ProductComment::class, 'parent_id')->where('status', 1);
    }
}
