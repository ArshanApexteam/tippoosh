<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class ProductTag extends Model
{
    protected $table = 'product_tag';
    public $timestamps = false;
    protected $fillable = ['product_id', 'caption'];


}
