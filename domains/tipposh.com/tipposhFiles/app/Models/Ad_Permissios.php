<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad_Permissios extends Model
{
    //    جدول مجوزها برای سطح دسترس مدیران در این جدول تمام مجوز ها قرار میگیرند و فقط داری فیلد مجوز می باشد
    protected $table = 'ad_permissions';
    public $timestamps = false;
    protected $fillable = ['action'];

}
