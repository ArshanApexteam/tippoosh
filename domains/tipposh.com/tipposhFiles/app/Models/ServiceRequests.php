<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class ServiceRequests extends Model
{
    protected $table = 'service_request';
    protected $fillable = [
        'user_id',
        'cookie_key',
        'mail',
        'phone',
        'service_id',
        'name',
        'status',  //  0 => sabt    ,  1 => taiid  , 2 => ok
        'summary'
    ];

    /**
     * user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Service
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
