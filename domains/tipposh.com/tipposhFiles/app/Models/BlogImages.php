<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class BlogImages extends Model
{
    protected $table = 'blog_images';
    public $timestamps = false;
    protected $fillable = ['blog_id' , 'photo'];
}