<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class Service extends Model
{
    protected $table = 'service';
    public $timestamps = false;
    protected $fillable = [
        'title',
        'status',
        'summary' ,
        'description',
        'image_medium',
        'image_big',
        'slug',
        'icon',
        'price'
    ];
}
