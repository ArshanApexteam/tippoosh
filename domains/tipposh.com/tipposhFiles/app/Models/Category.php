<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Product;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Category extends Model
{
    protected $table = 'category';
    public $timestamps = false;
    protected $fillable = ['name', 'status', 'parent_id', 'slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        if (!Category::find($this->parent_id))
            return false;
        return Category::find($this->parent_id);
    }


    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id')->where('status', 1);
    }

    /**
     *  تمام محصولات یک دسته بندی
     */
    public function products()
    {
        return Product::where('status', 1)->where('category_id', $this->id)->orderBy('id', 'desc')->paginate(12);
    }

    /**
     *  بازگردانی تمام پست های یک دسته بندی
     */
    public function blogsOfCate()
    {
        return $this->hasMany(Blog::class)->where('status', 1)->orderBy('id', 'desc')->take(15);
    }

}
