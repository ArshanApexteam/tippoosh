<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sale';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'user_id',
        'product_id',
        'count',
        'address',
        'color',
        'size',
        'phone',
        'photo',
        'discount_code'
    ];


    /**
     * user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
