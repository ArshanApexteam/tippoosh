<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Carousel extends Model
{
    protected $table = 'carousel';
    public $timestamps = false;
    protected $fillable = [
        'name_fa',
        'name_en',
        'price',
        'title',
        'description',
        'status',
        'photo'
    ];

}
