<div class="container productSectionTwo">
    <div class="row m-0">


        <div class="col-md-12 col-12 p-0">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#home" role="tab"
                       aria-controls="home" aria-selected="false"> <i class="fa fa-list"></i> توضیحات کامل </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link show" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                       aria-controls="profile" aria-selected="false"><i class="fa fa-archive"></i> مشخصات محصول </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                       aria-controls="contact" aria-selected="true"><i class="fa fa-comment"></i> نظرات کاربران </a>
                </li>
            </ul>
        </div>

        <div class="col-md-12 col-12 px-5 px-md-2 tabContentSection">
            <div class="tab-content mt-3" id="myTabContent">
                <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @php
                        echo $product->description;
                    @endphp
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row mb-5">
                        <div class="col-md-4 col-sm-12 reating-area">
                            <h3>امتیاز بندی</h3>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 text-right">
                                    <div class="rating-info">
                                        <span> طراحی : </span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 text-left px-md-0">
                                    <div class="star-rating">
                                        <span class="fa fa-star rating_1" data-rating="1"></span>
                                        <span class="fa fa-star rating_1" data-rating="2"></span>
                                        <span class="fa fa-star rating_1" data-rating="3"></span>
                                        <span class="fa fa-star rating_1" data-rating="4"></span>
                                        <span class="fa fa-star rating_1" data-rating="5"></span>
                                        <input type="hidden" id="tarahi" class="rating-value" value="3">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 text-right">
                                    <div class="rating-info">
                                        <span> ارزش خرید : </span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 text-left px-md-0">
                                    <div class="star-rating">
                                        <span class="fa fa-star  rating_2" data-rating="1"></span>
                                        <span class="fa fa-star  rating_2" data-rating="2"></span>
                                        <span class="fa fa-star  rating_2" data-rating="3"></span>
                                        <span class="fa fa-star  rating_2" data-rating="4"></span>
                                        <span class="fa fa-star  rating_2" data-rating="5"></span>
                                        <input type="hidden" id="pricing" class="rating-value" value="3">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 text-right">
                                    <div class="rating-info">
                                        <span> رضایت مشتری : </span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 text-left px-md-0">
                                    <div class="star-rating">
                                        <span class="fa fa-star  rating_3" data-rating="1"></span>
                                        <span class="fa fa-star  rating_3" data-rating="2"></span>
                                        <span class="fa fa-star  rating_3" data-rating="3"></span>
                                        <span class="fa fa-star  rating_3" data-rating="4"></span>
                                        <span class="fa fa-star  rating_3" data-rating="5"></span>
                                        <input type="hidden" id="rezaiatMoshtari" class="rating-value" value="3">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-12 text-right">
                                    <div class="rating-info">
                                        <span> کیفیت ساخت : </span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 text-left  px-md-0">
                                    <div class="star-rating">
                                        <span class="fa fa-star  rating_4" data-rating="1"></span>
                                        <span class="fa fa-star  rating_4" data-rating="2"></span>
                                        <span class="fa fa-star  rating_4" data-rating="3"></span>
                                        <span class="fa fa-star  rating_4" data-rating="4"></span>
                                        <span class="fa fa-star  rating_4" data-rating="5"></span>
                                        <input type="hidden" id="quentity" class="rating-value" value="3">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-8 col-sm-12" id="boxCommenets">
                            <h3>ارسال دیدگاه جدید</h3>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input type="text" id="user_name" class="form-control" placeholder="نام کامل">
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <input type="emal" id="user_mail" class="form-control" placeholder="ایمیل">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"
                                      placeholder="دیدگاه خود را اینجا بنویسید"></textarea>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                                    <button class="btn userCommentBtn">ارسال پیام <i class="fa fa-long-arrow-left"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="parent_id" id="parent_id">
                    <div class="userComments">

                        @foreach($product->comments as $comment)
                            <div class="commentItem row">
                                <div class="comment-box  col-md-8">
                                    <div class="thumb col-md-1 p-1">
                                        @php
                                            $imageProfile = $comment->user->imageProfile;
                                        @endphp
                                        @if (file_exists($imageProfile))
                                            <img src='{{ asset($imageProfile)}}' title="publisher" alt="کاربر"
                                                 class="user-image">
                                        @else
                                            <img src='{{ asset("assets/images/user/admins/none.png")}}'
                                                 title="publisher"
                                                 alt="کاربر" class="user-image">
                                        @endif
                                    </div>

                                    <div class="info col-md-11">
                                        @if($comment->user->name)
                                            <h3>{{$comment->user->name}}</h3>
                                        @else
                                            <h3>بدون نام</h3>
                                        @endif
                                        <a href="#boxCommenets" data-id="{{$comment->id}}" class="reply"><i
                                                    class="fa fa-share"></i>
                                            پاسخ</a>
                                        <span>{{ \Verta::instance($comment->created_at)->format('Y-m-d') }}</span>
                                    </div>

                                    <div class="comment col-md-12">
                                        <p>{{$comment->comment}}</p>
                                    </div>
                                </div>


                                <div class="col-md-4">

                                    @php
                                        if($comment->tarahi == 1 )
                                            $tarahi = "20%";
                                        elseif($comment->tarahi == 2 )
                                            $tarahi = "40%";
                                        elseif($comment->tarahi == 3 )
                                            $tarahi = "60%";
                                        elseif($comment->tarahi == 4 )
                                            $tarahi = "80%";
                                        else
                                            $tarahi = "100%";


                                        if($comment->pricing == 1 )
                                            $pricing = "20%";
                                        elseif($comment->pricing == 2 )
                                            $pricing = "40%";
                                        elseif($comment->pricing == 3 )
                                            $pricing = "60%";
                                        elseif($comment->pricing == 4 )
                                            $pricing = "80%";
                                        else
                                            $pricing = "100%";


                                        if($comment->rezaiatMoshtari == 1 )
                                            $rezaiatMoshtari = "20%";
                                        elseif($comment->rezaiatMoshtari == 2 )
                                            $rezaiatMoshtari = "40%";
                                        elseif($comment->rezaiatMoshtari == 3 )
                                            $rezaiatMoshtari = "60%";
                                        elseif($comment->rezaiatMoshtari == 4 )
                                            $rezaiatMoshtari = "80%";
                                        else
                                            $rezaiatMoshtari = "100%";

                                        if($comment->quentity == 1 )
                                            $quentity = "20%";
                                        elseif($comment->quentity == 2 )
                                            $quentity = "40%";
                                        elseif($comment->quentity == 3 )
                                            $quentity = "60%";
                                        elseif($comment->quentity == 4 )
                                            $quentity = "80%";
                                        else
                                            $quentity = "100%";

                                    @endphp

                                    <span>  طراحی : </span>
                                    <div class="progress m-b-20" style="height: 3px;">
                                        <div class="progress-bar" role="progressbar"
                                             style="width:{{$tarahi}}" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100">
                                        </div>
                                    </div>

                                    <br>

                                    <span> رضایت مشتری : </span>
                                    <div class="progress m-b-20" style="height: 3px;">
                                        <div class="progress-bar" role="progressbar"
                                             style="width: {{$pricing}};" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100">
                                        </div>
                                    </div>

                                    <br>

                                    <span> کیفیت ساخت : </span>
                                    <div class="progress m-b-20" style="height: 3px;">
                                        <div class="progress-bar" role="progressbar"
                                             style="width: {{$rezaiatMoshtari}};" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100">
                                        </div>
                                    </div>

                                    <br>

                                    <span> کیفیت ساخت : </span>
                                    <div class="progress m-b-20" style="height: 3px;">
                                        <div class="progress-bar" role="progressbar"
                                             style="width: {{$quentity}};" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            @isset($comment->Childs)
                                @foreach($comment->Childs as $chaild_1)
                                    <div class="commentItem row">
                                        <div class="comment-box  col-md-8">
                                            <div class="thumb col-md-1 p-1">
                                                @php
                                                    $imageProfile = $chaild_1->user->imageProfile;
                                                @endphp
                                                @if (file_exists($imageProfile))
                                                    <img src='{{ asset($imageProfile)}}' title="publisher"
                                                         alt="کاربر"
                                                         class="user-image">
                                                @else
                                                    <img src='{{ asset("assets/images/user/admins/none.png")}}'
                                                         title="publisher" alt="کاربر" class="user-image">
                                                @endif
                                            </div>

                                            <div class="info col-md-11">
                                                @if($chaild_1->user->name)
                                                    <h3>{{$chaild_1->user->name}}</h3>
                                                @else
                                                    <h3>بدون نام</h3>
                                                @endif
                                                <a href="#boxCommenets" data-id="{{$chaild_1->id}}" class="reply"><i
                                                            class="fa fa-share"></i> پاسخ</a>
                                                <span>{{ \Verta::instance($chaild_1->created_at)->format('Y-m-d') }}</span>
                                            </div>

                                            <div class="comment col-md-12">
                                                <p>{{$chaild_1->comment}}</p>
                                            </div>
                                        </div>


                                        <div class="col-md-4">

                                            @php
                                                if($chaild_1->tarahi == 1 )
                                                    $tarahi = "20%";
                                                elseif($chaild_1->tarahi == 2 )
                                                    $tarahi = "40%";
                                                elseif($chaild_1->tarahi == 3 )
                                                    $tarahi = "60%";
                                                elseif($chaild_1->tarahi == 4 )
                                                    $tarahi = "80%";
                                                else
                                                    $tarahi = "100%";


                                                if($chaild_1->pricing == 1 )
                                                    $pricing = "20%";
                                                elseif($chaild_1->pricing == 2 )
                                                    $pricing = "40%";
                                                elseif($chaild_1->pricing == 3 )
                                                    $pricing = "60%";
                                                elseif($chaild_1->pricing == 4 )
                                                    $pricing = "80%";
                                                else
                                                    $pricing = "100%";


                                                if($chaild_1->rezaiatMoshtari == 1 )
                                                    $rezaiatMoshtari = "20%";
                                                elseif($chaild_1->rezaiatMoshtari == 2 )
                                                    $rezaiatMoshtari = "40%";
                                                elseif($chaild_1->rezaiatMoshtari == 3 )
                                                    $rezaiatMoshtari = "60%";
                                                elseif($chaild_1->rezaiatMoshtari == 4 )
                                                    $rezaiatMoshtari = "80%";
                                                else
                                                    $rezaiatMoshtari = "100%";

                                                if($chaild_1->quentity == 1 )
                                                    $quentity = "20%";
                                                elseif($chaild_1->quentity == 2 )
                                                    $quentity = "40%";
                                                elseif($chaild_1->quentity == 3 )
                                                    $quentity = "60%";
                                                elseif($chaild_1->quentity == 4 )
                                                    $quentity = "80%";
                                                else
                                                    $quentity = "100%";

                                            @endphp

                                            <span>  طراحی : </span>
                                            <div class="progress m-b-20" style="height: 3px;">
                                                <div class="progress-bar" role="progressbar"
                                                     style="width:{{$tarahi}}" aria-valuenow="25" aria-valuemin="0"
                                                     aria-valuemax="100">
                                                </div>
                                            </div>

                                            <br>

                                            <span> رضایت مشتری : </span>
                                            <div class="progress m-b-20" style="height: 3px;">
                                                <div class="progress-bar" role="progressbar"
                                                     style="width: {{$pricing}};" aria-valuenow="25" aria-valuemin="0"
                                                     aria-valuemax="100">
                                                </div>
                                            </div>

                                            <br>

                                            <span> کیفیت ساخت : </span>
                                            <div class="progress m-b-20" style="height: 3px;">
                                                <div class="progress-bar" role="progressbar"
                                                     style="width: {{$rezaiatMoshtari}};" aria-valuenow="25"
                                                     aria-valuemin="0"
                                                     aria-valuemax="100">
                                                </div>
                                            </div>

                                            <br>

                                            <span> کیفیت ساخت : </span>
                                            <div class="progress m-b-20" style="height: 3px;">
                                                <div class="progress-bar" role="progressbar"
                                                     style="width: {{$quentity}};" aria-valuenow="25" aria-valuemin="0"
                                                     aria-valuemax="100">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    @isset($chaild_1->Childs)
                                        @foreach($chaild_1->Childs as $chaild_2)
                                            <div class="commentItem row">
                                                <div class="comment-box  col-md-8">
                                                    <div class="thumb col-md-1 p-1">
                                                        @php
                                                            $imageProfile = $chaild_2->user->imageProfile;
                                                        @endphp
                                                        @if (file_exists($imageProfile))
                                                            <img src='{{ asset($imageProfile)}}' title="publisher"
                                                                 alt="کاربر" class="user-image">
                                                        @else
                                                            <img src='{{ asset("assets/images/user/admins/none.png")}}'
                                                                 title="publisher" alt="کاربر" class="user-image">
                                                        @endif
                                                    </div>

                                                    <div class="info col-md-11">
                                                        @if($chaild_2->user->name)
                                                            <h3>{{$chaild_2->user->name}}</h3>
                                                        @else
                                                            <h3>بدون نام</h3>
                                                        @endif
                                                        <span>{{ \Verta::instance($chaild_2->created_at)->format('Y-m-d') }}</span>
                                                    </div>

                                                    <div class="comment col-md-12">
                                                        <p>{{$chaild_2->comment}}</p>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">

                                                    @php
                                                        if($chaild_2->tarahi == 1 )
                                                            $tarahi = "20%";
                                                        elseif($chaild_2->tarahi == 2 )
                                                            $tarahi = "40%";
                                                        elseif($chaild_2->tarahi == 3 )
                                                            $tarahi = "60%";
                                                        elseif($chaild_2->tarahi == 4 )
                                                            $tarahi = "80%";
                                                        else
                                                            $tarahi = "100%";


                                                        if($chaild_2->pricing == 1 )
                                                            $pricing = "20%";
                                                        elseif($chaild_2->pricing == 2 )
                                                            $pricing = "40%";
                                                        elseif($chaild_2->pricing == 3 )
                                                            $pricing = "60%";
                                                        elseif($chaild_2->pricing == 4 )
                                                            $pricing = "80%";
                                                        else
                                                            $pricing = "100%";


                                                        if($chaild_2->rezaiatMoshtari == 1 )
                                                            $rezaiatMoshtari = "20%";
                                                        elseif($chaild_2->rezaiatMoshtari == 2 )
                                                            $rezaiatMoshtari = "40%";
                                                        elseif($chaild_2->rezaiatMoshtari == 3 )
                                                            $rezaiatMoshtari = "60%";
                                                        elseif($chaild_2->rezaiatMoshtari == 4 )
                                                            $rezaiatMoshtari = "80%";
                                                        else
                                                            $rezaiatMoshtari = "100%";

                                                        if($chaild_2->quentity == 1 )
                                                            $quentity = "20%";
                                                        elseif($chaild_2->quentity == 2 )
                                                            $quentity = "40%";
                                                        elseif($chaild_2->quentity == 3 )
                                                            $quentity = "60%";
                                                        elseif($chaild_2->quentity == 4 )
                                                            $quentity = "80%";
                                                        else
                                                            $quentity = "100%";

                                                    @endphp

                                                    <span>  طراحی : </span>
                                                    <div class="progress m-b-20" style="height: 3px;">
                                                        <div class="progress-bar" role="progressbar"
                                                             style="width:{{$tarahi}}" aria-valuenow="25"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100">
                                                        </div>
                                                    </div>

                                                    <br>

                                                    <span> رضایت مشتری : </span>
                                                    <div class="progress m-b-20" style="height: 3px;">
                                                        <div class="progress-bar" role="progressbar"
                                                             style="width: {{$pricing}};" aria-valuenow="25"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100">
                                                        </div>
                                                    </div>

                                                    <br>

                                                    <span> کیفیت ساخت : </span>
                                                    <div class="progress m-b-20" style="height: 3px;">
                                                        <div class="progress-bar" role="progressbar"
                                                             style="width: {{$rezaiatMoshtari}};" aria-valuenow="25"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100">
                                                        </div>
                                                    </div>

                                                    <br>

                                                    <span> کیفیت ساخت : </span>
                                                    <div class="progress m-b-20" style="height: 3px;">
                                                        <div class="progress-bar" role="progressbar"
                                                             style="width: {{$quentity}};" aria-valuenow="25"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    @endisset
                                @endforeach
                            @endisset
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>