<!-- Begin Umino's Single Product Area -->
<div class="information-area">
    <div class="container">
        <div class="sp-nav">
            <div class="row m-0">
                <div class="col-lg-5 directionLTR gallerySection">
                    @php
                        $gallery = $product->gallery;
                        $img = $gallery->first()->photo;
                    @endphp
                    <div class="zoompro-border">
                        <img class="zoompro" src="{{asset($img)}}" data-zoom-image="{{asset($img)}}"
                             alt="Product Image"/>
                    </div>


                    <div id="gallery" class="sp-img_slider slider-navigation_style-4">
                        @foreach ($product->gallery as $key => $item)
                            <a class="@if($key == 0) active @endif"
                               ata-image="{{asset($item->photo)}}"
                               data-zoom-image="{{asset($item->photo)}}">
                                <img src="{{asset($item->photo)}}" alt="Product Image">
                            </a>
                        @endforeach
                    </div>
                </div>


                <div class="col-lg-7 p-info-section">
                    <div class="sp-content">
                        <div class="row topSection">

                            <div class="col-md-6 col-xs-12 text-right">
                                <h1> {{$product->name_fa}} </h1>
                                <h2> {{$product->name_en}} </h2>
                            </div>
                            <div class="col-md-6 col-xs-12 text-left">
                                    <span>
                                        <i class="fa fa-heart @if($product->liked()) font-weight-900  @endif "
                                           @click="like({{$product->id}} , $event)"></i>
                                        افزودن به علاقه مندی ها
                                </span>
                                <div class="rating-box directionLTR">
                                    <ul>
                                        <li @if($product->rate() > 0)class="" @else class="none" @endif><i
                                                class="fa fa-star 1"></i></li>
                                        <li @if($product->rate() > 1)class="" @else class="none"@endif><i
                                                class="fa fa-star 2"></i></li>
                                        <li @if($product->rate() > 2)class="" @else class="none"@endif><i
                                                class="fa fa-star 3"></i></li>
                                        <li @if($product->rate() > 3)class="" @else class="none"@endif><i
                                                class="fa fa-star 4"></i></li>
                                        <li @if($product->rate() > 4)class="" @else class="none"@endif><i
                                                class="fa fa-star 5"></i></li>
                                    </ul>
                                    <div class="rating-info directionRTL">
                                        <a>({{$product->rateCount()}})</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <p><span>جنس : </span> <span class="red"> {{$product->material}} </span>
                                </p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <p><span>دسته : </span> <span class="red"> {{$product->category->name}} </span>
                                </p>
                            </div>

                            <hr>
                            <br>

                        </div>

                        <div class="row topSection">
                            <p class="width-100">{{$product->summery}}</p>
                            <hr>
                        </div>

                        <div class="row pt-3">
                            <form metoth="post" action="{{route('basket.addItem')}}">
                                @csrf
                                <div class="col-md-12 col-xs-12 mb-2">
                                    <p class="mb-2"><span class="color">رنگبندی : </span></p>
                                    @foreach ($product->color as $key => $item)
                                        <div class="radio-box">
                                            <input type="radio" name="color" id="color-{{$key}}" class="colorElement"
                                                   value="{{$item->color}}" @if($key == 0)checked @endif>
                                            <label for="color-{{$key}}" style="background : {{$item->color}}"></label>
                                        </div>
                                    @endforeach
                                </div>


                                <div class="col-md-12 col-xs-12">
                                    <p>سایز تیشرت : </p>
                                    <select name="size" id="size" class="form-control width-20">
                                        <option value="s">S</option>
                                        <option value="m">M</option>
                                        <option value="l">L</option>
                                        <option value="xl">XL</option>
                                        <option value="xxl">XXL</option>
                                        <option value="xxxl">XXXL</option>
                                    </select>
                                    <a href="" class="helpSize">راهنمای سایز بندی</a>

                                    <br>
                                    <br>

                                    <hr>

                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <p>تعداد سفارش : </p>
                                    <div class="quantity">
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" id="count" name="count" value="1"
                                                   type="text">
                                            <div class="dec qtybutton"><i class="fa fa-minus"></i></div>
                                            <div class="inc qtybutton"><i class="fa fa-plus"></i></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-xs-12 text-left p-4 px-md-1">
                                    @php
                                        if($product->offer > 0) {
                                            $offer = $product->offer ;
                                            $price = $product->price ;
                                            $result = $price - ( ($price * $offer) / 100) ;
                                        }
                                    @endphp
                                    <p class="price">
                                        @if($product->offer > 0)
                                            <span>{{$result}}</span>  <span> تومان </span>
                                        @else
                                            <span>{{$product->price}}</span>  <span> تومان </span>
                                        @endif

                                        @if($product->offer > 0)
                                            <span class="offer"> {{$product->offer}}% تخفیف</span>
                                        @endif
                                    </p>

                                    @if($product->offer > 0)
                                        <p class="lastPrice"> {{$price}} </p>
                                    @endif
                                </div>


                                <div class="col-md-12 col-xs-12 p-4">
                                    <a class="order_btn" href="{{route('order.new' , $product->id)}}">سفارشی سازی
                                        محصول</a>

                                    <input type="hidden" value={{$product->id}} name="product_id">

                                    <button type="submit" class="btn additional_btn">
                                        <i class="fa fa-shopping-basket"></i>
                                        <span>پرداخت</span>
                                    </button>
                                </div>
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<!-- Umino's Single Product Area End Here -->

