<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 sidebar">

    <form id="searchForm" action="{{route('search')}}" method="get">

        <input type="hidden" name="sort_key" id="sort_key" value="{{$data['sort_key']}}">
        <input type="hidden" name="paginate" id="paginate" value="{{$data['paginate']}}">
        <input type="hidden" name="isStore" id="isStore" value="{{$data['isStore']}}">
        <input type="hidden" name="serach" id="serach" value="{{$data['curentSearch']}}">


        <div class="sidebarItem">
            <div class="title accordion-label-section  accordion-active">
                <h5>دسته بندی ها</h5>
            </div>
            {{-- scrollbar scrollbar-style-1  --}}
            <div class="sidebar-categories_menu accordion-panel-section accordion-panel-open">
                <ul>
                    @foreach ($allCategoreis as $key => $item)
                        @if(count($item->Childs) > 0)
                            <li class="has-sub"><a href="javascript:void(0)">{{$item->name}} <i
                                            class="fa fa-angle-left"></i></a>
                                <ul>
                                    @foreach ($item->Childs as $subCategory)
                                        <li>
                                            <a href="{{route('category',$subCategory->slug)}}">{{$subCategory->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li><a href="{{route('category',$item->slug)}}">{{$item->name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="sidebarItem">
            <div class="title accordion-label-section">
                <h5>محدوده قیمت</h5>
            </div>
            <div class="price-filter accordion-panel-section">
                <div id="slider-range"></div>
                <div class="price-slider-amount">
                    <div class="label-input">
                        <input type="text" id="amount_max" name="amount_max"/>
                        <span>-</span>
                        <input type="text" id="amount_min" name="amount_min"/>
                    </div>
                </div>
            </div>
        </div>


        <div class="sidebarItem">
            <div class="title accordion-label-section">
                <h5>جنسیت محصول </h5>
            </div>
            <ul class="accordion-panel-section">
                <li>
                    <input type="radio" id="for-1" name="for" value="آقایان">
                    <label for="for-1">آقایان </label>
                </li>

                <li>
                    <input type="radio" id="for-2" name="for" value="بانوان">
                    <label for="for-2">بانوان </label>
                </li>

                <li>
                    <input type="radio" id="for-3" name="for" value="کودکان">
                    <label for="for-3">کودکان </label>
                </li>
                <li>
                    <input type="radio" id="for-4" name="for" value="کودک و نوجوان">
                    <label for="for-4">کودک و نوجوان </label>
                </li>
                <li>
                    <input type="radio" id="for-5" name="for" value="عمومی">
                    <label for="for-5">عمومی </label>
                </li>
            </ul>
        </div>


        <div class="sidebarItem">
            <div class="title accordion-label-section">
                <h5>رنگ بندی </h5>
            </div>
            <ul class="checkbox_list accordion-panel-section colorList">
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-1" name="color" value="#000000">
                        <label for="color-1" style="background : #000000"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-2" name="color" value="#ffffff">
                        <label for="color-2" style="background : #ffffff"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-3" name="color" value="#50cfff">
                        <label for="color-3" style="background : #50cfff"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-4" name="color" value="#ff0047">
                        <label for="color-4" style="background : #ff0047"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-5" name="color" value="#00ff1f">
                        <label for="color-5" style="background : #00ff1f"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-6" name="color" value="#fb00ff">
                        <label for="color-6" style="background : #fb00ff"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-7" name="color" value="#ff920b">
                        <label for="color-7" style="background : #ff920b"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-8" name="color" value="#b36f55">
                        <label for="color-8" style="background : #b36f55"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-9" name="color" value="#ffcd05">
                        <label for="color-9" style="background : #ffcd05"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-10" name="color" value="#a922ab">
                        <label for="color-10" style="background : #a922ab"> </label>
                    </div>
                </li>
                <li>
                    <div class="radio-box">
                        <input type="radio" id="color-11" name="color" value="#adadad">
                        <label for="color-11" style="background : #adadad"></label>
                    </div>
                </li>

            </ul>
        </div>


        <div class="sidebarItem">
            <div class="title accordion-label-section">
                <h5> سایز بندی </h5>
            </div>
            <ul class="accordion-panel-section">
                <li>
                    <input type="radio" id="size-1" name="size" value="S">
                    <label for="size-1">S </label>
                </li>

                <li>
                    <input type="radio" id="size-2" name="size" value="M">
                    <label for="size-2">M </label>
                </li>

                <li>
                    <input type="radio" id="size-3" name="size" value="L">
                    <label for="size-3">L </label>
                </li>
                <li>
                    <input type="radio" id="size-4" name="size" value="XL">
                    <label for="size-4">XL </label>
                </li>
                <li>
                    <input type="radio" id="size-5" name="size" value="XLL">
                    <label for="size-5">XXL </label>
                </li>
            </ul>
        </div>


        <div class="sidebarItem">
            <div class="title accordion-label-section">
                <h5> جنس محصول </h5>
            </div>
            <ul class="accordion-panel-section">
                <li>
                    <input type="radio" id="material-1" name="material" value="پنبه ای">
                    <label for="material-1">پنبه ای </label>
                </li>

                <li>
                    <input type="radio" id="material-2" name="material" value="نخی">
                    <label for="material-2">نخی </label>
                </li>

                <li>
                    <input type="radio" id="material-3" name="material" value="پلی اتیلن">
                    <label for="material-3">پلی اتیلن </label>
                </li>
                <li>
                    <input type="radio" id="material-4" name="material" value="بافت">
                    <label for="material-4">بافت </label>
                </li>
                <li>
                    <input type="radio" id="material-5" name="material" value="چرم">
                    <label for="material-5">چرم </label>
                </li>
            </ul>
        </div>



        <div class="sidebarItem">
            <input type="submit" class="btn btn-1" value="اعمال فیلتر">
        </div>

    </form>

</div>