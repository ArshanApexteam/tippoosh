<section id="dotSlider"></section>

<section id="mainSlider">
    <div class="container p-5">
        <div class="row">
            <div class="owl-carousel owl-theme dir-ltr">

                @foreach(\App\Models\Carousel::where('status' ,1)->get() as $item)
                    <div class="item">
                        <div class="row">
                            <div class="col-md-1"></div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
                                <img src="{{asset($item->photo)}}" alt="{{$item->name_fa}}" title="{{$item->name_fa}}">
                            </div>

                            <div class="col-12 col-sm-12 col-md-7 col-lg-7 info">
                                <h1 class="name_en">{{$item->name_en}}</h1>
                                <h1 class="name_fa">{{$item->name_fa}}</h1>
                                <h3>{{$item->title}}</h3>
                                <p>{{$item->description}}</p>
                                <span>{{$item->price}}</span>
                            </div>

                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
