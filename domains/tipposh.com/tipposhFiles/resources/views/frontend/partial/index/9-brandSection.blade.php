<section id="brandSection">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>
                    <span> برخی از</span>
                    <span class="color-red"> مشتریان </span>
                </h3>
                <hr>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">

                @foreach(\App\Models\Brand::all() as $item)
                    <div class="col">
                        <img src="{{asset($item->photo)}}" alt="{{$item->caption}}">
                    </div>
                @endforeach
            </div>

        </div>
    </div>

</section>