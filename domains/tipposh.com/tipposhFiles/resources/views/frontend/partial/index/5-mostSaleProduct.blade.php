<section id="mostSaleProduct">
    <div class="container-fluid head">
        <h3>
            <span>محصولات</span>
            <span class="color-red">پر فروش</span>
        </h3>
        <hr>
    </div>

    <div class="container">
        <div class="owl-carousel owl-theme dir-ltr">

            @foreach($data['topProducts'] as $item)
                @isset($item->id)
                    <div class="item">
                        <div class="row head">
                            <i class="fa fa-star"></i>
                            <b>{{$item->rate()}}</b>

                            @if($item->checkProductIsNew())
                                <span>جدید</span>
                            @endif
                            
                        </div>
                        <a href="{{route('product.show', $item->slug)}}">
                            <div class="row body">
                                @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                                @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                                @endisset
                            </div>
                        </a>
                        <div class="row footer">
                            <a href="">
                                <p>{{$item->name_fa}}</p>
                            </a>
                            <span> {{$item->price}} <b>تومان</b> </span>
                            <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                               @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                               title="اضافه کردن به سبد خرید"></i>
                        </div>
                    </div>
                @endisset
            @endforeach
            {{--v-on:click="addToBasket({{$product->id}})"--}}
        </div>
    </div>
</section>
<section id="mostSaleProduct">
    <div class="container-fluid head">
        <h3>
            <span>تیشرت</span>
            <span class="color-red">زنانه</span>
        </h3>
        <hr>
    </div>

    <div class="container">
        <div class="owl-carousel owl-theme dir-ltr">

            @foreach($data['catwman'] as $item)
                @isset($item->id)
                    <div class="item">
                        <div class="row head">
                            <i class="fa fa-star"></i>
                            <b>{{$item->rate()}}</b>

                            @if($item->checkProductIsNew())
                                <span>جدید</span>
                            @endif
                            
                        </div>
                        <a href="{{route('product.show', $item->slug)}}">
                            <div class="row body">
                                @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                                @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                                @endisset
                            </div>
                        </a>
                        <div class="row footer">
                            <a href="">
                                <p>{{$item->name_fa}}</p>
                            </a>
                            <span> {{$item->price}} <b>تومان</b> </span>
                            <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                               @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                               title="اضافه کردن به سبد خرید"></i>
                        </div>
                    </div>
                @endisset
            @endforeach
            {{--v-on:click="addToBasket({{$product->id}})"--}}
        </div>
    </div>
</section>
<section id="mostSaleProduct">
    <div class="container-fluid head">
        <h3>
            <span>تیشرت</span>
            <span class="color-red">مردانه</span>
        </h3>
        <hr>
    </div>

    <div class="container">
        <div class="owl-carousel owl-theme dir-ltr">

            @foreach($data['catman'] as $item)
                @isset($item->id)
                    <div class="item">
                        <div class="row head">
                            <i class="fa fa-star"></i>
                            <b>{{$item->rate()}}</b>

                            @if($item->checkProductIsNew())
                                <span>جدید</span>
                            @endif
                            
                        </div>
                        <a href="{{route('product.show', $item->slug)}}">
                            <div class="row body">
                                @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                                @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                                @endisset
                            </div>
                        </a>
                        <div class="row footer">
                            <a href="">
                                <p>{{$item->name_fa}}</p>
                            </a>
                            <span> {{$item->price}} <b>تومان</b> </span>
                            <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                               @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                               title="اضافه کردن به سبد خرید"></i>
                        </div>
                    </div>
                @endisset
            @endforeach
            {{--v-on:click="addToBasket({{$product->id}})"--}}
        </div>
    </div>
</section>
<section id="mostSaleProduct">
    <div class="container-fluid head">
        <h3>
            <span>تیشرت</span>
            <span class="color-red">مذهبی</span>
        </h3>
        <hr>
    </div>

    <div class="container">
        <div class="owl-carousel owl-theme dir-ltr">

            @foreach($data['mazhabi'] as $item)
                @isset($item->id)
                    <div class="item">
                        <div class="row head">
                            <i class="fa fa-star"></i>
                            <b>{{$item->rate()}}</b>

                            @if($item->checkProductIsNew())
                                <span>جدید</span>
                            @endif
                            
                        </div>
                        <a href="{{route('product.show', $item->slug)}}">
                            <div class="row body">
                                @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                                @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                                @endisset
                            </div>
                        </a>
                        <div class="row footer">
                            <a href="">
                                <p>{{$item->name_fa}}</p>
                            </a>
                            <span> {{$item->price}} <b>تومان</b> </span>
                            <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                               @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                               title="اضافه کردن به سبد خرید"></i>
                        </div>
                    </div>
                @endisset
            @endforeach
            {{--v-on:click="addToBasket({{$product->id}})"--}}
        </div>
    </div>
</section>
<section id="mostSaleProduct">
    <div class="container-fluid head">
        <h3>
            <span>تیشرت</span>
            <span class="color-red">لش</span>
        </h3>
        <hr>
    </div>

    <div class="container">
        <div class="owl-carousel owl-theme dir-ltr">

            @foreach($data['lash'] as $item)
                @isset($item->id)
                    <div class="item">
                        <div class="row head">
                            <i class="fa fa-star"></i>
                            <b>{{$item->rate()}}</b>

                            @if($item->checkProductIsNew())
                                <span>جدید</span>
                            @endif
                            
                        </div>
                        <a href="{{route('product.show', $item->slug)}}">
                            <div class="row body">
                                @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                                @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                                @endisset
                            </div>
                        </a>
                        <div class="row footer">
                            <a href="">
                                <p>{{$item->name_fa}}</p>
                            </a>
                            <span> {{$item->price}} <b>تومان</b> </span>
                            <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                               @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                               title="اضافه کردن به سبد خرید"></i>
                        </div>
                    </div>
                @endisset
            @endforeach
            {{--v-on:click="addToBasket({{$product->id}})"--}}
        </div>
    </div>
</section>