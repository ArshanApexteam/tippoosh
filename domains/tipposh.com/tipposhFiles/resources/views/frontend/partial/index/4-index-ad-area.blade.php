<section id="index-ad-area">
    <div class="container">
        @php
            $cate_1 = \App\Models\Category::where('name','مدل های ست زوج')->first();
            $cate_2 = \App\Models\Category::where('name', 'like', '%دو کلاه%')->first();
            $cate_3 = \App\Models\Category::where('name', 'like', '%تیشرت%')->first();
        @endphp

        <div class="row">
            <div class="col-md-4 col-12 ad">
                <h2>{{ \App\Models\Setting::where('key' ,"baner1_text" )->first()->value }}</h2>

                @isset($cate_1)
                    <a href="{{route('category',$cate_1->slug)}}">مشاهده</a>
                @endisset
            </div>
            <div class="col-md-4 col-12 ad">
{{--                <p>{{ \App\Models\Setting::where('key' ,"baner2_text" )->first()->value }}</p>--}}

                @isset($cate_2)
                    <a href="{{route('category',$cate_2->slug)}}" style="background-color: #e52a1f;border-radius: 100px; padding: 5px 35px; color: #fff;float: left;margin-top: 20px;font-size: 12px;">مشاهده</a>
                @endisset
            </div>
            <div class="col-md-4 col-12 ad">
                <h2>{{ \App\Models\Setting::where('key' ,"baner3_text" )->first()->value }}</h2>
                @isset($cate_3)
                    <a href="{{route('category',$cate_3->slug)}}">مشاهده</a>
                @endisset
            </div>
        </div>
    </div>
</section>
