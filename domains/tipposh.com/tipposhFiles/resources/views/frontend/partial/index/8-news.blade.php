<section id="news">
    <div class="container-fluid head text-center">
        <h3>
            <span>آخرین های</span>
            <span class="color-red"> وبلاگ </span>
        </h3>
        <hr>
    </div>

    <div class="dir-ltr width-my-owl">

        <div class="owl-carousel owl-carousel-blog owl-theme">

            @foreach($data['blog'] as $item)
                <div class="item">

                    <a href="{{route('blog.details' , $item->slug)}}">

                        <img src="{{asset($item->index_photo)}}" alt="{{$item->title}}" title="{{$item->title}}">

                        <h3>{{$item->title}}</h3>

                        <p>{{$item->summary}}</p>

                        {{--<span>5 <i class="fa fa-heart"></i></span>--}}
                    </a>

                </div>
            @endforeach

        </div>
    </div>
</section>