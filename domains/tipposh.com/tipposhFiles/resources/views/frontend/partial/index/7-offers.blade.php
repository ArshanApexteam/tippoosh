<section id="offers">
    <div class="container-fluid">
        <div class="row">
            <h1>داغ ترین تخفیف ها</h1>
        </div>

        <div class="row">

            @foreach($data['productOffer'] as $item)
                @isset($item)
                    <div class="col-md-2 col">
                        <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                        <div class="box row">
                            <h3>{{$item->offer}}% تخفیف</h3>
                            <p>{{$item->name_fa}}</p>
                            <a href="{{route('product.show', $item->slug)}}">مشاهده</a>
                        </div>
                    </div>
                @endisset
            @endforeach

        </div>

    </div>
</section>