<div class="row m-0">
    <div class="col-md-12 col-xs-12 p-0 mt-3 mb-3">

        <h1 class="title-1"> آرشیو محصولات </h1>
        <p class="guid"><a href="">صفحه اصلی</a> / <span>{{$data['curentCategory']->name}}</span></p>

    </div>
</div>

<div class="content-area">
    <div class="row m-0">
        <div class="col-md-12 col-sm-12  p-0 mt-3 mb-3 box-1 filterSection">

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 px-2">
                <label class="select-label"> مرتب سازی براساس : </label>
                <select class="nice-select order filteringAplay" id="el_sort_key">
                    <option value="1" @if($data['sort_key']==1) selected @endif>جدیدترین</option>
                    <option value="2" @if($data['sort_key']==2) selected @endif>قدیمی ترین</option>
                    <option value="3" @if($data['sort_key']==3) selected @endif>ارزانترین</option>
                    <option value="4" @if($data['sort_key']==4) selected @endif>گران ترین</option>
                </select>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 text-left">
                <label class="select-label">فقط نمایش کالاهای موجود : </label>
                <div class="wrapper float-left">
                    <div class="switch_box box_4">
                        <div class="input_wrapper">
                            <input type="checkbox" class="switch_4 filteringAplay" id="el_isStore"
                                   @if($data['isStore'] == 1) checked @endif>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-2 col-md-2 col-sm-12  col-xs-12 px-2">
                <label class="select-label"> نمایش : </label>
                <select class="nice-select filteringAplay" id="el_paginate">
                    <option value="24" @if($data['paginate']==24) selected @endif>24</option>
                    <option value="48" @if($data['paginate']==48) selected @endif>48</option>
                    <option value="72" @if($data['paginate']==72) selected @endif>72</option>
                    <option value="96" @if($data['paginate']==96) selected @endif>96</option>
                </select>
            </div>

        </div>
    </div>

    <div class="row productSection">
        @forelse ($data['allProducts'] as $item)

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="item">
                    <div class="row head">
                        <i class="fa fa-star"></i>
                        <b>{{$item->rate()}}</b>

                        @if($item->checkProductIsNew())
                            <span>جدید</span>
                        @endif
                    </div>
                    <a href="{{route('product.show', $item->slug)}}">
                        <div class="row body">
                            @isset($item->gallery[0])
                                <img src="{{asset($item->gallery[0]->photo)}}" alt="{{$item->name_fa}}">
                            @else
                                <img src="{{asset('assets/images/none.png')}}" alt="{{$item->name_fa}}">
                            @endisset
                        </div>
                    </a>
                    <div class="row footer">
                        <a href="">
                            <p>{{$item->name_fa}}</p>
                        </a>
                        <span> {{$item->price}} <b>تومان</b> </span>
                        <i class="fa fa-shopping-basket transition @if($item->checkProductInBasket()) bg-danger @endif"
                           @click="addOrRemoveFromBasket({{$item->id}} , $event)"
                           title="اضافه کردن به سبد خرید"></i>
                    </div>
                </div>
            </div>
        @empty
            <h1 class="notFound"> موردی یافت نشد</h1>
        @endforelse

    </div>

    <div class="row m-0">
        <div class="page-pagination">
            <nav aria-label="Pagination">
                {{ $data['allProducts']->links() }}
            </nav>
        </div>
    </div>

</div>