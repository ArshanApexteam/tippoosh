@extends('frontend.layout.app')

@section('title') راهنما @stop

@section('css') @stop
@section('js')@stop

@section('content')
    <div class="orderPage">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1>راهنمای کار کردن با تیپ پوش</h1>
                </div>

                <div class="col-md-12">
                   {{\App\Models\Setting::where('key' ,"aboutUs-description" )->first()->value}}
                </div>
            </div>

        </div>
    </div>

@stop