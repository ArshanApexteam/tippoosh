﻿@extends('frontend.layout.app')

@section('title') {{$product->name_fa}}  @endsection
@section('pageLink')   صفحه محصول   @stop
@section('pageTitle')   {{$product->name_fa}}   @stop


@section('css')

    <link rel="stylesheet" href="{{asset('assets/backend/vendors/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/pages/product.css')}}">

@endsection


@section('content')
    <div class="productPage">

        @include('frontend.partial.product.1-product-info')
        @include('frontend.partial.product.2-section-two')
        
    </div>

@endsection

@section('js')

    <script src="{{asset('assets/backend/vendors/slick/slick.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/pages/product.js')}}"></script>


    <script>
        $(document).ready(function () {

            // پاسخ دادن به پیام دیگران
            $('.reply').on('click', function () {
                $('#parent_id').val(($(this).attr('data-id')));
                $('#user_mail').addClass('activeTextBoxForAnswer');
                $('#comment').addClass('activeTextBoxForAnswer');
            });

            // ثبت نظر
            $('.userCommentBtn').on('click', function () {
                if ($('#user_mail').val() == "")
                    toastr["error"]("وارد کردن پست الکترونیکی الزامی می باشد");
                if ($('#comment').val() == "")
                    toastr["error"]("وارد کردن نظر الزامی می باشد");
                else {
                    $.get('{{ route("product.commentStore") }}', {
                        product_id: "{{$product->id }}",
                        parent_id: $('#parent_id').val(),
                        user_name: $('#user_name').val(),
                        user_mail: $('#user_mail').val(),
                        comment: $('#comment').val(),

                        tarahi: $('#tarahi').val(),
                        pricing: $('#pricing').val(),
                        rezaiatMoshtari: $('#rezaiatMoshtari').val(),
                        quentity: $('#quentity').val(),

                    }).done(function (result) {
                        $('#comment').val("").removeClass('activeTextBoxForAnswer');
                        $('#user_mail').val("").removeClass('activeTextBoxForAnswer');
                        $('#user_name').val("").removeClass('activeTextBoxForAnswer');
                        $('#parent_id').val("");
                        toastr["success"]("نظر شما با موفقیت ثبت گردید");
                    });
                }
            });
        });
    </script>

@endsection