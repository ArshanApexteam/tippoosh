@extends('frontend.layout.app')

@section('title') تماس با ما @stop

@section('css') @stop
@section('js')@stop

@section('content')
    <div class="orderPage">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1>راه های تماس با ما</h1>
                </div>

                <div class="col-12 col-md-4">
                    <p><b>آدرس : </b></p>
                    <p>{{\App\Models\Setting::where('key' ,"address" )->first()->value}}</p>
                </div>

                <div class="col-12 col-md-4">
                    <p><b>تلفن های تماس : </b></p>
                    <p>{{\App\Models\Setting::where('key' ,"cell-phone" )->first()->value}}</p>
                    <p>{{\App\Models\Setting::where('key' ,"phone" )->first()->value}}</p>
                </div>

                <div class="col-12 col-md-4">
                    <p><b>پست الکترونیکی : </b></p>
                    <p>{{\App\Models\Setting::where('key' ,"email" )->first()->value}}</p>
                </div>

                <div class="col-md-12 p-5 m-5">
                    @php
                        $photo =  \App\Models\Setting::where('key' ,"photo-location" )->first()->value;
                    @endphp

                    <img src="{{asset($photo)}}" alt="تی پوش" title="تی پوش">
                </div>

            </div>


        </div>
    </div>

@stop