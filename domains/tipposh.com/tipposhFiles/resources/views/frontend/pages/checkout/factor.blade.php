@extends('frontend.layout.app')

@section('title')  پیش فاکتور   @stop
@section('pageLink')  پیش فاکتور    @stop
@section('pageTitle')  پیش فاکتور    @stop

@section('css')@endsection

@section('content')
    <!-- Main Container -->
    <section id="factorPage" class="userPanelSection">
        <div class="container">
            <div class="row m-0">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <!--@if (session('paymentError'))-->
                    <!--<div class="alert alert-danger">-->
                    <!--        <p>{{session()->get('paymentError')}}</p>-->
                    <!--    </div>-->
                    <!--    {{session()->forget('paymentError')}}-->
                    <!--@endif-->
                    
                    <div class="card">
                        @if (session('userNotLogin'))
                            <div class="alert alert-danger">
                                {{ session('userNotLogin') }}
                            </div>
                        @endif
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام محصول</th>
                                        <th>قیمت</th>
                                        <th>تخفیف</th>
                                        <th>تعداد</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    @foreach($data['basket'] as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$item->product->name_fa}}</td>
                                            <td>{{$item->product->price}}</td>
                                            <td>
                                                @if($item->product->offer)
                                                    %{{$item->product->offer}}
                                                @else
                                                    ندارد
                                                @endif
                                            </td>
                                            <td>{{$item->count}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-12 m-0 text-right priceSection">
                                <p><b> میزان خرید شما :</b> <span>{{$data['salePrice']}} تومان </span></p>
                                <p><b>میزان تخفیف شما :</b> <span>{{$data['offerPrice']}} تومان </span></p>
                                <p><b>هزینه قابل پرداخت :</b> <span>{{$data['totalPrice']}} تومان </span></p>
                                <br>
                                <label class="address"> آدرس تحویل محصول : </label>
                                <input type="text" class="form-control" id="deliveryAddress" placeholder="آدرس تحویل محصول"
                                       @change="updateBasketAddress()"
                                       value="@if($data['basket'][0]->address){{$data['basket'][0]->address}}@endif">

                                <input type="text" class="form-control"  @keyup="updateBasketAddress()" id="deliveryPhone"placeholder="شماره تلفن خود را وارد کنید">
                                <p>با این شماره تلفن برای شما یک حساب کاربری ایجاد میگردد و بعدا شما میتوانید با این شماره تلفن وارد حساب کاربری خود شوید</p>

                            </div>
                            <div class="col-md-12 col-12 m-0 text-left">
                                <button class="btn btn-2 printPage"> چاپ</button>
                                <a href="{{route('payment')}}" class="btn btn-3 payment"> پرداخت </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <!-- Tostar -->
    <link href="{{ asset('assets/backend/vendors/Toastr/toastr.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('assets/backend/vendors/Toastr/toastr.js') }}"></script>

    <script>
        $('button.printPage').click(function () {
            window.print();
            return false;
        });
    </script>
@endsection