@extends('frontend.layout.app')

@section('title')  سبد خرید    @stop
@section('pageLink')  سبد خرید    @stop
@section('pageTitle')  سبد خرید     @stop

@section('css')
    <style>
        .discountEl{
            width: 170px;
            padding: 1px;
            text-align: center;
        }
    </style>
@stop
@section('js')
    <!-- Tostar -->
    <link href="{{ asset('assets/backend/vendors/Toastr/toastr.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('assets/backend/vendors/Toastr/toastr.js') }}"></script>
@stop



@section('content')

    <main class="container basketPage userPanelSection">
        <div class="row m-0">
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 px-2">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نام محصول</th>
                                    <th class="hidden-md">عکس</th>
                                    <th>تعداد</th>
                                    <th class="hidden-md">قیمت</th>
                                    <th class="hidden-md">کد تخفیف</th>
                                    <th>حذف </th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($allProduct as $key => $item)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$item->product->name_fa}}</td>
                                        <td class="hidden-md">
                                            <img src="{{asset($item->product->gallery->first()->photo)}}" alt="{{$item->product->name_fa}}"
                                            width="100" height="80p">
                                        </td>

                                        <td>
                                            <input type="number" min="1" value="{{$item->count}}" class="form-control"
                                                   @change="updateBasketItem({{$item->id}} ,$event)">
                                        </td>

                                        <td class="hidden-md">{{$item->product->price}} تومان</td>

                                        <td>
                                            <input type="text" min="1" value="{{$item->discount_code}}" class="form-control discountEl"
                                                   @change="addDiscountCode({{$item->id}} ,$event)">
                                        </td>

                                        <td><i v-on:click="removeFromBasket({{$item->id}} ,$event)" class="remove">X</i></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 px-2">
                <div id="factorUnit">
                    <div class="block-title">صورت حساب</div>
                    <div class="block-content">
                        <dl>
                            <dt>میزان خرید شما: </dt>
                            <dd>@{{basketSalePrice}} تومان</dd>


                            <dt>میزان تخفیف :</dt>
                            <dd>@{{basketOfferPrice}} تومان</dd>


                            <dt>مبلغ قابل پرداخت</dt>
                            <dd>@{{basketTotalPrice}} تومان</dd>
                        </dl>
                    </div>
                    <div class="row factorRow">
                        <a href="{{route('basket.factor')}}">
                            <button class="btn btn-2" type="button">فاکتور</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </main>
@stop
