@extends('frontend.layout.app')

@section('title')  ورود    @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  ورود کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection lofinForm">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    @include('backend.layout.errors')
                    <form class="form clearfix" method="post" action="{{route('user.authentication')}}">
                        @csrf

                        <h1> ورود کاربر</h1>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="mail">ایمیل</label>
                                <input type="email" id="mail" name="mail" class="form-control" placeholder="ایمیل"
                                value="{{old('mail')}}">
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="password">رمز عبور</label>
                                <input type="password" id="password" name="password" class="form-control"
                                       placeholder="رمز عبور">
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 px-0">
                                    <label for="security">عبارت امنیتی</label>
                                    <input name="security" id="security" type="text" class="form-control securityInput"
                                           placeholder="حاصل جمع مقابل" required>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h2 class="robotSecurity"> = {{$data['securityX']}} + {{$data['securityY']}} </h2>
                                </div>
                            </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                                <button class="btn btn-2"> ورود <i class="fa fa-long-arrow-left"></i>
                                </button>
                            </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p class="ruls">آیا هنوز عضو سایت نشده اید؟ <a href="{{route('user.register')}}">ثبت
                                        نام</a>
                                </p>

                                <p class="ruls"><a href="{{route('user.forgetPassForm')}}"> فراموشی رمز عبور ؟ </a>
                                </p>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
