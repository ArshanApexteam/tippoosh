@extends('frontend.layout.app')

@section('title')  تغییر رمز عبور     @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">

                    @include('backend.layout.errors')
                    <form method="post" action="{{route('user.passwordUpdate')}}" enctype="multipart/form-data">
                        @csrf


                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="row">

                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label for="newPassword">رمز عبور جدید</label>
                                        <input type="password" name="newPassword" class="form-control" placeholder="رمز عبور جدید">
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label for="rePassword">تکرار رمز عبور</label>
                                        <input type="password" name="rePassword" class="form-control"
                                               placeholder="تکرار رمز عبور">
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                                        <button class="btn btn-2"> ذخیره <i class="fa fa-long-arrow-left"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
