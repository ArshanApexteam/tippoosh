@extends('frontend.layout.app')

@section('title')  سبد خرید     @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  سبد خرید  کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">
                    <div class="card m-0">
                        @isset($allProduct[0])
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>نام محصول</th>
                                            <th class="hidden-md">عکس</th>
                                            <th>تعداد</th>
                                            <th class="hidden-md">قیمت</th>
                                            <th>حذف کردن</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($allProduct as $key => $item)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>
                                                    <a href="{{route('product.show' , $item->product->slug)}}">
                                                        {{$item->product->name_fa}}
                                                    </a>
                                                </td>

                                                <td class="hidden-md">
                                                    <a href="{{route('product.show' , $item->product->slug)}}">
                                                        <img src="{{asset($item->product->gallery->first()->small)}}"
                                                             alt="{{$item->product->name_fa}}">
                                                    </a>
                                                </td>

                                                <td>
                                                    <input type="number" min="1" value="{{$item->count}}"
                                                           class="form-control"
                                                           @change="updateBasketItem({{$item->id}} ,$event)">
                                                </td>
                                                <td class="hidden-md">{{$item->product->price}} تومان</td>
                                                <td><i v-on:click="removeFromBasket({{$item->id}} ,$event)" class="remove">X</i></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="row m-0 text-left  factorRow">
                                    <a href="{{route('basket.factor')}}">
                                        <button class="btn btn-2 ml-2 mb-5" type="button">فاکتور</button>
                                    </a>
                                </div>

                            </div>
                        @else
                            <h5 class="text-center">موردی یافت نشد</h5>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
