@extends('frontend.layout.app')

@section('title')  پروفایل     @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  پروفایل  کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">
                    @include('backend.layout.errors')
                    <form method="post" action="{{route('user.profileUpdate')}}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="name">ایمیل </label>
                                <input type="text" name="username" class="form-control" placeholder=" ایمیل" value="{{$user->username}}">
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="name">نام</label>
                                <input type="text" name="name" class="form-control" placeholder="نام" value="{{$user->name}}">
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="phone">شماره تلفن</label>
                                <input type="text" name="phone" class="form-control" placeholder="شماره تلفن" value="{{$user->phone}}">
                            </div>


                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="address">آدرس</label>
                                <input type="text" name="address" class="form-control" placeholder="آدرس" value="{{$user->address}}">
                            </div>


                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="imageProfile">عکس</label>
                                <input type="file" name="imageProfile" class="form-control">
                            </div>



                            <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                                <button class="btn btn-2"> ذخیره  <i class="fa fa-long-arrow-left"></i>
                                </button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
