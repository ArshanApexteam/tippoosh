@extends('frontend.layout.app')

@section('title')  نظرات     @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  نظرات کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">
                    <div class="card m-0">
                        @isset($allComments[0])
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="hidden-md">نام محصول</th>
                                            <th>نظر</th>
                                            <th>حذف کردن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allComments as $key => $item)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td class="hidden-md">
                                                    <a href="{{route('product.show' , $item->product->slug)}}">
                                                        {{$item->product->name_fa}}
                                                    </a>
                                                </td>
                                                <td>{{$item->comment}} </td>
                                                <td><i @click="removeComments({{$item->id}} , $event)" class="remove">X</i></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h5 class="text-center notFound">موردی یافت نشد</h5>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
