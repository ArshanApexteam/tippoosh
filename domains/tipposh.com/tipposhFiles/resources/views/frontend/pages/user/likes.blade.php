@extends('frontend.layout.app')

@section('title')  مورد علاقه ها     @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  مورد علاقه های کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12  mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">
                    <div class="card m-0">
                        @isset($allProduct[0])
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>نام محصول</th>
                                            <th class="hidden-md">عکس</th>
                                            <th>قیمت</th>
                                            <th>حذف کردن</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($allProduct as $key => $item)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>
                                                    <a href="{{route('product.show' , $item->slug)}}">
                                                        {{$item->name_fa}}
                                                    </a>
                                                </td>
                                                <td class="hidden-md">
                                                    <a href="{{route('product.show' , $item->slug)}}">
                                                        <img src="{{asset($item->gallery->first()->small)}}"
                                                             alt="{{$item->name_fa}}">
                                                    </a>
                                                </td>
                                                <td>{{$item->price}} تومان</td>
                                                <td><i @click="like({{$item->id}} , $event)" class="remove">X</i></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h5 class="text-center notFound">موردی یافت نشد</h5>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
