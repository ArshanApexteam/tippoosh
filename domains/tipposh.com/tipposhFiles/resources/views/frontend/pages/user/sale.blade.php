@extends('frontend.layout.app')

@section('title')  خرید ها     @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  خرید های  کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection">
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mb-5 px-2">
                    @include('frontend.pages.user.rightSide')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddding-xs-0 px-2">

                    @if (session('paymentOk'))
                        <div class="alert alert-success">
                            <p>پرداخت با موفقیت انجام شد.</p>
                        </div>
                    @endif


                    <div class="card m-0">
                        @isset($allProduct[0])
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>نام محصول</th>
                                            <th>عکس</th>
                                            <th>تعداد</th>
                                            <th>تاریخ خرید</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($allProduct as $key => $item)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>
                                                    <a href="{{route('product.show' , $item->product->slug)}}">
                                                        {{$item->product->name_fa}}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{route('product.show' , $item->product->slug)}}">
                                                        <img src="{{asset($item->product->gallery->first()->small)}}"
                                                             alt="{{$item->product->name_fa}}">
                                                    </a>
                                                </td>
                                                <td>{{$item->count}}</td>
                                                <td>{{ \Verta::instance($item->created_at)->format('d %B y') }}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h5 class="text-center notFound">موردی یافت نشد</h5>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
