<div class="row userPanleRightSide m-0">
    @php  $user = Auth::guard('web')->user(); @endphp
    <div class="row imageSection">
        <a href="{{route('user.profile')}}" title="تغییر عکس پروفایل">
            @if(file_exists($user->imageProfile))
                <img src="{{asset($user->imageProfile)}}" alt="{{$user->name}}">
            @else
                <img src='{{asset("assets/images/user/users/none.png")}}' alt="{{$user->name}}">
            @endif
            <p><b>{{$user->name}}</b></p>
        </a>

    </div>

    <ul>
        <li><a href="{{route('user.profile')}}">پروفایل</a></li>
        <li><a href="{{route('user.sale')}}">خرید ها </a></li>
        <li><a href="{{route('user.basket')}}">سبد خرید</a></li>
        <li><a href="{{route('user.likes')}}">مورد علاقه ها</a></li>
        <li><a href="{{route('user.comments')}}">نظرات</a></li>
        <li><a href="{{route('user.password')}}">تغییر رمز عبور</a></li>
        <li><a href="{{route('user.logout')}}">خروج</a></li>
    </ul>
</div>


@section('js-2')
    <script>
        $(document).ready(function(){
            var headerLocation = window.location;
            $('.userPanleRightSide a').each(function(){
                if(($(this).attr('href')+'/') == headerLocation  || $(this).attr('href') == headerLocation){
                    $(this).parents('li').addClass('activeLink');
                }
            });
        });
    </script>
@endsection