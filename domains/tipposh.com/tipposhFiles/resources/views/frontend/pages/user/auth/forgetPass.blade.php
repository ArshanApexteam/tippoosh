@extends('frontend.layout.app')

@section('title')  فراموشی رمز عبور    @stop
@section('pageLink')  کاربر    @stop
@section('pageTitle')  فراموشی رمز عبور کاربر   @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="userPanelSection lofinForm">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    @include('backend.layout.errors')
                    <form class="form clearfix" method="post" action="{{route('user.forgetPass')}}">
                        @csrf

                        <h1> فراموشی رمز عبور کاربر</h1>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="mail">ایمیل</label>
                                <input type="email" id="mail" name="mail" class="form-control" placeholder="ایمیل"
                                value="{{old('mail')}}">
                            </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                                <button class="btn btn-2"> فراموشی رمز عبور <i class="fa fa-long-arrow-left"></i>
                                </button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
