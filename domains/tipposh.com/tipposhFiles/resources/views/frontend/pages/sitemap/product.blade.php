<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach($query as $value)
        <url>
            <loc>
                {{ urldecode(route('product.show',$value->slug)) }}
            </loc>
            <lastmod>{{gmdate(DateTime::W3C, strtotime($value->updated_at)) }} </lastmod>
            <changefreq>hourly</changefreq>
            <priority>0.8</priority>
            <image:image>
                <image:loc>
                    @isset($value->gallery[0])
                        {{ asset($value->gallery[0]->photo) }}
                    @endisset
                </image:loc>
                <image:caption>تیپ پوش تیم</image:caption>
                <image:title>{{ $value->name_fa }}</image:title>
            </image:image>
        </url>
    @endforeach
</urlset>

