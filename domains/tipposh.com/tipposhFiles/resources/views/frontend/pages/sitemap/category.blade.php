<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($query as $value)
        <url>
            <loc>{{ urldecode(url('category',$value->slug)) }}</loc>
            <lastmod>{{gmdate(DateTime::W3C, strtotime($value->updated_at)) }}</lastmod>
            <changefreq>always</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
</urlset>