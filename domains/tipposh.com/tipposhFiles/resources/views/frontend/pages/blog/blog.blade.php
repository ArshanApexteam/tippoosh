@extends('frontend.layout.app')

@section('title')  وبلاگ ما    @stop

@section('css')@stop
@section('js')@stop



@section('content')

    <section class="blogPage">
        <div class="container">
            <div class="row">

                @foreach($blogs as $item)
                    <div class="item col-md-3 col-12">

                        <a href="{{route('blog.details' , $item->slug)}}">

                            <img src="{{asset($item->index_photo)}}" alt="{{$item->title}}" title="{{$item->title}}">

                            <h3>{{$item->title}}</h3>

                            <p>{{$item->summary}}</p>

                            {{--<span>5 <i class="fa fa-heart"></i></span>--}}
                        </a>

                    </div>
                @endforeach


            </div>
        </div>
    </section>
    <!--===========================
            End Blog
    ===========================-->
@stop