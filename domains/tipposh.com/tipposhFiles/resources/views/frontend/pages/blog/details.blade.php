@extends('frontend.layout.app')

@section('title')  {{$blog->title}}    @stop

@section('css')@stop
@section('js')@stop



@section('content')


    <section class="blogPage">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="blog_details_right">
                        <h2>مطالب اخیر</h2>
                        <ul>
                            @foreach($blog->newBlogs() as $item)
                                <li>
                                    <a href="{{route('blog.details' , $item->slug)}}">
                                        <img src='{{asset(asset($item->index_photo))}}' alt="{{$item->title}}">
                                        {{$item->title}}
                                    </a>
                                </li>
                        @endforeach

                    </div>
                </div>


                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="contentSection">

                        <div class="imgSection"
                             style="background: url({{asset($blog->main_photo)}}) center center no-repeat;">
                            <span class="blog_date">{{ \Verta::instance($blog->created_at)->format('d %B') }}</span>
                        </div>

                        <div class="blog_details">
                            <span>{{$blog->auther->name}}</span>
                            <h1>{{$blog->title}}</h1>
                            @php
                                echo $blog->description;
                            @endphp

                        </div>

                        <div class="tags">
                            <p>برچسب ها :
                                @foreach($blog->blogTag as $item)
                                    <a>{{$item->caption}}</a>
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--===========================
            End Blog Details
    ===========================-->

@stop