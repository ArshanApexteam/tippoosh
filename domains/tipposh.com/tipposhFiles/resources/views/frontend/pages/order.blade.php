@extends('frontend.layout.app')

@section('title') سفارش قالب اختصاصی@stop

@section('css') @stop
@section('js')
    {{-- multi Select File --}}
    <link href="{{asset('assets/backend/vendors/multiSelectFile/selectize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/backend/vendors/multiSelectFile/custom.css')}}" rel="stylesheet">
    <script src="{{asset('assets/backend/vendors/multiSelectFile/selectize.min.js')}}"></script>
    <script src="{{asset('assets/backend/vendors/multiSelectFile/custom.js')}}"></script>
    <script src="{{asset('assets/backend/vendors/multiSelectFile/jQuery.MultiFile.min.js')}}"></script>

@stop

@section('content')
    <div class="orderPage">
        <div class="container">

            <form action="{{ route('order.save') }}" method="post" enctype="multipart/form-data" >
                @csrf
                
                <div class="row">
                    
                    <div class="col-md-12">
                        @include('backend.layout.errors')
                    </div>
            
                    <div class="col-md-12">
                        <h1>سفارش قالب اختصاصی</h1>
                    </div>
                    
                    <input type="hidden" name="product_id" value="{{ $product_id }}">
                    
                    <div class="col-md-12">
                        <div class="file-upload-previews"></div>
                        <div class="file-upload">
                            <input type="file" name="photo" class="file-upload-input with-preview" title="Click to add files" accept="gif|jpg|png|jpeg">
                            <span><i class="fa fa-plus-circle"></i>کلیک یا عکس خود را به اینجا درگ کنید</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-2" value="ثبت">
                    </div>
                </div>
            </form>

        </div>
    </div>

@stop