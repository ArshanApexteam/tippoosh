﻿@extends('frontend.layout.app')

@section('title') تیپ پوش | Tipposh @stop

@section('css') @stop
@section('js') @stop

@section('content')

    @include('frontend.partial.index.1-slide')
    @include('frontend.partial.index.2-why-us')
    @include('frontend.partial.index.3-how-to-order')
    @include('frontend.partial.index.4-index-ad-area')
    @include('frontend.partial.index.5-mostSaleProduct')
    @include('frontend.partial.index.6-save-order')
    @include('frontend.partial.index.7-offers')
{{--    @include('frontend.partial.index.8-news')--}}
{{--    @include('frontend.partial.index.9-brandSection')--}}

@stop
