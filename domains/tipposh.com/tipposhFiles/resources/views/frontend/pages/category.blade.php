﻿@extends('frontend.layout.app')

@section('title')  دسته بندی ::  {{$data['curentCategory']->name}}   @endsection

@section('css')

    <link rel="stylesheet" href="{{asset('assets/backend/vendors/nice-select/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/vendors/jqueryUi/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/pages/category.css')}}">


@endsection

@section('js')

    <script src="{{asset('assets/backend/vendors/jqueryUi/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/backend/vendors/nice-select/jquery.nice-select.js')}}"></script>
    <script src="{{asset('assets/frontend/js/pages/category.js')}}"></script>
    <script>
        $('.filteringAplay').on('change', function () {
            $('#sort_key').val($("#el_sort_key").val());
            $('#paginate').val($("#el_paginate").val());
            if ($('#el_isStore').is(':checked'))
                $('#isStore').val(1);
            else
                $('#isStore').val(0);
        });

        $(document).ready(function () {
            /*--------------------------------
            Price Slider Active
            -------------------------------- */
            var maxPrice = "{{$data['maxPrice']}}";
            var minPrice = "{{$data['minPrice']}}";

            var min = 20000000;
            var max = 80000000;

            if (maxPrice > 10)
                max = maxPrice;

            if (minPrice > 10)
                min = minPrice;

            var sliderrange = $('#slider-range');
            var amountprice_min = $('#amount_min');
            var amountprice_max = $('#amount_max');
            $(function () {
                sliderrange.slider({
                    range: true,
                    min: 0,
                    max: 100000000,
                    values: [min, max],
                    slide: function (event, ui) {
                        amountprice_min.val(ui.values[0] + ' تومان ');
                        amountprice_max.val(ui.values[1] + ' تومان ');
                    }
                });
                amountprice_min.val(sliderrange.slider('values', 0) + ' تومان ');
                amountprice_max.val(sliderrange.slider('values', 1) + ' تومان ');
            });

        });
    </script>
@endsection


@section('content')

    <div class="container-fluid categoreisPage">
        <div class="row">

            @include('frontend.partial.category.sidebar')

            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                @include('frontend.partial.category.content')

            </div>
        </div>
    </div>

@endsection