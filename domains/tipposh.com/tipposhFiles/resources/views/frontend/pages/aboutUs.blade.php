@extends('frontend.layout.app')

@section('title') درباره ما @stop

@section('css') @stop
@section('js')@stop

@section('content')
    <div class="orderPage">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1>هر آنچه درباره ما باید بدانید</h1>
                </div>

                <div class="col-12 col-md-7">
                {{\App\Models\Setting::where('key' ,"aboutUs-description" )->first()->value}}
                    
                </div>
                <div class="col-12 col-md-5">
                    <img src="{{asset(\App\Models\Setting::where('key' ,"aboutUs-description" )->first()->value)}}" alt="درباره ما" title="درباره ما">
                </div>
            </div>

        </div>
    </div>

@stop