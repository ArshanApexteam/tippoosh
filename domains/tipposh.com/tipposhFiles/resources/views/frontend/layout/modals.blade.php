<div class="modal fade suigUpModal" id="suigUpModalTakePhone" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row m-0">
                    <h5 class="modal-title">ورود / ثبت نام </h5>
                </div>

                <div class="row m-0 py-15">
                    <span class="number active">1</span>
                    <span class="title">شماره موبایل</span>
                    <hr>
                    <span class="number">2</span>
                    <span class="title">تایید کد</span>
                </div>

                <div class="row m-0 mt-4">
                    <div class="col-md-12">
                        @if (session('prossesOk'))
                            <div class="alert alert-success">
                                {{ session('prossesOk') }}
                            </div>
                        @endif
                        @if (session('prossesError'))
                            <div class="alert alert-danger">
                                {{ session('prossesError') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row m-0">
                    <form method="post" action="{{route('user.loginTakePhone')}}">
                        @csrf
                        <input type="text" name="phone" class="form-control" value="{{old('phone')}}"
                               placeholder="شماره تلفن خود را وارد نمایید">
                        <button type="submit" class="btn">تایید</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade suigUpModal" id="suigUpModalVerifyPhone" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row m-0">
                    <h5 class="modal-title">ورود / ثبت نام </h5>
                </div>

                <div class="row m-0 py-15">
                    <span class="number">1</span>
                    <span class="title">شماره موبایل</span>
                    <hr>
                    <span class="number active">2</span>
                    <span class="title">تایید کد</span>
                </div>

                <div class="row m-0 mt-4">
                    <div class="col-md-12">
                        @if (session('prossesOk'))
                            <div class="alert alert-success">
                                {{ session('prossesOk') }}
                            </div>
                        @endif
                        @if (session('prossesError'))
                            <div class="alert alert-danger">
                                {{ session('prossesError') }}
                            </div>
                        @endif
                    </div>
                </div>


                <div class="row m-0">
                    <form method="post" action="{{route('user.loginVerifyCode')}}">
                        @csrf
                        <input type="text" name="code" class="form-control" value="{{old('code')}}"
                               placeholder="کد تایید را وارد نمایید">
                        <button type="submit" class="btn">تایید</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade suigUpModal" id="suigUpModalUserName" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row m-0">
                    <h5 class="modal-title"> ورود با ایمیل </h5>
                </div>


                <div class="row m-0 mt-4">
                    <div class="col-md-12">
                        @if (session('prossesOk'))
                            <div class="alert alert-success">
                                {{ session('prossesOk') }}
                            </div>
                        @endif
                        @if (session('prossesError'))
                            <div class="alert alert-danger">
                                {{ session('prossesError') }}
                            </div>
                        @endif
                    </div>
                </div>


                <div class="row m-0">
                    <form method="post" action="{{route('user.loginWithUsername')}}">
                        @csrf
                        <input type="text" name="username" class="form-control" value="{{old('username')}}"
                               placeholder="ایمیل ">

                        <br>
                        <input type="password" name="password" class="form-control"
                               placeholder="رمز عبور">

                        <button type="submit" class="btn">ورود</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


@if (session('userRequestLogin'))
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#suigUpModalVerifyPhone').modal('show');
        });
    </script>

    {{session()->forget('userRequestLogin')}}
@endif

@if (session('userErrorInPhone'))
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#suigUpModalTakePhone').modal('show');
        });
    </script>

    {{session()->forget('userErrorInPhone')}}
@endif


@if (session('loginWithUsername'))
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#suigUpModalUserName').modal('show');
        });
    </script>

    {{session()->forget('loginWithUsername')}}
@endif

