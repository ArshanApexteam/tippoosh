<section id="header">
    <div class="container">
        <div class="row row-header">
            <div class="col-12 col-sm-6 col-md-2 col-lg-2 pt-5 pb-4 div-logo-header logoSection">
                <a href="{{route('index')}}">
                    @php $logo = \App\Models\Setting::where('key' ,"logo" )->first(); @endphp
                    <img src="{{asset($logo->value)}}" alt='{{trans("translator.$logo->key")}}'
                         title='{{trans("translator.$logo->key")}}'>
                </a>
            </div>

            <div class="col-12 col-sm-6 col-md-7 col-lg-8 menuSection">
                <div class="container p-0">

                    <div class="row m-0 bg-two rounded row-menu">

                        <nav class="navbar navbar-light navbar-expand-lg mainmenu">
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto">

                                    <li class="home-menu active">
                                        <a href="#">
                                            <i class="fas fa-bars"></i>
                                            <span class="sr-only">(current)</span>
                                        </a>
                                    </li>


		 
									<li class="dropdown">
										<a href="#" id="navbarDropdown11" role="button"
										   data-toggle="dropdown" aria-haspopup="true"
										   aria-expanded="false">دسته بندی ها
										</a>

										<ul class="dropdown-menu" aria-labelledby="navbarDropdown11">
											@foreach(\App\Models\Category::where('parent_id',0)->where('status',1)->get() as $category)
												@isset($category->childs[0])
													@foreach($category->childs as $subcategory)
														<li>
															<a href="{{route('category',$subcategory->slug)}}">{{$subcategory->name}}</a>
														</li>
													@endforeach
											
												@else
													<li><a href="{{route('category',$category->slug)}}">{{$category->name}}</a></li>
												@endisset
											@endforeach
										</ul>
									</li>
                    

									<li><a href="{{route('aboutUs')}}">درباره ما</a></li>
									<li><a href="{{route('contactUs')}}">تماس با ما</a></li>
									<li><a href="{{route('blog.all')}}">وبلاگ</a></li>
									<li><a href="{{route('help')}}">راهنما</a></li>


                                    <form class="mr-auto form-inline" action="{{route('search')}}">
                                        <input class="form-control" type="search" placeholder="جستجوی محصول"
                                               aria-label="جستجوی محصول" name="serach">
                                        <button class="btn" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </form>
                                </ul>
                            </div>
                        </nav>

                    </div>

                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-3 col-lg-2  px-1 acountSection">

                <div class="dropdown userIcon">
                    <i class="fa fa-user" data-toggle="dropdown"></i>
                    <div class="dropdown-menu">
                        @guest
                            <a class="dropdown-item" data-toggle="modal" data-target="#suigUpModalTakePhone"> ورود با شماره همراه </a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#suigUpModalUserName"> ورود با ایمیل </a>
                        @else
                            <a class="dropdown-item" href="{{route('user.profile')}}"> پروفایل </a>
                            <a class="dropdown-item" href="{{route('user.likes')}}"> مورد علاقه ها </a>
                            <a class="dropdown-item" href="{{route('user.sale')}}"> خرید ها </a>
                            <a class="dropdown-item" href="{{route('user.comments')}}"> نظرات </a>
                            <a class="dropdown-item" href="{{route('user.logout')}}"> خروج </a>
                        @endguest
                    </div>
                </div>

                <a href="{{route('basket.index')}}" class="btn basket">
                    <i class="fa fa-shopping-basket"></i>
                    <span>سبد خرید</span>
                    <span class="badge">@{{ userBasketCount }}</span>
                </a>
            </div>

        </div>
    </div>

</section>

