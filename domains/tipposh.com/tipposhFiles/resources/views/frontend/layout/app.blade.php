<!DOCTYPE html>

@php

    if(isset($_GET['utm_source']))
    {
        if($_GET['utm_source'] == "takhfifan")
            setcookie('takhfifan', $_GET['tatoken'], time() + (86400 * 30), '/');

    }
    
@endphp

<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <!--favicon-->
    @php $favicon = \App\Models\Setting::where('key' ,"favicon" )->first(); @endphp
    <link rel="icon" href='{{asset($favicon->value)}}'>

    <!--stylesheet-->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" tipposh | تیپ پوش , tipposh , تیپ پوش " />
    <meta name="keywords" content=" tipposh | تیپ پوش , tipposh , تیپ پوش " />
    <meta name="document-rating" content="General" />
    <meta name="classification" content="" />
    <meta name="rating" content="Safe For Kids" />
    <meta name="resource-type" content="document" />
    <meta name="author" content="mahmood bagheri" />
    <meta name="copyright" content="April 2020" />
    <meta name="doc-class" content="Living Document" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="robots" content="index,follow,noodp,noydir" />
    

    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/fontawesome5/fontawesome5.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/fontawesome4/fontawesome4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/iransans/font.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/style.css')}}" />

    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets/backend/vendors/Toastr/toastr.css')}}">

    @yield('css')

    <script>
        (function(i,s,o,g,r,a,m){i['TakhfifanAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://cashback.takhfifan.com/cashback.js','ta');
    </script>
    
    <script>
    	!function (t, e, n) {
    		t.yektanetAnalyticsObject = n, t[n] = t[n] || function () {
    			t[n].q.push(arguments)
    		}, t[n].q = t[n].q || [];
    		var a = new Date, r = a.getFullYear().toString() + "0" + a.getMonth() + "0" + a.getDate() + "0" + a.getHours(),
    			c = e.getElementsByTagName("script")[0], s = e.createElement("script");
    		s.id = "ua-script-4e7JjxM4"; s.dataset.analyticsobject = n; s.async = 1; s.type = "text/javascript";
    		s.src = "https://cdn.yektanet.com/rg_woebegone/scripts_v3/4e7JjxM4/rg.complete.js?v=" + r, c.parentNode.insertBefore(s, c)
    	}(window, document, "yektanet");
    </script>


</head>
<body>

<div class="myVueApp">

    @include('frontend.layout.header')

    @yield('content')

    @include('frontend.layout.footer')


</div>


<script src="{{asset('assets/frontend/js/jquery.min.js')}}"></script>

@include('frontend.layout.modals')

<!-- Toastr -->
<script src="{{asset('assets/backend/vendors/Toastr/toastr.js')}}"></script>

<!-- Vuejs -->
<script type="text/javascript" src='{{asset("assets/backend/vendors/VueJS/vue.js") }}'></script>
<script type="text/javascript" src='{{asset("assets/backend/vendors/VueJS/myApps.js") }}'></script>

<script src="{{asset('assets/frontend/js/popper.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/custom.js')}}"></script>


@yield('js')
@yield('js-2')

</body>
</html>