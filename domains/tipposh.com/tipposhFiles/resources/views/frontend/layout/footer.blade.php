<footer>
    <div class="container-fluid">

        <div class="row">
            <a href="{{route('index')}}" class="d-inline-block mb-4 a-logo-footer">
                @php $logo = \App\Models\Setting::where('key' ,"logo" )->first(); @endphp
                <img src="{{asset($logo->value)}}" alt='{{trans("translator.$logo->key")}}'
                     title='{{trans("translator.$logo->key")}}' width="130" height="45">
            </a>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12 div-about-footer">
                <p class="text-justify div-about-footer-p">
                    {{\App\Models\Setting::where('key' ,"footer-description" )->first()->value}}
                </p>

                <span> <i class="fa fa-map-marker"></i>
                    {{\App\Models\Setting::where('key' ,"address" )->first()->value}}
                </span>

                <span class="float-right ml-5"> <i class="fa fa-phone"></i>
                    {{\App\Models\Setting::where('key' ,"cell-phone" )->first()->value}}
                </span>

                <span> <i class="fa fa-envelope"></i>
                    {{\App\Models\Setting::where('key' ,"email" )->first()->value}}
                </span>
            </div>

            <div class="col-lg-4 col-md-12 px-5">
                <div class="row">
                    <h2 class="title"> لینک ها </h2>
                    <h6> در حاشیه اینجا چند لینک جالب برای شما گذاشته ایم. لذت ببرید </h6>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sn-12 col-12">
                        <ul>
                            <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                            <li><a href="{{route('aboutUs')}}">درباره ما</a></li>
                            <li><a href="{{route('contactUs')}}">تماس با ما</a></li>
                            <li><a href="{{route('help')}}">راهنما</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sn-12 col-12">
                        <ul>
                            <li><a href="{{route('blog.all')}}">وبلاگ</a></li>
                            {{--<li><a href="">آموزش</a></li>--}}

                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 mb-md-5 px-5">
                <div class="row">
                    <h2 class="title">نماد های اعتماد</h2>
                    <h6>با اعتماد و اطمینان خرید کنید.</h6>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6">  {!! \App\Models\Setting::where('key' ,"e-nemad" )->first()->value !!}</div>
                    <div class="col-12 col-sm-6 col-md-6">  {!! \App\Models\Setting::where('key' ,"e-samandehi" )->first()->value !!}</div>
                </div>
            </div>
        </div>


        <div class="row copyright">
            <div class="col-md-6 txt"> تمامی حقوق مادی و معنوی برای تیپ پوش محفوظ می باشد </div>
            <div class="col-md-6 text-left">

                <a href="{{\App\Models\Setting::where('key' ,"instagram-link" )->first()->value}}"><i class="far fa-instagram"></i></a>
                <a href="{{\App\Models\Setting::where('key' ,"telegram-link" )->first()->value}}"><i class="far fa-telegram"></i></a>
                <a href="{{\App\Models\Setting::where('key' ,"email" )->first()->value}}"><i class="far fa-google-plus"></i></a>

            </div>
        </div>
    </div>
</footer>