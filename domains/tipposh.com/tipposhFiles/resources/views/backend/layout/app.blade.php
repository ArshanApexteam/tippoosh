<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href='{{asset("assets/backend/media/image/favicon.png")}}'>

    <!-- Theme Color -->
    <meta name="theme-color" content="#5867dd">

    <!-- Plugin styles -->
    <link rel="stylesheet" href='{{asset("assets/backend/vendors/bundle.css")}}' type="text/css">

    <!-- Datepicker -->
    <link rel="stylesheet" href='{{asset("assets/backend/vendors/datepicker/daterangepicker.css")}}'>

    <!-- Slick -->
    <link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick.css")}}'>
    <link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick-theme.css")}}'>

    <!-- Vmap -->
    <link rel="stylesheet" href='{{asset("assets/backend/vendors/vmap/jqvmap.min.css")}}'>

    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/fontawesome5/fontawesome5.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/fontawesome4/fontawesome4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/iransans/font.css')}}" />

    <!-- App styles -->
    <link rel="stylesheet" href='{{asset("assets/backend/css/app.css")}}' type="text/css">

    @yield('css')

    <link rel="stylesheet" href='{{asset("assets/backend/css/demo.css")}}' type="text/css">


</head>

<body>

@include('backend.layout.1-loader')
@include('backend.layout.2-userProfile')
@include('backend.layout.3-settings')
@include('backend.layout.4-navigation')
@include('backend.layout.5-header')

@yield('content')



<!-- Plugin scripts -->
<script src='{{asset("assets/backend/vendors/bundle.js")}}'></script>

<!-- Chartjs -->
<script src='{{asset("assets/backend/vendors/charts/chartjs/chart.min.js")}}'></script>

<!-- Circle progress -->
<script src='{{asset("assets/backend/vendors/circle-progress/circle-progress.min.js")}}'></script>

<!-- Peity -->
<script src='{{asset("assets/backend/vendors/charts/peity/jquery.peity.min.js")}}'></script>
<script src='{{asset("assets/backend/assets/js/examples/charts/peity.js")}}'></script>

<!-- Datepicker -->
<script src='{{asset("assets/backend/vendors/datepicker/daterangepicker.js")}}'></script>

<!-- Slick -->
<script src='{{asset("assets/backend/vendors/slick/slick.min.js")}}'></script>

<!-- Vamp -->
<script src='{{asset("assets/backend/vendors/vmap/jquery.vmap.min.js")}}'></script>
<script src='{{asset("assets/backend/vendors/vmap/maps/jquery.vmap.usa.js")}}'></script>
<script src='{{asset("assets/backend/js/examples/vmap.js")}}'></script>

<!-- Dashboard scripts -->
<script src='{{asset("assets/backend/js/examples/dashboard.js")}}'></script>
<div class="colors">
    <!-- To use theme colors with Javascript -->
    <div class="bg-primary"></div>
    <div class="bg-primary-bright"></div>
    <div class="bg-secondary"></div>
    <div class="bg-secondary-bright"></div>
    <div class="bg-info"></div>
    <div class="bg-info-bright"></div>
    <div class="bg-success"></div>
    <div class="bg-success-bright"></div>
    <div class="bg-danger"></div>
    <div class="bg-danger-bright"></div>
    <div class="bg-warning"></div>
    <div class="bg-warning-bright"></div>
</div>

<!-- App scripts -->
<script src='{{asset("assets/backend/js/app.js")}}'></script>
<script src='{{asset("assets/backend/js/custom.js")}}'></script>

@yield('js')
@yield('navJS')

</body>

</html>
