<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-icon-menu">
        <ul>
            <li data-toggle="tooltip" title="آمار سایت">
                <a href="#navigationDashboards" title="آمار سایت">
                    <i class="icon ti-pie-chart"></i>
                    <span class="badge badge-warning">2</span>
                </a>
            </li>
            <li data-toggle="tooltip" title="برنامه ها" id="mangeNavigationApps">
                <a href="#navigationApps" title="برنامه ها">
                    <i class="icon ti-package"></i>
                </a>
            </li>
        </ul>
        <ul>
            <li data-toggle="tooltip" title="ویرایش پروفایل">
                <a href="{{route('admin.profile')}}" class="go-to-page">
                    <i class="icon ti-settings"></i>
                </a>
            </li>
            <li data-toggle="tooltip" title="خروج">
                <a href="{{route('admin.logout')}}" class="go-to-page">
                    <i class="icon ti-power-off"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="navigation-menu-body">
        <ul id="navigationDashboards">
            <li>
                <a href="#" class="mb-2">
                    <div class="d-flex align-items-center">
                        <div>
                            <div class="icon-block bg-warning text-white mr-3">
                                <i class="ti-bar-chart"></i>
                            </div>
                        </div>
                        <div>
                            <h6 class="font-size-13 line-height-22 primary-font m-b-5">مجموع فروش</h6>
                            <h4 class="m-b-0 primary-font font-weight-bold line-height-30">
                                {{\App\Models\Sale::all()->sum('count')}}
                            </h4>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="#">
                    <div class="d-flex align-items-center">
                        <div>
                            <div class="icon-block bg-info text-white mr-3">
                                <i class="ti-user"></i>
                            </div>
                        </div>
                        <div>
                            <h6 class="font-size-13 line-height-22 primary-font m-b-5">مجموع کاربران</h6>
                            <h4 class="m-b-0 primary-font font-weight-bold line-height-30">
                                {{\App\Models\User::all()->count()}}
                            </h4>
                        </div>
                    </div>
                </a>
            </li>

        </ul>

        <ul id="navigationApps">
            <li class="navigation-divider">برنامه ها</li>

            <li><a href="{{route('admin.dashboard')}}">داشبرد</a> </li>
            <li><a href="{{route('admin.admins.index')}}"> اعضا </a> </li>
            <li><a href="{{route('admin.users.index')}}"> کاربران </a> </li>
            <li><a href="{{route('admin.product.index')}}"> محصولات </a> </li>
            <li><a href="{{route('admin.sale.index')}}"> فروش </a> </li>
            <li><a href="{{route('admin.productDiscount.index')}}">تخفیف </a> </li>
            <li><a href="{{route('admin.comment.index')}}"> نظرات  </a> </li>
            <li><a href="{{route('admin.category.index')}}"> دسته بندی </a> </li>
            <li><a href="{{route('admin.setting.index')}}">تنظیمات </a> </li>
            <li><a href="{{route('admin.carousel.index')}}"> اسلایدر </a> </li>
            <li><a href="{{route('admin.blog.index')}}"> وبلاگ  </a> </li>
            <li><a href="{{route('admin.brand.index')}}"> برند  </a> </li>


        </ul>
    </div>
</div>
<!-- end::navigation -->

@section('navJS')
    <script>
        $(document).ready(function () {

            var headerLocation = window.location.href;

            var spl = headerLocation.split("/");

            $('.navigation-menu-body a').each(function () {
                if ( ($(this).attr('href').includes(spl[4])) || $(this).attr('href') == headerLocation  || ($(this).attr('href') + '/') == headerLocation) {
                    $(this).addClass('active');
                    $(this).parents('ul').addClass('navigation-active');

                    var parentId = "#" + $(this).parents('ul').attr('id');

                    $("[href='"+parentId+"']").parent().addClass('active');
                }
            });
        });
    </script>
@stop
