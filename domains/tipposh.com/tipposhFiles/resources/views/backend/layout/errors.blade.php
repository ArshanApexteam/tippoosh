@if (session('prossesOk'))
    <div class="alert alert-success">
        {{ session('prossesOk') }}
    </div>
@endif
@if (session('prossesError'))
    <div class="alert alert-danger">
        {{ session('prossesError') }}
    </div>
@endif
@if ($errors->any())
    <p class="errorTitle">لطفا خطا های زیر را برطرف نمایید</p>
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif