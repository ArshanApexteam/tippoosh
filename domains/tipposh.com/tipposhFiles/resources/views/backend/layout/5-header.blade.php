
<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="{{route('admin.dashboard')}}">
            @php $logo = \App\Models\Setting::where('key' ,"logo" )->first(); @endphp
            <img src="{{asset($logo->value)}}" class="large-logo" style="width: 70px ; height: 50px">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            @yield('pageInfo')

        </div>


    </div>
    <!-- end::header body -->

</div>
<!-- end::header -->
