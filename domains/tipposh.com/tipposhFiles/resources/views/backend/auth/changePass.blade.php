<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>تغییر رمز عبور</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href='{{asset("assets/backend/media/image/favicon.png")}}'>

	<!-- Theme Color -->
	<meta name="theme-color" content="#5867dd">

	<!-- Plugin styles -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/bundle.css")}}' type="text/css">

	<!-- Datepicker -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/datepicker/daterangepicker.css")}}'>

	<!-- Slick -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick.css")}}'>
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick-theme.css")}}'>

	<!-- Vmap -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/vmap/jqvmap.min.css")}}'>

	<!-- App styles -->
	<link rel="stylesheet" href='{{asset("assets/backend/css/app.css")}}' type="text/css">

	@yield('css')


</head>

<body class="form-membership">

	<!-- begin::page loader-->
	<div class="page-loader">
		<div class="spinner-border"></div>
	</div>
	<!-- end::page loader -->

	<div class="form-wrapper">

		<!-- logo -->
		<div class="logo">
			<img src="{{asset("$appSettings->logo")}}" width="70" alt="image">
		</div>
		<!-- ./ logo -->

		<h5>ورود</h5>

		<!-- form -->
		<form action="{{route('admin.changePass')}}" method="post">
			@csrf

			<input type="hidden" name="email" value="{{$data['userEmil']}}">

			<div class="form-group">
				<input type="password" name="password" class="form-control text-left" placeholder="رمز عبور" dir="ltr" required>
			</div>

			<button class="btn btn-primary btn-block">ورود</button>
			<hr>
		</form>
		<!-- ./ form -->

	</div>

	<script src='{{asset("assets/backend/vendors/bundle.js")}}'></script>
	<script src='{{asset("assets/backend/js/app.js")}}'></script>
</body>

</html>