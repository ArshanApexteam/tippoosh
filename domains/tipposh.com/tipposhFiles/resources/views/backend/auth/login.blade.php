<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ورود به پنل مدیریتی</title>


	<!-- Favicon -->
	<link rel="shortcut icon" href='{{asset("assets/backend/media/image/favicon.png")}}'>

	<!-- Theme Color -->
	<meta name="theme-color" content="#5867dd">

	<!-- Plugin styles -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/bundle.css")}}' type="text/css">

	<!-- Datepicker -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/datepicker/daterangepicker.css")}}'>

	<!-- Slick -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick.css")}}'>
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/slick/slick-theme.css")}}'>

	<!-- Vmap -->
	<link rel="stylesheet" href='{{asset("assets/backend/vendors/vmap/jqvmap.min.css")}}'>

	<!-- App styles -->
	<link rel="stylesheet" href='{{asset("assets/backend/css/app.css")}}' type="text/css">

	@yield('css')


</head>

<body class="form-membership">

	<!-- begin::page loader-->
	<div class="page-loader">
		<div class="spinner-border"></div>
	</div>
	<!-- end::page loader -->

	<div class="form-wrapper">

		<!-- logo -->
		<div class="logo">
			@php $logo = \App\Models\Setting::where('key' ,"logo" )->first(); @endphp
			<img src="{{asset($logo->value)}}" alt='{{trans("translator.$logo->key")}}'
				 title='{{trans("translator.$logo->key")}}'  width="70">
		</div>
		<!-- ./ logo -->

		<h5>ورود</h5>

		<!-- form -->
		<form action="{{route('admin.authentication')}}" method="post">
			@csrf

			<div class="form-group">
				<input type="email" name="email" class="form-control text-left" placeholder="نام کاربری یا ایمیل" dir="ltr"
					   required autofocus {{old('email')}}>
			</div>
			<div class="form-group">
				<input type="password" name="password" class="form-control text-left" placeholder="رمز عبور" dir="ltr" required>
			</div>
			<div class="form-group d-sm-flex justify-content-between text-left mb-4">
				<div class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input" checked id="customCheck1">
					<label class="custom-control-label" for="customCheck1">به خاطر سپاری</label>
				</div>
				<a class="d-block mt-2 mt-sm-0" href="{{route('admin.forgetPassForm')}}">بازنشانی رمز عبور</a>
			</div>
			<button class="btn btn-primary btn-block">ورود</button>
			<hr>
		</form>
		<!-- ./ form -->

	</div>

	<script src='{{asset("assets/backend/vendors/bundle.js")}}'></script>
	<script src='{{asset("assets/backend/js/app.js")}}'></script>
</body>

</html>