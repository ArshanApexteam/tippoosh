@extends('backend.layout.app')

@section('title') نمایش خدمات درخواستی   @stop
@section('pageInfo')
    <h3 class="page-title">خدمات درخواستی</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.userRequestService.index')}}"> خدمات درخواستی</a></li>
            <li class="breadcrumb-item active" aria-current="page">نمایش</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop
@section('css')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">نمایش خدمات درخواستی </h6>

                        <form method="post" action="{{route('admin.userRequestService.update' , $service->id )}}">
                            @method('put')
                            @csrf

                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <p><b>نام کاربر : </b> <span>{{$service->name}}</span> </p>
                                    <p><b>ایمیل کاربر : </b> <span>{{$service->mail}}</span> </p>
                                    <p><b>شماره تماس کاربر : </b> <span>{{$service->phone}}</span> </p>
                                    <p><b>خدمات درخواستی : </b> <span>{{$service->service->title}}</span> </p>
                                    <p><b>تاریخ ثبت درخواست : </b> <span>{{ \Verta::instance($service->created_at)->format('d %B Y') }}</span> </p>
                                    <p><b>خلاصه درخواست کاربر : </b> <span>{{$service->summary}}</span> </p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label for="validationServer04">وضعیت</label>
                                    <select name="status" id="" class="form-control">
                                        <option value="0" @if($service->status == 0 ) selected @endif>درخواست اولیه
                                        </option>
                                        <option value="1" @if($service->status == 1 ) selected @endif> درحال انجام
                                        </option>
                                        <option value="2" @if($service->status == 2 ) selected @endif> پایان کار
                                        </option>
                                    </select>
                                </div>

                            </div>

                            <button class="btn btn-primary" type="submit">ثبت</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </main>
@stop