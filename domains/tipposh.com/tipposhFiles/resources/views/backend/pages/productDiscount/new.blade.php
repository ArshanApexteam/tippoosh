@extends('backend.layout.app')

@section('title') تخفیف جدید  @stop
@section('pageInfo')
    <h3 class="page-title">تخفیف</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.productDiscount.index')}}"> تخفیف</a></li>
            <li class="breadcrumb-item active" aria-current="page">جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">اضافه کردن تخفیف جدید</h6>
                        <form method="post" action="{{route('admin.productDiscount.store')}}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="price">میزان تخفیف</label>
                                    <input type="text" class="form-control" id="price" name="price"
                                           placeholder="میزان تخفیف" value="{{old('price')}}" required>
                                </div>

                            </div>

                            <button class="btn btn-primary" type="submit">ثبت</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </main>
@stop
