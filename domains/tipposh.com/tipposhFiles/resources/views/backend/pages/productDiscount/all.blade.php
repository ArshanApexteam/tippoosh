@extends('backend.layout.app')

@section('title') همه تخفیف ها  @stop

@section('pageInfo')
    <h3 class="page-title">تخفیف</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.productDiscount.index')}}"> تخفیف</a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')  @stop
@section('js')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست تخفیف ها </h6>

                        <a href="{{route('admin.productDiscount.create')}}" title="اضافه کردن رکورد جدید" class="float-right">
                            <button type="button" class="btn btn-whatsapp btn-floating btn-lg btn-pulse">
                                <i class="ti-plus"></i>
                            </button>
                        </a>


                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">کد</th>
                                    <th scope="col">مبلغ</th>
                                    <th scope="col">وضعیت</th>
                                    <th scope="col">محصول استفاده شده</th>
                                    <th scope="col">کاربر استفاده کننده </th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>
                                            @if($item->status == 0)
                                                <span class="p-1 rounded bg-info">استفاده نشده</span>
                                            @else
                                                <span class="p-1 rounded bg-success">استفاده شده</span>
                                            @endif
                                        </td>

                                        <td>{{$item->product}}</td>
                                        <td>{{$item->user}}</td>

                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.productDiscount.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                            ویرایش و بروزرسانی
                                                        </button>
                                                    </a>



                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.productDiscount.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$all->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop
