@extends('backend.layout.app')

@section('title') منو جدید  @stop
@section('pageInfo')
	<h3 class="page-title">منو</h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a href="{{route('admin.menu.index')}}"> منو</a></li>
			<li class="breadcrumb-item active" aria-current="page">جدید</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')  @stop

@section('content')
	<main class="main-content">
		<div class="row">

			<div class="col-md-12">
				@include('backend.layout.errors')
			</div>

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">اضافه کردن منو جدید</h6>
						<form method="post" action="{{route('admin.menu.store')}}">
							@csrf

							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label for="validationServer01">نام</label>
									<input type="text" class="form-control" id="validationServer01" name="name"
										   placeholder="نام" value="{{old('name')}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer01">لینک</label>
									<select class="form-control" id="validationServer02" name="link">
										<option value="admin.dashboard">{{ trans('translator.admin.dashboard') }}</option>
										<option value="admin.category.index">{{ trans('translator.admin.category.index') }}</option>
										<option value="admin.carousel.index">{{ trans('translator.admin.carousel.index') }}</option>
										<option value="admin.service.index">{{ trans('translator.admin.service.index') }}</option>
										<option value="admin.setting.index">{{ trans('translator.admin.setting.index') }}</option>
										<option value="admin.menu.index">{{ trans('translator.admin.menu.index') }}</option>
										<option value="admin.blog.index">{{ trans('translator.admin.blog.index') }}</option>
										<option value="admin.product.index">{{ trans('translator.admin.product.index') }}</option>
										<option value="admin.brand.index">{{ trans('translator.admin.brand.index') }}</option>
										<option value="admin.admins.index">{{ trans('translator.admin.admins.index') }}</option>
										<option value="admin.users.index">{{ trans('translator.admin.users.index') }}</option>
										<option value="admin.productComment.index">{{ trans('translator.admin.productComment.index') }}</option>
										{{--<option value="admin.newsletter.index">{{ trans('translator.admin.newsletter.index') }}</option>--}}
										<option value="admin.userRequestService.index">{{ trans('translator.admin.userRequestService.index') }}</option>
									</select>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer04">وضعیت</label>
									<div class="form-group">
										<div class="custom-control custom-switch custom-checkbox-success">
											<input type="checkbox" class="custom-control-input" id="customSwitch2_" checked name="status">
											<label class="custom-control-label" for="customSwitch2_">نمایش یا عدم نمایش</label>
										</div>
									</div>
								</div>


							</div>

							<button class="btn btn-primary" type="submit">ثبت </button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</main>
@stop