@extends('backend.layout.app')

@section('title') همه سفارش ها  @stop

@section('pageInfo')
    <h3 class="page-title">سفارش</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.brand.index')}}"> سفارش</a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')  @stop
@section('js')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست سفارش ها </h6>

                        <p>در لیست زیر ممکن است کاربرانی که هنوز عضو سایت نشده اند هم سفارش داده باشند که به محض ورود به سایت اطلاعاتشان ثبت میگردد </p>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">کاربر</th>
                                    <th scope="col">محصول مورد نظر</th>
                                    <th scope="col">عکس</th>
                                    <th scope="col">وضعیت سفارش</th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>
                                            @if($item->user)
                                                {{$item->user->name}} <br>
                                                {{$item->user->phone}} 
                                            @else
                                                کاربر هنوز عضو سایت نشده است
                                            @endif

                                        </td>

                                        <td>   
                                            @isset($item->product->gallery[0])          
                                                <a href="{{asset($item->product->gallery[0]->photo)}}">
                                                    <img src="{{asset($item->product->gallery[0]->photo)}}" alt="">
                                                </a>
                                            @else
                                                {{ $item->product->name_fa }}
                                            @endif
                                        </td>

                                        <td><img src="{{asset($item->photo)}}" alt="" ></td>
                                        <td>
                                            @if($item->status == 0)
                                                ثبت اولیه و منتظر پرداخت وجه
                                            @else
                                                پرداخت شده و آماده تحویل
                                            @endif
                                        </td>

                                        
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.order.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                           تغییر به پرداخت شده
                                                        </button>
                                                    </a>

                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.order.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop
