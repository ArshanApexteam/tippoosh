@extends('backend.layout.app')

@section('title') نمایش اعضا  @stop

@section('pageInfo')
    <h3 class="page-title">اعضا </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.admins.index')}}"> اعضا </a></li>
            <li class="breadcrumb-item active" aria-current="page">نمایش</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')
    <style>
        label {
            margin-top: 0;
        }

        .select2-search__field {
            padding: 3px 10px !important;
        }

        ul.select2-selection__rendered > li {
            float: right !important;
        }
    </style>
@stop
@section('js')
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/select2/css/select2.min.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/vendors/select2/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();

    </script>
@endsection




@section('content')
    <main class="main-content" data-ng-app="myApp" data-ng-controller="blogJsController">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title">نمایش مدیر</h6>

                        <form method="POST" action="{{ route('admin.admins.update' , $admin->id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')


                            <div class="form-row">
                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="title"> نام </label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{$admin->name}}" placeholder=" نام " required>

                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="phone"> شماره تلفن </label>
                                    <input type="text" id="phone" name="phone" class="form-control"
                                           value="{{$admin->phone}}" placeholder=" شماره تلفن " required>
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="password"> رمز عبور </label>
                                    <input type="password" id="password" name="password" class="form-control"
                                           placeholder=" رمز عبور ">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="file"> انتخاب عکس </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="file" name="file">
                                        <label class="custom-file-label" for="file">انتخاب </label>
                                    </div>
                                </div>

                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="address"> آدرس </label>
                                    <input type="text" id="address" name="address" class="form-control"
                                           value="{{$admin->address}}" placeholder=" آدرس " required>

                                </div>
                            </div>

{{--                            <div class="form-row">--}}
{{--                                <div class="col-md-12 mt-3 mb-3">--}}
{{--                                    <label for="menu_id"> انتخاب مجوز دسترسی برای مدیر</label>--}}
{{--                                    <select name="rule[]" id="" class="form-control select2" multiple="multiple"--}}
{{--                                            data-placeholder="انتخاب کنید">--}}
{{--                                        @foreach($actions as $action)--}}
{{--                                            <option value="{{$action->id}}">{{ trans("translator.$action->action") }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}


                            <button class="btn btn-primary submit" type="submit">ثبت</button>
                        </form>

                        <br><br><br><br>
{{--                        <h6 class="card-title">مجوز های مدیر</h6>--}}
{{--                        <div class="form-row">--}}
{{--                            @foreach ($admin->permision->chunk(10) as $chunk)--}}
{{--                                <div class="col-md-3 mt-1 mb-3">--}}
{{--                                    <table class="table table-bordered table-striped">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th> نقش</th>--}}
{{--                                            <th style="width: 30px"> حذف</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @foreach ($chunk as $item)--}}
{{--                                            <tr>--}}
{{--                                                @php $trans = $item->action->action;  @endphp--}}
{{--                                                <td>{{ trans("translator.$trans") }}</td>--}}
{{--                                                <td class="text-center">--}}
{{--                                                    <a href="{{ route('admin.rule.delete', $item->id)}}"--}}
{{--                                                       class='fa fa-close'></a>--}}
{{--                                                </td>--}}
{{--                                            </tr>--}}
{{--                                        @endforeach--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
