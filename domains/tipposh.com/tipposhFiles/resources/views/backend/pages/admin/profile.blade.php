@extends('backend.layout.app')

@section('title') پروفایل  @stop

@section('pageInfo')
    <h3 class="page-title">اعضا </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.admins.index')}}"> اعضا </a></li>
            <li class="breadcrumb-item active" aria-current="page">پروفایل</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')@stop
@section('js')@stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title">پروفایل</h6>

                        <form method="POST" action="{{ route('admin.profileUpdate') }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="form-row">
                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="title"> نام </label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{$admin->name}}" placeholder=" نام " required>

                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="phone"> شماره تلفن </label>
                                    <input type="text" id="phone" name="phone" class="form-control"
                                           value="{{$admin->phone}}" placeholder=" شماره تلفن " required>
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="password"> رمز عبور </label>
                                    <input type="password" id="password" name="password" class="form-control"
                                           placeholder=" رمز عبور ">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="file"> انتخاب عکس </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="file" name="file">
                                        <label class="custom-file-label" for="file">انتخاب </label>
                                    </div>
                                </div>

                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="address"> آدرس </label>
                                    <input type="text" id="address" name="address" class="form-control"
                                           value="{{$admin->address}}" placeholder=" آدرس " required>

                                </div>
                            </div>

                            <button class="btn btn-primary submit" type="submit">ثبت</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop