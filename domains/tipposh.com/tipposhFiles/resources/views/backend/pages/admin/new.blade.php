@extends('backend.layout.app')

@section('title')  اعضا جدید @stop

@section('pageInfo')
    <h3 class="page-title">اعضا </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.admins.index')}}"> اعضا </a></li>
            <li class="breadcrumb-item active" aria-current="page">جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')
    <style>
        label {
            margin-top: 0;
        }

        .select2-search__field {
            padding: 3px 10px !important;
        }

        ul.select2-selection__rendered > li {
            float: right !important;
        }
    </style>
@stop
@section('js')
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/select2/css/select2.min.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/vendors/select2/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();

    </script>
@endsection


@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title"> مدیر جدید</h6>

                        <form method="POST" action="{{ route('admin.admins.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-row">
                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="title"> نام </label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{old('name')}}" placeholder=" نام " required>

                                </div>

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="mail"> ایمیل </label>
                                    <input type="text" id="mail" name="mail" class="form-control"
                                           value="{{old('mail')}}" placeholder=" ایمیل " required>
                                </div>

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="address"> آدرس </label>
                                    <input type="text" id="address" name="address" class="form-control"
                                           value="{{old('address')}}" placeholder=" آدرس " required>

                                </div>

                            </div>

                            <div class="form-row">

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="phone"> شماره تلفن </label>
                                    <input type="text" id="phone" name="phone" class="form-control"
                                           value="{{old('phone')}}" placeholder=" شماره تلفن " required>
                                </div>

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="file"> انتخاب عکس </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="file" name="file">
                                        <label class="custom-file-label" for="file">انتخاب </label>
                                    </div>
                                </div>

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="password"> رمز عبور </label>
                                    <input type="password" id="password" name="password" class="form-control"
                                           placeholder=" رمز عبور ">
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="menu_id"> انتخاب مجوز دسترسی برای مدیر</label>
                                    <select name="rule[]" id="" class="form-control select2" multiple="multiple"
                                            data-placeholder="انتخاب کنید">
                                        @foreach($actions as $action)
                                            <option value="{{$action->id}}">{{ trans("translator.$action->action") }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <button class="btn btn-primary submit" type="submit">ثبت</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection