@extends('backend.layout.app')

@section('title') تنظیمات سایت   @stop
@section('pageInfo')
    <h3 class="page-title">تنظیمات سایت</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item active" aria-current="page"> تنظیمات سایت</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop
@section('css')

    <style>
        .card-title {
            font-weight: 900 !important;
            margin-bottom: 5px !important;
            font-size: 20px;
        }
    </style>
@stop
@section('js')
    <!-- CKEditor -->
    <script src="{{asset('assets/backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/js/examples/ckeditor.js')}}"></script>

@stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('admin.setting.update' , $settings->id )}}"
                              enctype="multipart/form-data">
                            @method('put')
                            @csrf


                            <h6 class="card-title">تصاویر اصلی سایت</h6>
                            <div class="form-row">

                                <div class="col-md-6 mb-3">
                                    <label for="logo">انتخاب لوگو سایت</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="logo" name="logo">
                                        <label class="custom-file-label" for="logo"> انتخاب لوگو سایت</label>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="headerBackground">تصویر پس زمینه هدر تمام صفحات</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="headerBackground"
                                               name="headerBackground">
                                        <label class="custom-file-label" for="headerBackground">تصویر پس زمینه هدر تمام
                                            صفحات</label>
                                    </div>
                                </div>

                            </div>


                            <br><br>
                            <h6 class="card-title">تماس با ما</h6>
                            <div class="form-row">

                                <div class="col-md-9 mb-3">
                                    <label for="location_title">تیتر</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="location_title" name="location_title"
                                               placeholder="تیتر" value="{{$settings->location_title}}" required>
                                    </div>
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="location_image">تصویر موقعیت جغرافیایی</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="location_image"
                                               name="location_image">
                                        <label class="custom-file-label" for="location_image">انتخاب عکس</label>
                                    </div>
                                </div>

                                <div class="col-md-9 mb-3">
                                    <label for="address">آدرس</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="address" name="address"
                                               placeholder="آدرس" value="{{$settings->address}}" required>
                                    </div>
                                </div>


                                <div class="col-md-3 mb-3">
                                    <label for="phone">شماره تلفن</label>
                                    <input type="text" class="form-control" id="phone" name="phone"
                                           placeholder="شماره تلفن" value="{{$settings->phone}}" required>
                                </div>


                                <div class="col-md-12 m-1 mb-3">
                                    <label for="location_description">توضیحات</label>
                                    <textarea id="editor-demo3"
                                              name="location_description">{{$settings->location_description}}</textarea>
                                </div>

                            </div>


                            <br><br>
                            <h6 class="card-title">تایم کاری</h6>
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="time">تایم کاری</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="time" name="time"
                                               placeholder="تایم کاری" value="{{$settings->time}}" required>
                                    </div>
                                </div>
                            </div>


                            <br><br>
                            <h6 class="card-title">شبکه های اجتماعی</h6>
                            <div class="form-row">

                                <div class="col-md-4 mb-3">
                                    <label for="telegram">تلگرام</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="telegram" name="telegram"
                                               placeholder="تلگرام" value="{{$settings->telegram}}" required>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="instagram">اینستاگرام</label>
                                    <input type="text" class="form-control" id="instagram" name="instagram"
                                           placeholder="اینستاگرام" value="{{$settings->instagram}}" required>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="google">ایمیل</label>
                                    <input type="text" class="form-control" id="google" name="google"
                                           placeholder="ایمیل" value="{{$settings->google}}" required>
                                </div>


                            </div>


                            <br><br>
                            <h6 class="card-title">توضیحات فوتر</h6>
                            <div class="form-row  px-1">
                                <div class="col-md-12 mb-3">
                                    <label for="telegram">توضیحات فوتر</label>
                                    <textarea id="editor-demo2"
                                              name="footer_description">{{$settings->footer_description}}</textarea>
                                </div>

                            </div>


                            <br><br>
                            <h6 class="card-title"> خدمات</h6>
                            <div class="form-row">

                                <div class="col-md-6 mb-3">
                                    <label for="service_image">تصویر اصلی</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="service_image"
                                               name="service_image">
                                        <label class="custom-file-label" for="service_image">تصویر اصلی</label>
                                    </div>
                                </div>

                                <div class="col-md-12 m-1 mb-3">
                                    <label for="about_summary"> توضیحات</label>
                                    <textarea id="editor-demo5"
                                              name="service_description">{{$settings->service_description}}</textarea>
                                </div>
                            </div>


                            <br><br>
                            <h6 class="card-title"> درباره ما</h6>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label for="about_title"> عنوان</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="about_title" name="about_title"
                                               placeholder=" عنوان" value="{{$settings->about_title}}" required>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="about_image">تصویر اصلی</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="about_image"
                                               name="about_image">
                                        <label class="custom-file-label" for="about_image">تصویر اصلی</label>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-3">
                                    <label for="about_summary"> توضیح کوتاه</label>
                                    <div class="custom-file">
                                        <textarea name="about_summary" id="about_summary" required  placeholder=" عنوان"
                                                  class="form-control">{{$settings->about_summary}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12 m-1 mb-3">
                                    <label for="about_summary"> توضیحات</label>
                                    <textarea id="editor-demo6"
                                              name="about_description">{{$settings->about_description}}</textarea>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="submit">ثبت</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </main>
@stop