@extends('backend.layout.app')

@section('title') نمایش اسلایدر   @stop
@section('pageInfo')
	<h3 class="page-title">اسلایدر</h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a href="{{route('admin.carousel.index')}}"> اسلایدر</a></li>
			<li class="breadcrumb-item active" aria-current="page">نمایش</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')  @stop

@section('content')
	<main class="main-content">
		<div class="row">

			<div class="col-md-12">
				@include('backend.layout.errors')
			</div>

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">نمایش اسلایدر </h6>
						<form method="post" action="{{route('admin.carousel.update' , $carousel->id )}}" enctype="multipart/form-data">
							@method('put')
							@csrf

							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label for="name_fa">اسم فارسی</label>
									<input type="text" class="form-control" id="name_fa" name="name_fa"
										   placeholder="اسم فارسی" value="{{$carousel->name_fa}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<label for="name_en">اسم انگلیسی</label>
									<input type="text" class="form-control" id="name_en" name="name_en"
										   placeholder="اسم انگلیسی" value="{{$carousel->name_en}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<label for="price">شروع قیمت</label>
									<input type="text" class="form-control" id="price" name="price"
										   placeholder="شروع از 100 هزار تومان" value="{{$carousel->price}}" required>
								</div>

							</div>

							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label for="validationServer01">عنوان</label>
									<input type="text" class="form-control" id="validationServer01" name="title"
										   placeholder="عنوان" value="{{$carousel->title}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer04">انتخاب عکس</label>
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="customFile" name="photo">
										<label class="custom-file-label" for="customFile">ابعاد تصویر 440*330</label>
									</div>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer04">وضعیت</label>
									<div class="form-group">
										<div class="custom-control custom-switch custom-checkbox-success">
											<input type="checkbox" class="custom-control-input" id="customSwitch2_"  name="status"
												   @if($carousel->status == 1 ) checked @endif >
											<label class="custom-control-label" for="customSwitch2_">نمایش یا عدم نمایش</label>
										</div>
									</div>
								</div>


							</div>

							<div class="form-row">
								<div class="col-md-12 mb-3">
									<div class="form-group">
										<label for="exampleFormControlTextarea1">توضیحات</label>
										<textarea class="form-control" id="exampleFormControlTextarea1" name="description"
												  rows="3">{{$carousel->description}}</textarea>
									</div>
								</div>
							</div>

							<button class="btn btn-primary" type="submit">ثبت </button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</main>
@stop