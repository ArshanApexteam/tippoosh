@extends('backend.layout.app')

@section('title') همه کاربران  @stop

@section('pageInfo')
	<h3 class="page-title">کاربران </h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}"> کاربران </a></li>
			<li class="breadcrumb-item active" aria-current="page">همه</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop

@section('js')
	<script src="{{ asset('assets/backend/demo/admins.js') }}"></script>
@endsection

@section('content')
	<main class="main-content">
		<div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">لیست کاربران </h6>

						<div class="table-responsive">
							<table class="table">
								<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">نام</th>
									<th scope="col">شماره تلفن </th>
									<th scope="col">ایمیل</th>
									<th class="text-right" scope="col">عمل</th>
								</tr>
								</thead>
								<tbody>
								@foreach($all as $key => $item)
									<tr>
										<th scope="row">{{$key + 1}}</th>
										<td>{{ $item->name}}</td>
										<td>{{ $item->phone}}</td>
										<td>{{ $item->email}}</td>

										<td class="text-right">
											<div class="dropdown">
												<a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
												   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right">
													<a href="{{route('admin.users.edit', $item->id)}}">
														<button class="dropdown-item" type="button">
															مشاهده و بروزرسانی
														</button>
													</a>

													<button class="dropdown-item" type="button"
															onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
													</button>

													<form id="deleteRecordFrom_{{$item->id}}"
														  action="{{route('admin.users.destroy', $item->id)}}"
														  method="post" style="display: none;">
														@csrf
														@method('delete')
													</form>

												</div>
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						
						<div class="col-md-12">
							{{$all->links()}}
						 </div>
						 
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection
