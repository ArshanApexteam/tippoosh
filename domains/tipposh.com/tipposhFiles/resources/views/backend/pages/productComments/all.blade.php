@extends('backend.layout.app')

@section('title') همه نظرات  @stop

@section('pageInfo')
    <h3 class="page-title">نظرات </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a  href="{{route('admin.productComment.index')}}"> نظرات </a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('js')
@endsection

@section('content')

    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست نظرات کاربران</h6>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">شماره</th>
                                    <th scope="col">کاربر</th>
                                    <th scope="col">پیام</th>
                                    <th scope="col"> وضعیت نمایش</th>
                                    <th scope="col">حذف</th>
                                    <th scope="col">مشاهده تاریخچه</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($all as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>
                                            @isset($item->user->name)
                                                {{ $item->user->name }}
                                            @endisset
                                        </td>
                                        <td>
                                            {{ $item->comment }}
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                <a href="{{ route('admin.productComment.changeStatus' , $item->id) }}">
                                                    <span style="color: green">فعال</span>
                                                </a>
                                            @else
                                                <a href="{{ route('admin.productComment.changeStatus' , $item->id) }}">
                                                    <span style="color: red">غیر فعال</span>
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.productComment.delete' ,  $item->id) }}">
                                                <span> حذف</span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.productComment.userHistory' ,  $item->user_id) }}">
                                                <span> تاریخچه</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mt-3 mb-3">
                                <form action="{{route('admin.productComment.replay')}}" method="get">
                                    <h6 class="card-title">پاسخ</h6>
                                    <input type="text" name="comment_id" class="form-control"
                                           placeholder="شماره کامنت مورد نظر">
                                    <br>
                                    <textarea name="text" id="" rows="5" class="form-control"></textarea>
                                    <br>
                                    <input type="submit" class="btn btn-primary pull-left" value="ذخیره">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection