@extends('backend.layout.app')

@section('title')  تاریخچه  @stop

@section('pageInfo')
    <h3 class="page-title">نظرات </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.productComment.index')}}"> نظرات </a></li>
            <li class="breadcrumb-item active" aria-current="page">تاریخچه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('js')
@endsection

@section('content')

    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">
                            تاریخچه :
                            @isset($all[0]->user->name)
                                {{ $all[0]->user->name }}
                            @endisset
                        </h6>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">شماره</th>
                                    <th scope="col">پیام</th>
                                    <th scope="col"> وضعیت نمایش</th>
                                    <th scope="col">کاربر</th>
                                    <th scope="col">حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($all as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->comment }}</td>
                                        <td>
                                            @if($item->status == 1)
                                                <a href="{{ route('admin.productComment.changeStatus' , $item->id) }}"><span
                                                            style="color: green">فعال</span></a>
                                            @else
                                                <a href="{{ route('admin.productComment.changeStatus' , $item->id) }}"><span
                                                            style="color: red">غیر فعال</span></a>
                                            @endif
                                        </td>
                                        <td>پیام خود کاربر</td>
                                        <td>
                                            <a href="{{ route('admin.productComment.delete' ,  $item->id) }}"><span> حذف</span></a>
                                        </td>
                                    </tr>

                                    @foreach ($item->Childs as $replay)
                                        <tr>
                                            <td>{{ $replay->id }}</td>
                                            <td>{{ $replay->comment }}</td>
                                            <td>
                                                @if($replay->status == 1)
                                                    <a href="{{ route('admin.productComment.changeStatus' , $replay->id) }}"><span
                                                                style="color: green">فعال</span></a>
                                                @else
                                                    <a href="{{ route('admin.productComment.changeStatus' , $replay->id) }}"><span
                                                                style="color: red">غیر فعال</span></a>
                                                @endif
                                            </td>
                                            <td>
                                                @if($replay->user_is_admin == 1 )
                                                    پاسخ مدیر
                                                @elseif($replay->user->id == $all[0]->user->id )
                                                    پاسخ خود کاربر
                                                @else
                                                    پاسخ کاربران دیگر
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.productComment.delete' ,  $replay->id) }}"><span> حذف</span></a>
                                            </td>
                                        </tr>

                                        @foreach ($replay->Childs as $replay2)
                                            <tr>
                                                <td>{{ $replay2->id }}</td>
                                                <td>{{ $replay2->comment }}</td>
                                                <td>
                                                    @if($replay2->status == 1)
                                                        <a href="{{ route('admin.productComment.changeStatus' , $replay2->id) }}"><span
                                                                    style="color: green">فعال</span></a>
                                                    @else
                                                        <a href="{{ route('admin.productComment.changeStatus' , $replay2->id) }}"><span
                                                                    style="color: red">غیر فعال</span></a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($replay2->user_is_admin == 1 )
                                                        پاسخ مدیر
                                                    @elseif($replay2->user->id == $all[0]->user->id )
                                                        پاسخ خود کاربر
                                                    @else
                                                        پاسخ کاربران دیگر
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.productComment.delete' ,  $replay2->id) }}"><span> حذف</span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                                </tbody>

                            </table>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mt-3 mb-3">
                                <form action="{{route('admin.productComment.replay')}}" method="get">
                                    <h6 class="card-title">پاسخ</h6>
                                    <input type="text" name="comment_id" class="form-control"
                                           placeholder="شماره کامنت مورد نظر">
                                    <br>
                                    <textarea name="text" id="" rows="5" class="form-control"></textarea>
                                    <br>
                                    <input type="submit" class="btn btn-primary pull-left" value="ذخیره">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection