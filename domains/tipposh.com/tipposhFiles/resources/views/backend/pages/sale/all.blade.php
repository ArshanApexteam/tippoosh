@extends('backend.layout.app')

@section('title')  فروش  @stop

@section('pageInfo')
    <h3 class="page-title">فروش </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> فروش </a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('js')
    <script src="{{ asset('assets/backend/demo/product.js') }}"></script>
@endsection


@section('css')
    <style>
        .table td , .table th {
            white-space: initial !important;
            line-height: 1.5 !important;
            text-align: center !important;
        }
    </style>
@endsection

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>


            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست فروش </h6>


                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">نام محصول</th>
                                    <th scope="col">نام خریدار</th>
                                    <th scope="col">شماره خریدار</th>
                                    <th scope="col">آدرس خریدار</th>
                                    <th scope="col">درخواست سفارشی </th>
                                    <th scope="col">تعداد</th>
                                    <th scope="col">سایز</th>
                                    <th scope="col">رنگ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$item->product->name_fa}}</td>
                                        <td>
                                            <a href="{{route('admin.users.edit', $item->user->id)}}">{{ $item->user->name }}</a>
                                        </td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->address}}</td>
                                        <td>
                                            @if($item->photo)
                                                <img src="{{ asset($item->photo) }}">
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $item->count }} </td>
                                        <td>{{ $item->size }} </td>
                                        <td> {{ $item->color }} </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            {{$all->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>



@endsection
