@extends('backend.layout.app')

@section('title') دسته بندی جدید  @stop
@section('pageInfo')
	<h3 class="page-title">دسته بندی</h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a> دسته بندی</a></li>
			<li class="breadcrumb-item active" aria-current="page">جدید</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')  @stop

@section('content')
	<main class="main-content">
		<div class="row">

			<div class="col-md-12">
				@include('backend.layout.errors')
			</div>

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">اضافه کردن دسته بندی جدید</h6>
						<form method="post" action="{{route('admin.category.store')}}">
							@csrf

							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label for="validationServer01">نام</label>
									<input type="text" class="form-control" id="validationServer01" name="name"
										   placeholder="نام" value="{{old('name')}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<div class="form-group">
										<label for="validationServer02">انتخاب سر شاخه</label>
										<select class="form-control" id="validationServer02" name="parent_id">
											<option value="0">----</option>
											@foreach($allCategoreis as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@foreach($item->childs as $subcate)
													<option value="{{$subcate->id}}">------>{{$subcate->name}}</option>
													@foreach($subcate->childs as $subSubcate)
														<option value="{{$subSubcate->id}}">----------->{{$subSubcate->name}}</option>
													@endforeach
												@endforeach
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer04">وضعیت</label>
									<div class="form-group">
										<div class="custom-control custom-switch custom-checkbox-success">
											<input type="checkbox" class="custom-control-input" id="customSwitch2_" checked name="status">
											<label class="custom-control-label" for="customSwitch2_">نمایش یا عدم نمایش</label>
										</div>
									</div>
								</div>


							</div>

							<button class="btn btn-primary" type="submit">ثبت </button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</main>
@stop