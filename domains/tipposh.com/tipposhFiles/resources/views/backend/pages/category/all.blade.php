@extends('backend.layout.app')

@section('title') همه دسته بندی ها  @stop

@section('pageInfo')
    <h3 class="page-title">دسته بندی</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> دسته بندی</a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')  @stop
@section('js')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست دسته بندی </h6>

                        <a href="{{route('admin.category.create')}}" title="اضافه کردن رکود جدید" class="float-right">
                            <button type="button" class="btn btn-whatsapp btn-floating btn-lg btn-pulse">
                                <i class="ti-plus"></i>
                            </button>
                        </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">نام</th>
                                    <th scope="col">سر شاخه</th>
                                    <th scope="col">وضعیت نمایش</th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $number = 1 ;  @endphp
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$number++}}</th>
                                        <td>{{$item->name}}</td>
                                        <td>
                                            @if($item->parent())
                                                {{$item->parent()->name}}
                                            @else
                                                ندارد
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                نمایش بده
                                            @else
                                                نمایش نده
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.category.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                            مشاهده و بروزرسانی
                                                        </button>
                                                    </a>

                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.category.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @foreach($item->childs as $index => $subItem)
                                        <tr>
                                            <th scope="row">{{$number++}}</th>
                                            <td>{{$subItem->name}}</td>
                                            <td>
                                                @if($subItem->parent())
                                                    {{$subItem->parent()->name}}
                                                @else
                                                    ندارد
                                                @endif
                                            </td>
                                            <td>
                                                @if($subItem->status == 1)
                                                    نمایش بده
                                                @else
                                                    نمایش نده
                                                @endif
                                            </td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                       data-toggle="dropdown" aria-haspopup="true"
                                                       aria-expanded="false">
                                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a href="{{route('admin.category.edit', $subItem->id)}}">
                                                            <button class="dropdown-item" type="button">
                                                                مشاهده و بروزرسانی
                                                            </button>
                                                        </a>

                                                        <button class="dropdown-item" type="button"
                                                                onclick="deleteRecordFromTable({{$subItem->id}})">حذف
                                                            کردن
                                                        </button>

                                                        <form id="deleteRecordFrom_{{$subItem->id}}"
                                                              action="{{route('admin.category.destroy', $subItem->id)}}"
                                                              method="post" style="display: none;">
                                                            @csrf
                                                            @method('delete')
                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop
