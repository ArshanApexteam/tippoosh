@extends('backend.layout.app')

@section('title') داشبرد  @stop
@section('css')  @stop

@section('content')
    <!-- begin::main content -->
    <main class="main-content">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">درآمد های اخیر شما</h6>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="d-flex align-items-center m-b-20">
                                    <div class="icon-block m-r-15 icon-block-lg icon-block-outline-success text-success">
                                        <i class="fa fa-bar-chart"></i>
                                    </div>
                                    <div>
                                        <h6 class="font-size-13 primary-font">سود ناخالص</h6>
                                        <h4 class="m-b-0 primary-font line-height-28">1000 تومان </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="d-flex align-items-center m-b-20">
                                    <div class="icon-block m-r-15 icon-block-lg icon-block-outline-danger  text-danger">
                                        <i class="fa fa-hand-lizard-o"></i>
                                    </div>
                                    <div>
                                        <h6 class="font-size-13 primary-font">تخفیفات</h6>
                                        <h4 class="m-b-0 primary-font line-height-28">2000  تومان </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="d-flex align-items-center m-b-20">
                                    <div class="icon-block m-r-15 icon-block-lg icon-block-outline-warning text-warning">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <div>
                                        <h6 class="font-size-13 primary-font">سود خالص</h6>
                                        <h4 class="m-b-0 primary-font line-height-28"> 3000  تومان </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table m-b-0">
                                <thead>
                                <tr>
                                    <th>تاریخ</th>
                                    <th>تعداد فروش</th>
                                    <th>سود ناخالص</th>
                                    <th>تخفیف</th>
                                    <th class="text-right">سود خالص</th>
                                </tr>
                                </thead>
                                <tbody>
{{--                                @foreach($lastSale as $item)--}}
{{--                                    <tr>--}}
{{--                                        <td>{{$item['date']}}</td>--}}
{{--                                        <td>{{$item['count']}}</td>--}}
{{--                                        <td class="text-success">{{$item['mainPrice']}} تومان</td>--}}
{{--                                        <td class="text-danger">{{$item['offer']}} تومان</td>--}}
{{--                                        <td class="text-right">{{$item['resultPrice']}} تومان</td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="col-xl-12 col-md-12">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <h6 class="card-title m-b-20">گزارش فروش سالیانه</h6>--}}
{{--                        <canvas id="chart_demo_sale"></canvas>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </main>
    <!-- end::main content -->
@stop

@section('js')
    <script>
        'use strict';
        $(document).ready(function () {
            var colors = {
                primary: $('.colors .bg-primary').css('background-color'),
                primaryLight: $('.colors .bg-primary-bright').css('background-color'),
                secondary: $('.colors .bg-secondary').css('background-color'),
                secondaryLight: $('.colors .bg-secondary-bright').css('background-color'),
                info: $('.colors .bg-info').css('background-color'),
                infoLight: $('.colors .bg-info-bright').css('background-color'),
                success: $('.colors .bg-success').css('background-color'),
                successLight: $('.colors .bg-success-bright').css('background-color'),
                danger: $('.colors .bg-danger').css('background-color'),
                dangerLight: $('.colors .bg-danger-bright').css('background-color'),
                warning: $('.colors .bg-warning').css('background-color'),
                warningLight: $('.colors .bg-warning-bright').css('background-color'),
            };


            var chartColors = {
                primary: {
                    base: '#3f51b5',
                    light: '#c0c5e4'
                },
                danger: {
                    base: '#f2125e',
                    light: '#fcd0df'
                },
                success: {
                    base: '#0acf97',
                    light: '#cef5ea'
                },
                warning: {
                    base: '#ff8300',
                    light: '#ffe6cc'
                },
                info: {
                    base: '#00bcd4',
                    light: '#e1efff'
                },
                dark: '#37474f',
                facebook: '#3b5998',
                twitter: '#55acee',
                linkedin: '#0077b5',
                instagram: '#517fa4',
                whatsapp: '#25D366',
                dribbble: '#ea4c89',
                google: '#DB4437',
                borderColor: '#e8e8e8',
                fontColor: '#999'
            };

            chart_demo_sale();

            function chart_demo_sale() {
                if ($('#chart_demo_sale').length) {
                    var element = document.getElementById("chart_demo_sale");
                    element.height = 146;
                    new Chart(element, {
                        type: 'bar',
                        data: {
                            labels: ["1391", "1392", "1393", "1394", "1395", "1396", "1397", "1398"],
                            datasets: [
                                {
                                    label: "مجموع فروش",
                                    backgroundColor: colors.primary,
                                    data: [133, 221, 783, 978, 214, 421, 211, 577]
                                }, {
                                    label: "میانگین",
                                    backgroundColor: colors.info,
                                    data: [408, 947, 675, 734, 325, 672, 632, 213]
                                }
                            ]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            scales: {
                                xAxes: [{
                                    ticks: {
                                        fontSize: 11,
                                        fontColor: chartColors.fontColor
                                    },
                                    gridLines: {
                                        display: false,
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        fontSize: 11,
                                        fontColor: chartColors.fontColor
                                    },
                                    gridLines: {
                                        color: chartColors.borderColor
                                    }
                                }],
                            }
                        }
                    })
                }
            }
        });
    </script>
@stop
