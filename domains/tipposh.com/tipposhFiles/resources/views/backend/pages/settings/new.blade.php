@extends('backend.layout.app')

@section('title')  جدید  @stop

@section('pageInfo')
    <h3 class="page-title">تنظیمات </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.setting.index')}}"> تنظیمات </a></li>
            <li class="breadcrumb-item active" aria-current="page">جدید</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')
    <style>
        .el-display-none {
            display: none;
        }

        .el-display-block {
            display: block;
        }
    </style>
@stop

@section('content')

    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>


            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title">اضافه کردن </h6>

                        <form method="post" action="{{route('admin.setting.store')}}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-row">

                                <div class="col-md-5 mt-3 mb-3">
                                    <label for="status"> شما قصد ذخیره سازی چه چیزی دارید </label>
                                    <select id="status" name="type" class="form-control" placeholder=" وضعیت ">
                                        <option value="sum"> متن کوتاه</option>
                                        <option value="photo"> تصویر</option>
                                        <option value="des">متن بلند(بیش از 10 خط ، متن صفحه ای)</option>
                                    </select>
                                </div>


                                <div class="col-md-7 mt-3 mb-3">
                                    <label for="key"> کلید </label>
                                    <select id="key" name="key" class="form-control select2" placeholder=" کلید ">

                                        @foreach(\App\Models\SettingItems::all() as $item)
                                            <option value="{{$item->en_name}}">{{$item->fa_name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                            </div>


                            <div class="form-row el-display-block">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="summary"> متن </label>
                                    <input type="text" id="summary" name="summary" class="form-control"
                                           placeholder=" متن " value="{{old('summary')}}">
                                </div>
                            </div>

                            <div class="form-row el-display-none">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="photo"> انتخاب عکس </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="photo" name="photo">
                                        <label class="custom-file-label" for="photo">انتخاب عکس</label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-row el-display-none">
                                <div class="col-md-12 m-1 mb-3">
                                    <div class="form-group">
                                        <label for="description">متن</label>
                                        <textarea class="form-control" id="des" name="description"
                                                  placeholder="متن">{{old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>


                            <button class="btn btn-primary submit" type="submit">ثبت</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="{{asset('assets/backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('assets/backend/vendors/select2/css/select2.min.css') }}">
    <script type="text/javascript" src="{{ asset('assets/backend/vendors/select2/js/select2.min.js') }}"></script>
    
    <script>
        $('.select2').select2();

        $(function () {
            CKEDITOR.replace("des");
        });

        $('#status').on('change', function () {
            var element = $(this).val();
            $('.el-display-block').removeClass('el-display-block').addClass('el-display-none');
            $("#" + element).parents('.form-row').removeClass('el-display-none').addClass('el-display-block');
        })

    </script>
@stop
