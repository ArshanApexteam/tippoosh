@extends('backend.layout.app')

@section('title') تنظیمات  @stop

@section('pageInfo')
    <h3 class="page-title">تنظیمات </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.setting.index')}}"> تنظیمات </a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')
    <style>
        .table:not(.table-bordered) td {
            white-space: unset;
        }
    </style>
@endsection

@section('js') @endsection

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>


            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست تنظیمات </h6>

                        <a href="{{route('admin.setting.create')}}" title="اضافه کردن رکود جدید" class="float-right">
                            <button type="button" class="btn btn-whatsapp btn-floating btn-lg btn-pulse">
                                <i class="ti-plus"></i>
                            </button>
                        </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">کلید</th>
                                    <th scope="col" width="70%">مقدار</th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>

                                        <td>{{\App\Models\SettingItems::where('en_name',$item->key)->first()->fa_name}}</td>

                                        <td>
                                            @if($item->type == "photo")
                                                @if(file_exists($item->value))
                                                    <img src="{{asset($item->value)}}" alt="">
                                                @endif
                                            @elseif($item->type == "sum")
                                                {{$item->value}}
                                            @else
                                               متن صفحه ای ، برای دیدن کلید مشاهده را بزنید
                                            @endif
                                        </td>

                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.setting.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                            مشاهده و بروزرسانی
                                                        </button>
                                                    </a>

                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.setting.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection