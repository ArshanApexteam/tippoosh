@extends('backend.layout.app')

@section('title')  محصول نمایش  @stop

@section('pageInfo')
    <h3 class="page-title">محصولات </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> محصولات </a></li>
            <li class="breadcrumb-item active" aria-current="page">نمایش</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')

    <style>
        .newTagAdd {
            margin-left: 15px;
            float: right;
        }

        .newTagAdd span {
            color: #f70000;
            position: relative;
            top: -4px;
            margin-right: 2px;
            cursor: pointer;
            z-index: 100;
            float: left;
        }

        .colorSection {
        }

        .colorSection span {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            padding: 0px 20px;
            border: .7px solid;
            margin-left: 20px;
        }

        .colorSection i {
            color: red;
            position: absolute;
            margin-right: -3.8%;
            cursor: pointer;
        }

        .gallerySection img {
            margin-left: 20px;
        }

        .gallerySection i {
            color: red;
            margin-right: -33px;
            cursor: pointer;
            margin-left: 33px;
        }
    </style>

@stop

@section('content')

    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title">بروزرسانی کردن محصول</h6>

                        <form method="post" id="addForm" action="{{route('admin.product.update' , $product->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')


                            <div class="form-row">

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="name_fa"> نام فارسی </label>
                                    <input type="text" id="name_fa" name="name_fa" class="form-control"
                                           placeholder=" نام فارسی " value="{{$product->name_fa}}">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="name_en"> نام انگلیسی </label>
                                    <input type="text" id="name_en" name="name_en" class="form-control"
                                           placeholder=" نام انگلیسی " value="{{$product->name_en}}">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="validationServer04">وضعیت</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-switch custom-checkbox-success">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2_"
                                                   @if($product->status == 1) checked @endif name="status">
                                            <label class="custom-control-label" for="customSwitch2_">نمایش یا عدم
                                                نمایش</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="material"> جنس </label>
                                    <input type="text" id="material" name="material" class="form-control"
                                           placeholder=" جنس " value="{{$product->material}}">
                                </div>


                            </div>

                            <div class="form-row">
                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="color"> رنگ بندی </label>
                                    <select name="color[]" id="color" class="form-control select2" multiple="multiple"
                                            data-placeholder="انتخاب کنید">
                                            <option value="#000000"> سیاه</option>
                                            <option value="#fff"> سفید</option>
                                            <option value="#50cfff"> آبی</option>
                                            <option value="#ff0047"> قرمز</option>
                                            <option value="#00ff1f"> سبز</option>
                                            <option value="#fb00ff"> صورتی</option>
                                            <option value="#f69400"> نارنجی</option>
                                            <option value="#b36f55"> قهوه ای</option>
                                            <option value="#ffcd05"> زرد</option>
                                            <option value="#a922ab"> بنفش</option>
                                            <option value="#7e7e7e"> خاکستری</option>
                                            <option value="#a8a8a8"> طوسی</option>
                                            <option value="#9e9d99"> ملانژ</option>
                                    </select>
                                </div>
                                <div class="col-md-8 mt-5 mb-5 colorSection">
                                    @foreach ($product->color as $item)
                                        <span style="background-color: {{$item->color}};"></span>
                                        <i class="fa fa-close" title="حذف کردن" data-id="{{$item->id}}"></i>
                                    @endforeach
                                </div>
                            </div>


                            <div class="form-row">

                                <div class="col-md-3  mt-3 mb-3">
                                    <label for="category_id"> دسته بندی </label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="0"> ---</option>
                                        @foreach ($allCategory as $item)
                                            <option value="{{ $item->id }}" @isset($product->category->id)
                                            @if($item->id == $product->category->id)
                                            selected
                                                    @endif
                                                    @endisset
                                            > {{ $item->name }} </option>

                                            @foreach ($item->Childs as $child)
                                                <option value="{{ $child->id }}" @isset($product->category->id)
                                                @if($child->id == $product->category->id)
                                                selected
                                                        @endif
                                                        @endisset
                                                >---> {{ $child->name }} </option>
                                            @endforeach

                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="store"> موجودی به عدد </label>
                                    <input type="text" id="store" name="store" class="form-control"
                                           placeholder=" موجودی " value="{{$product->store}}">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="price"> قیمت به تومان</label>
                                    <input type="text" id="price" name="price" class="form-control"
                                           placeholder="قیمت " value="{{$product->price}}">
                                </div>

                                <div class="col-md-3 mt-3 mb-3">
                                    <label for="offer"> تخفیف به درصد </label>
                                    <input type="text" id="offer" name="offer" class="form-control"
                                           placeholder=" تخفیف " value="{{$product->offer}}">
                                </div>


                            </div>

                            <div class="form-row">
                                <div class="file-upload-previews"></div>
                                <div class="file-upload">
                                    <input type="file" name="files[]" class="file-upload-input with-preview" multiple
                                           title="Click to add files" accept="gif|jpg|png|jpeg">
                                    <span><i class="fa fa-plus-circle"></i>کلیک یا عکس خود را به اینجا درگ کنید</span>
                                </div>
                            </div>

                            <div class="form-row gallerySection">
                                @foreach ($product->gallery as $item)
                                    <img src="{{asset($item->photo)}}" alt="">
                                    <i class="fa fa-close" title="حذف کردن" data-id="{{$item->id}}"></i>
                                @endforeach
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="productTag"><span class="pull-left"> تگ ها : </span><span
                                                id="allProductTags">
                                           @foreach ($product->tags as $item)
                                                <b class="newTagAdd">{{ $item->caption }}<span
                                                            data-id={{ $item->id }}>x</span></b>
                                            @endforeach
                              </span></label><br>
                                    <input type="text" id="productTag" class="form-control" style="width:31.2% ;">
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="metakeywords"> کلمات کلیدی برای سئو </label>
                                    <textarea name="metakeywords" id="metakeywords" cols="30" rows="3"
                                              placeholder=" حتما کاما از هم جدا شوند "
                                              class="form-control">{{$product->metakeywords}}</textarea>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="metaDescription"> توضیحات برای سئو </label>
                                    <textarea name="metaDescription" id="metaDescription" cols="30" rows="5"
                                              placeholder=" حتما کاما از هم جدا شوند "
                                              class="form-control">{{$product->metaDescription}}</textarea>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="summery"> خلاصه توضیحات </label>
                                    <textarea name="summery" id="summery" cols="30" rows="5"
                                              placeholder=" خلاصه توضیحات  "
                                              class="form-control">{{$product->summery}}</textarea>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">توضیحات</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1"
                                                  name="description"
                                                  rows="3">{{$product->description}}</textarea>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" id="product_id" name="product_id" value="{{$product->id}}">

                            <input type="hidden" id="curentSidebarMenu" data-page="edit">

                            <button class="btn btn-primary submit" type="button">ثبت</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="{{asset('assets/backend/vendors/ckeditor/ckeditor.js')}}"></script>
    {{--<script src="{{asset('assets/backend/js/examples/ckeditor.js')}}"></script>--}}


    <script src="{{ asset('assets/backend/vendors/demo/product.js') }}"></script>

    {{-- multi Select File --}}
    <link href="{{asset('assets/backend/vendors/multiSelectFile/selectize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/backend/vendors/multiSelectFile/custom.css')}}" rel="stylesheet">
    <script src="{{asset('assets/backend/vendors/multiSelectFile/selectize.min.js')}}"></script>
    <script src="{{asset('assets/backend/vendors/multiSelectFile/custom.js')}}"></script>
    <script src="{{asset('assets/backend/vendors/multiSelectFile/jQuery.MultiFile.min.js')}}"></script>

    {{-- Select2 --}}
    <link href="{{asset('assets/backend/vendors/select2/css/select2.min.css')}}" rel="stylesheet">
    <script src="{{asset('assets/backend/vendors/select2/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>

@stop