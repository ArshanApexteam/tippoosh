@extends('backend.layout.app')

@section('title') نمایش خدمات   @stop
@section('pageInfo')
	<h3 class="page-title">خدمات</h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a> خدمات</a></li>
			<li class="breadcrumb-item active" aria-current="page">نمایش</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')
    <!-- CKEditor -->
    <script src="{{asset('assets/backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/js/examples/ckeditor.js')}}"></script>

@stop

@section('content')
	<main class="main-content">
		<div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">نمایش خدمات جدید</h6>
						<form method="post" action="{{route('admin.service.update' , $service->id )}}" enctype="multipart/form-data">
							@method('put')
							@csrf

                            <div class="form-row">
                                <div class="col-md-10 mb-3">
                                    <label for="validationServer01">عنوان</label>
                                    <input type="text" class="form-control" id="validationServer01" name="title"
                                           placeholder="عنوان" value="{{$service->title}}" required>
                                </div>

                                <div class="col-md-2 mb-3">
                                    <label for="validationServer04">وضعیت</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-switch custom-checkbox-success">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2_"
                                                   name="status" @if($service->status == 1 ) checked @endif >
                                            <label class="custom-control-label" for="customSwitch2_">نمایش یا عدم
                                                نمایش</label>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-row">

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="price">قیمت</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="price" name="price"
                                               placeholder=" قیمت به تومان" value="{{$service->price}}" required>
                                    </div>
                                </div>


                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="validationServer04">آیکن</label>
                                    <div class="custom-file">
                                        <input type="text" class="form-control" id="validationServer01" name="icon"
                                               placeholder="آیکن" value="{{$service->icon}}" required>
                                    </div>
                                </div>

                                <div class="col-md-4 mt-3 mb-3">
                                    <label for="validationServer04">انتخاب عکس</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="image">انتخاب عکس</label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">خلاصه توضیحات</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1"
                                                  name="summary"
                                                  rows="3">{{$service->summary}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row px-1">
                                <div class="col-md-12 mt-3 mb-3">

                                    <h6 class="card-title">توضیحات کامل</h6>
                                    <textarea id="editor-demo2" name="description">{{$service->description}}</textarea>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="submit">ثبت</button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</main>
@stop