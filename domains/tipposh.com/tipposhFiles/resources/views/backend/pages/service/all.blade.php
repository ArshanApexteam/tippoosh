@extends('backend.layout.app')

@section('title') همه خدمات   @stop

@section('pageInfo')
    <h3 class="page-title">خدمات</h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> خدمات</a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')  @stop
@section('js')  @stop

@section('content')
    <main class="main-content">
        <div class="row">

            <div class="col-md-12">
                @include('backend.layout.errors')
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست  خدمات</h6>

                        <a href="{{route('admin.service.create')}}" title="اضافه کردن رکود جدید" class="float-right">
                            <button type="button" class="btn btn-whatsapp btn-floating btn-lg btn-pulse">
                                <i class="ti-plus"></i>
                            </button>
                        </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">عنوان</th>
                                    <th scope="col">وضعیت نمایش</th>
                                    <th scope="col">عکس</th>
                                    <th scope="col">آیکن</th>
                                    <th scope="col">قیمت</th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$item->title}}</td>
                                        <td>
                                            @if($item->status == 1)
                                                نمایش بده
                                            @else
                                                نمایش نده
                                            @endif
                                        </td>

                                        <td><img src="{{asset($item->image_medium)}}" alt=""></td>

                                        <td><i class="fa {{$item->icon}}"></i></td>
                                        <td> {{$item->price}} تومان </td>

                                        
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.service.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                            مشاهده و بروزرسانی
                                                        </button>
                                                    </a>

                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.service.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop
