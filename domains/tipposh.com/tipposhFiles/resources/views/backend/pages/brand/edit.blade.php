@extends('backend.layout.app')

@section('title') نمایش برند   @stop
@section('pageInfo')
	<h3 class="page-title">برند</h3>
	<!-- begin::breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a>برنامه ها</a></li>
			<li class="breadcrumb-item"><a href="{{route('admin.brand.index')}}"> برند</a></li>
			<li class="breadcrumb-item active" aria-current="page">نمایش</li>
		</ol>
	</nav>
	<!-- end::breadcrumb -->
@stop
@section('css')  @stop
@section('js')  @stop

@section('content')
	<main class="main-content">
		<div class="row">

			<div class="col-md-12">
				@include('backend.layout.errors')
			</div>

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h6 class="card-title">نمایش برند </h6>
						<form method="post" action="{{route('admin.brand.update' , $brand->id )}}" enctype="multipart/form-data">
							@method('put')
							@csrf


							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label for="caption">عنوان</label>
									<input type="text" class="form-control" id="caption" name="caption"
										   placeholder="عنوان" value="{{$brand->caption}}" required>
								</div>

								<div class="col-md-4 mb-3">
									<label for="validationServer04">انتخاب عکس</label>
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="customFile" name="photo">
										<label class="custom-file-label" for="customFile">انتخاب فایل</label>
									</div>
								</div>

							</div>

							<button class="btn btn-primary" type="submit">ثبت </button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</main>
@stop