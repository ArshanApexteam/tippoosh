@extends('backend.layout.app')

@section('title')  نمایش پست   @stop

@section('pageInfo')
    <h3 class="page-title">وبلاگ </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> وبلاگ </a></li>
            <li class="breadcrumb-item active" aria-current="page">نمایش</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('css')

    <style>
        .newTagAdd {
            margin-left: 15px;
            float: right;
        }

        .newTagAdd span,
        .blogMoreImagesUrls span {
            color: #f70000;
            position: relative;
            top: -6px;
            margin-right: -8px;
            cursor: pointer;
            z-index: 100;
            float: left;
        }

        .blogMoreImagesUrls tr td {
            text-align: left
        }

        input[list] {
            width: 100%;
            height: 40px;
            padding: 15px;
            border: none;
            border-radius: 5px;
        }
    </style>

@stop

@section('content')

    <main class="main-content" data-ng-app="myApp" data-ng-controller="blogJsController">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-12">
                    @include('backend.layout.errors')
                </div>

                <div class="card">
                    <div class="card-body">

                        <h6 class="card-title">نمایش پست </h6>

                        <div class="form-row">
                            <div class="col-md-4" style="padding-right:0 ; padding-left:20px">
                                <label for="title">اضافه کردن عکس های داخل صفحه </label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="photo"
                                           onchange="angular.element(this).scope().uploadFile(this.files)">
                                    <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>مسیر عکس ها</th>
                                    </tr>
                                    </thead>
                                    <tbody class="blogMoreImagesUrls">
                                    @foreach ($blog->blogImages as $item)
                                        <tr>
                                            <td> http://127.0.0.1:8000/assets/images/blog/{{ $item->id }}.jpg <span
                                                        data-id="{{ $item->id }}">x</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <form method="post" id="addForm" action="{{route('admin.blog.update' , $blog->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="title"> عنوان </label>
                                    <input type="text" id="title" name="title" class="form-control"
                                           placeholder=" عنوان " value="{{$blog->title}}">
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="summary"> خلاصه </label>
                                    <textarea name="summary" id="summary" rows="5" class="form-control"
                                              placeholder="خلاصه مطلب">{{$blog->summary}}</textarea>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="menu_id"> دسته بندی </label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="0"> ---</option>
                                        @foreach ($allCategory as $item)
                                            <option value="{{ $item->id }}" @isset($blog->category()->first()->id)
                                            @if($item->id == $blog->category()->first()->id)
                                            selected
                                                    @endif
                                                    @endisset
                                            > {{ $item->name }} </option>

                                            @foreach ($item->Childs as $child)
                                                <option value="{{ $child->id }}" @isset($blog->category()->first()->id)
                                                @if($child->id == $blog->category()->first()->id)
                                                selected
                                                        @endif
                                                        @endisset
                                                >---> {{ $child->name }} </option>
                                            @endforeach

                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="key_word"> کلمه کلیدی اصلی </label>
                                    <input type="text" id="key_word" name="key_word" class="form-control"
                                           placeholder=" کلمه کلیدی اصلی " value="{{$blog->key_word}}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="validationServer04">وضعیت</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-switch custom-checkbox-success">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2_"
                                                   @if($blog->status == 1) checked @endif name="status">
                                            <label class="custom-control-label" for="customSwitch2_">نمایش یا عدم
                                                نمایش</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-md-3 mb-3">
                                    <label for="main_photo"> انتخاب عکس اصلی </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="main_photo" name="main_photo">
                                        <label class="custom-file-label" for="main_photo">انتخاب فایل</label>
                                    </div>
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="index_photo"> انتخاب عکس نمایه </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="index_photo"
                                               name="index_photo">
                                        <label class="custom-file-label" for="index_photo">انتخاب فایل</label>
                                    </div>

                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="image_alt">alt عکس </label>
                                    <input type="text" id="image_alt" name="image_alt" class="form-control"
                                           placeholder="  آلت عکس شاخص " value="{{$blog->image_alt}}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="iamge_title">title عکس </label>
                                    <input type="text" id="iamge_title" name="iamge_title" class="form-control"
                                           placeholder="  عنوان عکس شاخص " value="{{$blog->iamge_title}}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-12 mt-3 mb-3">
                                    <label for="blogTag"><span class="pull-left"> تگ ها : </span><span id="allBlogTags">
                                           @foreach ($allBlogTags as $item)
                                                <b class="newTagAdd">{{ $item->caption }}<span data-id={{ $item->id }}>x<span></b>
                                            @endforeach
                              </span></label><br>
                                    <input type="text" id="blogTag" class="form-control" style="width:31.2% ;">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-12 m-1 mb-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">توضیحات</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1"
                                                  name="description"
                                                  rows="3">{{$blog->description}}</textarea>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" id="blog_id" name="blog_id">
                            {{-- مورد نیاز آنگولار --}}
                            <input type="hidden" id="blog_images_id" name="blog_images_id">


                            <button class="btn btn-primary submit" type="button">ثبت</button>

                            <input type="hidden" id="curentSidebarMenu" data-page="edit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="{{asset('assets/backend/vendors/ckeditor/ckeditor.js')}}"></script>
    {{--<script src="{{asset('assets/backend/js/examples/ckeditor.js')}}"></script>--}}

    <!-- Angular -->
    <script src="{{ asset('assets/backend/vendors/Angular/angular.min.js')}}"></script>
    <script src="{{ asset('assets/backend/vendors/Angular/controller/angularJsApp.js') }}"></script>
    <script src="{{ asset('assets/backend/vendors/Angular/controller/blogJsController.js') }}"></script>

    <script src="{{ asset('assets/backend/vendors/demo/blog.js') }}"></script>

    <script>
        $('input[list]').on('change', function () {
            var options = $('option', this.list).map(function () {
                return this.value
            }).get();
            if (options.indexOf(this.value) === -1) {
                this.value = "";
            }
        });
    </script>

@stop