@extends('backend.layout.app')

@section('title') همه پست ها  @stop

@section('pageInfo')
    <h3 class="page-title">وبلاگ </h3>
    <!-- begin::breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a>برنامه ها</a></li>
            <li class="breadcrumb-item"><a> وبلاگ </a></li>
            <li class="breadcrumb-item active" aria-current="page">همه</li>
        </ol>
    </nav>
    <!-- end::breadcrumb -->
@stop

@section('js')
    <script src="{{ asset('assets/backend/demo/blog.js') }}"></script>
@endsection

@section('content')
    <main class="main-content">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-12">
                    @include('backend.layout.errors')
                </div>

                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">لیست پست ها </h6>

                        <a href="{{route('admin.blog.create')}}" title="اضافه کردن رکود جدید" class="float-right">
                            <button type="button" class="btn btn-whatsapp btn-floating btn-lg btn-pulse">
                                <i class="ti-plus"></i>
                            </button>
                        </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">عنوان</th>
                                    <th scope="col">وضعیت نمایش</th>
                                    <th scope="col">ناشر</th>
                                    <th scope="col">عکس</th>
                                    <th class="text-right" scope="col">عمل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$item->title}}</td>
                                        <td>
                                            @if($item->status == 1)
                                                نمایش بده
                                            @else
                                                نمایش نده
                                            @endif
                                        </td>

                                        <td>{{ $item->auther()->first()->name }}</td>
                                        <td><img src="{{asset($item->index_photo)}}" alt=""></td>


                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-light btn-floating btn-icon btn-sm"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{route('admin.blog.edit', $item->id)}}">
                                                        <button class="dropdown-item" type="button">
                                                            مشاهده و بروزرسانی
                                                        </button>
                                                    </a>

                                                    <button class="dropdown-item" type="button"
                                                            onclick="deleteRecordFromTable({{$item->id}})">حذف کردن
                                                    </button>

                                                    <form id="deleteRecordFrom_{{$item->id}}"
                                                          action="{{route('admin.blog.destroy', $item->id)}}"
                                                          method="post" style="display: none;">
                                                        @csrf
                                                        @method('delete')
                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection