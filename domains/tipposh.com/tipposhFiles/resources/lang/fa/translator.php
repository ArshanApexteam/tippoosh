<?php
return [

    //#############################################################
    #########################################   PERMISSIONS
    'dashboard_links' => 'مشاهده داشبرد پنل مدیریتی',

    'admins_links' => 'مشاهده لینک مدیران',
    'admins_create' => 'فرم ساخت مدیر جدید',
    'admins_store' => 'ساختن مدیر جدید',
    'admins_edit' => 'فرم ویرایش مدیران',
    'admins_update' => 'بروزرسانی مدیران',
    'admins_destroy' => 'حذف حساب کاربری مدیر',
    'admins_index' => 'مشاهده همه مدیران',

    'service_links' => 'مشاهده لینک خدمات',
    'service_create' => 'فرم ساخت خدمات جدید',
    'service_store' => 'ساختن خدمات جدید',
    'service_edit' => 'فرم ویرایش خدمات',
    'service_update' => 'بروزرسانی خدمات',
    'service_destroy' => 'حذف خدمات',
    'service_index' => 'مشاهده همه خدمات',



    'order_links' => 'مشاهده لینک سفارشات',
    'order_create' => 'فرم ساخت سفارش جدید',
    'order_store' => 'ساختن سفارش جدید',
    'order_edit' => 'فرم ویرایش سفارشات',
    'order_update' => 'بروزرسانی سفارشات',
    'order_destroy' => 'حذف  سفارش',
    'order_index' => 'مشاهده همه سفارشات',

    
    'productComment_links' => 'مشاهده لینک نظرات ',
    'productComment_create' => 'فرم ساخت نظر جدید',
    'productComment_store' => 'ساختن نظر جدید',
    'productComment_edit' => 'فرم ویرایش نظر',
    'productComment_update' => 'بروزرسانی نظر',
    'productComment_destroy' => 'حذف نظر',
    'productComment_index' => 'مشاهده همه نظرات',

    'menu_links' => 'مشاهده لینک منو',
    'menu_create' => 'فرم ساخت منو جدید',
    'menu_store' => 'ساختن منو جدید',
    'menu_edit' => 'فرم ویرایش منو',
    'menu_update' => 'بروزرسانی منو',
    'menu_destroy' => 'حذف منو',
    'menu_index' => 'مشاهده همه منو',

    'carousel_links' => 'مشاهده لینک اسلایدرها',
    'carousel_create' => 'فرم ساخت اسلایدر جدید',
    'carousel_store' => 'ساختن اسلایدر جدید',
    'carousel_edit' => 'فرم ویرایش اسلایدر',
    'carousel_update' => 'بروزرسانی اسلایدر',
    'carousel_destroy' => 'حذف اسلایدر',
    'carousel_index' => 'مشاهده همه اسلایدرها',

    'permission_links' => 'مشاهده لینک مجوز ها',
    'permission_create' => 'فرم ساخت مجوز جدید',
    'permission_store' => 'ساختن مجوز جدید',
    'permission_edit' => 'فرم ویرایش مجوز',
    'permission_update' => 'بروزرسانی مجوز',
    'permission_destroy' => 'حذف مجوز',
    'permission_index' => 'مشاهده همه مجوزها',

    'blog_links' => 'مشاهده لینک وبلاگ',
    'blog_create' => 'فرم ساخت وبلاگ جدید',
    'blog_store' => 'ساختن وبلاگ جدید',
    'blog_edit' => 'فرم ویرایش وبلاگ',
    'blog_update' => 'بروزرسانی وبلاگ',
    'blog_destroy' => 'حذف وبلاگ',
    'blog_index' => 'مشاهده همه وبلاگ ها',

    'brand_links' => 'مشاهده لینک برند ها',
    'brand_create' => 'فرم ساخت برند جدید',
    'brand_store' => 'ساختن برند جدید',
    'brand_edit' => 'فرم ویرایش برند',
    'brand_update' => 'بروزرسانی برند',
    'brand_destroy' => 'حذف برند',
    'brand_index' => 'مشاهده همه برند ها',

    'seo_links' => 'مشاهده لینک سئو ها',
    'seo_create' => 'فرم ساخت سئو جدید',
    'seo_store' => 'ساختن سئو جدید',
    'seo_edit' => 'فرم ویرایش سئو',
    'seo_update' => 'بروزرسانی سئو',
    'seo_destroy' => 'حذف سئو',
    'seo_index' => 'مشاهده همه سئو ها',

    'product_links' => 'مشاهده لینک محصولات',
    'product_create' => 'فرم ساخت محصول جدید',
    'product_store' => 'ساختن محصول جدید',
    'product_edit' => 'فرم ویرایش محصول',
    'product_update' => 'بروزرسانی محصول',
    'product_destroy' => 'حذف محصول',
    'product_index' => 'مشاهده همه محصولات',

    'category_links' => 'مشاهده لینک دسته بندی',
    'category_create' => 'فرم ساخت دسته بندی جدید',
    'category_store' => 'ساختن دسته بندی جدید',
    'category_edit' => 'فرم ویرایش دسته بندی',
    'category_update' => 'بروزرسانی دسته بندی',
    'category_destroy' => 'حذف دسته بندی',
    'category_index' => 'مشاهده همه دسته بندی ها',


    'setting_links' => 'مشاهده لینک تنظیمات',
    'setting_create' => 'فرم ساخت تنظیمات جدید',
    'setting_store' => 'ساختن تنظیمات جدید',
    'setting_edit' => 'فرم ویرایش تنظیمات',
    'setting_update' => 'بروزرسانی تنظیمات',
    'setting_destroy' => 'حذف تنظیمات',
    'setting_index' => 'مشاهده همه تنظیمات',


    'users_links' => 'مشاهده لینک کاربران',
    'users_create' => 'فرم ساخت کاربر جدید',
    'users_store' => 'ساختن کاربر جدید',
    'users_edit' => 'فرم ویرایش کاربران',
    'users_update' => 'بروزرسانی کاربران',
    'users_destroy' => 'حذف حساب کاربری کاربر',
    'users_index' => 'مشاهده همه کاربران',


    'notSecurityInputs' => 'به دلیل استفاده از داده مخرب نمی توانید ثبت نام کنید . لطفا با داده مطمئن ثبت نام کنید',
    'securityCode' => 'حاصل جمع عبارت امنیتی اشتباه است',


];
