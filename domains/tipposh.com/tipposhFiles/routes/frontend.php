<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('index');

Route::get('category/{slug}', 'FrontController@category')->name('category');
Route::post('category/{slug}', 'FrontController@category')->name('category');


Route::get('aboutUs', 'FrontController@aboutUs')->name('aboutUs');
Route::get('contactUs', 'FrontController@contactUs')->name('contactUs');
Route::get('help', 'FrontController@help')->name('help');
Route::get('order', 'FrontController@order')->name('order');

Route::get('userlogin', 'FrontController@userlogin')->name('user.login');
Route::get('userregisterForm', 'FrontController@userregisterForm')->name('user.registerForm');


Route::get('contact', 'FrontController@contact')->name('contact');
Route::get('userContactUs', 'FrontController@userContactUs');

Route::prefix('service')->name('service.')->group(function () {
    Route::get('/', 'ServiceController@services')->name('services');
    Route::get('/{slug}', 'ServiceController@details')->name('details');
});

Route::get('search', 'FrontController@search')->name('search'); // جستجو

Route::get('newsLetter', 'FrontController@newsLetter')->name('newsLetter');

Route::fallback(function () {
    return view('frontend.pages.404');
})->name('404');


Route::get('userServiceRequests', 'ServiceUserRequestController@add');


########### order
Route::prefix('order')->name('order.')->group(function () {
    Route::get('/{id}', 'OrderController@order')->name('new');
    Route::post('Save', 'OrderController@orderSave')->name('orderSave');
});

########### product
Route::prefix('product')->name('product.')->group(function () {
    Route::get('/{slug}', 'ProductController@show')->name('show');
    Route::get('all', 'ProductController@all')->name('all');
    Route::get('comment/Store', 'ProductController@commentStore')->name('commentStore');
});


########### Blog
Route::prefix('blog')->name('blog.')->group(function () {
    Route::get('/', 'BlogController@all')->name('all');
    Route::get('/{slug}', 'BlogController@details')->name('details');
    Route::get('commentStore', 'BlogController@commentStore')->name('commentStore');
    Route::get('category/{id}/{title}', 'BlogController@categoryPosts')->name('categoryPosts');
});


########### user
Route::prefix('user')->name('user.')->middleware('auth:web')->group(function () {
    Route::get('profile', 'UserController@profile')->name('profile');
    Route::post('profile', 'UserController@profileUpdate')->name('profileUpdate');
    Route::get('sale', 'UserController@sale')->name('sale');
    Route::get('basket', 'UserController@basket')->name('basket');
    Route::get('cash', 'UserController@cash')->name('cash');
    Route::get('likes', 'UserController@myLike')->name('likes');
    Route::get('requests', 'UserController@requests')->name('requests');
    Route::get('comments', 'UserController@comments')->name('comments');
    Route::get('removeComments', 'UserController@removeComments');
    Route::get('logout', 'UserController@logout')->name('logout');
    Route::get('password', 'UserController@password')->name('password');
    Route::post('password/update', 'UserController@passwordUpdate')->name('passwordUpdate');
});

Route::prefix('user')->name('user.')->middleware('guest')->group(function () {
    Route::post('loginTakePhone', 'UserController@loginTakePhone')->name('loginTakePhone');
    Route::post('login/VerifyCode', 'UserController@loginVerifyCode')->name('loginVerifyCode');
});


Route::prefix('user')->name('user.')->middleware('guest')->group(function () {
    Route::post('loginWithUsername', 'UserController@loginWithUsername')->name('loginWithUsername')->middleware('throttle:5,1');
});
Route::prefix('user')->name('user.')->group(function () {
    Route::get('like', 'LikeController@likeAndDislike');
});
### End user


########### basket
Route::prefix('basket')->name('basket.')->group(function () {
    Route::get('/', 'BasketController@index')->name('index');
    Route::get('all', 'BasketController@all');
    Route::get('add', 'BasketController@add');
    Route::post('add', 'BasketController@add')->name('addItem');
    Route::get('addOrRemove', 'BasketController@addOrRemove');
    Route::get('update', 'BasketController@update');
    Route::get('remove', 'BasketController@remove');
    Route::get('factor', 'BasketController@factor')->name('factor');
    Route::get('updateBasketAddress', 'BasketController@updateBasketAddress');
    Route::get('factorCompare', 'BasketController@factorCompare');
    Route::get('addDiscountCode', 'BasketController@addDiscountCode');
});
### End Product


Route::get('payment', 'PaymentController@payment')->name('payment');
Route::any('paymentCallback', 'PaymentController@paymentCallback')->name('paymentCallback');



########### Sitemap
Route::get('sitemap.xml','SitemapController@sitemap');
Route::get('sitemap-static.xml','SitemapController@statics');
Route::get('sitemap-category.xml','SitemapController@category');
Route::get('sitemap-post.xml','SitemapController@post');
Route::get('sitemap-product.xml','SitemapController@product');
### End Sitemap
