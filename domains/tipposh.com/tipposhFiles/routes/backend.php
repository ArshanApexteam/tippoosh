<?php

use Illuminate\Support\Facades\Route;

Route::name('admin.')->group(function () {
    Route::get('login', 'BackendController@login')->name('login');
    Route::post('authentication', 'BackendController@authentication')->name('authentication');

    Route::get('forgetPass', 'AdminController@forgetPassForm')->name('forgetPassForm');
    Route::post('forgetPass', 'AdminController@forgetPass')->name('forgetPass');

    Route::get('changePass/{token}/{email}', 'AdminController@changePassForm')->name('changePassForm');
    Route::post('changePass', 'AdminController@changePass')->name('changePass');


    Route::middleware('adminAuth')->group(function () {
        Route::get('profile', 'BackendController@profile')->name('profile');
        Route::put('profileUpdate', 'BackendController@profileUpdate')->name('profileUpdate');
        Route::post('logout', 'AdminController@logout')->name('logout');
    });
});

Route::name('admin.')->middleware('adminAuth')->group(function () {
    Route::get('/', 'BackendController@dashboard')->name('dashboard');


    //******    admins Rule
    Route::get('rule/{id?}', 'Ad_AdminRuleController@destroy')->name('rule.delete');

    Route::resource('category', 'CategoryController');
    Route::resource('carousel', 'CarouselController');
    Route::resource('service', 'ServiceController');
    Route::resource('setting', 'SettingController');
    Route::resource('menu', 'MenuController');
    Route::resource('brand', 'BrandController');
    Route::resource('admins', 'AdminController');
    Route::resource('users', 'UserController');
    Route::resource('newsletter', 'AdminController');
    Route::resource('userRequestService', 'ServiceUserRequestController');

    Route::resource('order', 'OrderController');
	Route::resource('productDiscount', 'ProductDiscountController');

    Route::get('sale', 'PaymentController@saleAll')->name('sale.index');


    //*************************/ Blog
    Route::prefix('blog')->name('blog.')->group(function () {
        Route::get('addTag', 'BlogController@addTag')->name('addTag');
        Route::post('addTag', 'BlogController@addTag')->name('addTag');
        Route::get('removeTag', 'BlogController@removeTag')->name('removeTag');
        Route::post('removeTag', 'BlogController@removeTag')->name('removeTag');
        Route::post('images', 'BlogController@images')->name('images');
        Route::get('removImage', 'BlogController@removImage')->name('removImage');
    });
    Route::resource('blog', 'BlogController');
    //************/

    //*************************/ Product
    Route::prefix('product')->name('product.')->group(function () {
        Route::get('addTag', 'ProductController@addTag')->name('addTag');
        Route::get('removeTag', 'ProductController@removeTag')->name('removeTag');
        Route::get('removeColor', 'ProductController@removeColor')->name('removeColor');
        Route::get('removImage', 'ProductController@removImage')->name('removImage');
    });
    Route::resource('product', 'ProductController');
    //************/

    //*************************/ productComment
    Route::prefix('comment')->name('comment.')->group(function () {
        Route::get('/', 'ProductCommentController@index')->name('index');
        Route::get('replay', 'ProductCommentController@replay')->name('replay');
        Route::get('userHistory/{id}', 'ProductCommentController@userHistory')->name('userHistory');
        Route::get('changeStatus/{id}', 'ProductCommentController@changeStatus')->name('changeStatus');
        Route::get('destroy/{id}', 'ProductCommentController@delete')->name('delete');
    });
    //************/
});
