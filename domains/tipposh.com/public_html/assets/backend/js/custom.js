'use strict';

(function ($) {


})(jQuery);

//    Delete Function In All Page
function deleteRecordFromTable(id) {
    swal({
        title: "آیا شما مطمئن هستید؟",
        text: "بعد از حذف دیگر قابلیت بازگردانی وجود ندارد.",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function(willDelete) {
        if (willDelete) document.getElementById("deleteRecordFrom_" + id).submit();
        else swal("حذف کنسل شد", { icon: "success" });
    });
}
