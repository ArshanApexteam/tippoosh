var curentURL = "";
$(document).ready(function() {
    /*
     *
     *
     *
     *                    مشخص کردن یو آر ال جاری
     */

    if ($("#curentSidebarMenu").attr("data-page") == "edit") {
        curentURL = "../";
    }

    activeRemoveMoreImage();
});

/*
 *
 *
 *
 *                    آنگولار کنترولر برای آپلود عکس
 */

app.controller("blogJsController", function($scope, $http, $timeout) {
    var self = this;

    $scope.uploadFile = function(files) {
        var iamgeLocation = window.location.host + "/assets/images/blog/";
        var data = new FormData();
        var imageId = $("#blog_images_id").val();

        //Take the first selected file
        data.append("file", files[0]);
        data.append("imageId", imageId);

        $http
            .post(curentURL + "images", data, {
                withCredentials: true,
                headers: { "Content-Type": undefined },
                transformRequest: angular.identity
            })
            .then(function(response) {
                if (response.data != "errorInSave") {
                    toastr["success"](" عکس با موفقیت ذخیره گردید. ");
                    $("#blog_images_id").val(response.data.blog_id);
                    $(".blogMoreImagesUrls").append(
                        "<tr><td>" +
                            iamgeLocation +
                            response.data.id +
                            ".jpg <span data-id=" +
                            response.data.id +
                            ">x</span></td></tr> "
                    );
                    activeRemoveMoreImage();
                }
            });
    };
});

/*
 *
 *
 *
 *                  تابع حذف عکس های بیشتر
 */
function activeRemoveMoreImage() {
    $(".blogMoreImagesUrls span").on("click", function() {
        var image_id = $(this).attr("data-id");
        var element = $(this);
        $.ajax({
            url: curentURL + "removImage",
            method: "get",
            data: { image_id: image_id },
            dataType: "json",
            success: function(response) {
                element.parents("tr").remove();
            }
        });
    });
}
