var myApp = new Vue({
    el: ".myVueApp",
    data: {
        userBasketCount: "",
        basketSalePrice: "",
        basketOfferPrice: "",
        basketTotalPrice: "" // هزیته نهایی
    },
    methods: {

        //#######################################################################################################
        // ###########################################     newsLetter
        newsLetter: function () {
            $.get("/.newsLetter", {
                mail: $('.newsLetterMali').val(),
            }).done(function (result) {
                if (result.status == 1) {
                    toastr["success"](result.msg);
                    $('.newsLetterMali').val('');
                }
            });
        },

        //#######################################################################################################
        // ###########################################     factor
        factor: function () {
            let myApp = this;
            $.get("./basket/factorCompare").done(function (result) {
                if (result.status == 1) {
                    myApp.basketSalePrice = result.salePrice;
                    myApp.basketOfferPrice = result.offerPrice;
                    myApp.basketTotalPrice = result.totalPrice;
                }
            });
        },


        //#######################################################################################################
        // ###########################################   addToBasket
        addToBasket: function (product_id) {
            let myApp = this;
            var count = document.getElementById("count").value;

            var color = 1;
            var size = $("#size").val();
            var colorElement = $(".colorElement");
            $.each(colorElement, function (index, item) {
                if ($(this).is(':checked'))
                    color = $(this).val();
            });

            $.get("./basket/add", {
                product_id: product_id,
                color: color,
                count: count,
                size: size
            }).done(function (result) {
                if (result.status == 1) {
                    window.open("./basket", "_self");
                } else
                    toastr["error"](result.error);

                myApp.userBasketCount = result.all;
            });
        },

        //#######################################################################################################
        // ###########################################   addOrRemoveFromBasket
        addOrRemoveFromBasket: function (product_id, event) {
            let myApp = this;
            $.get("./basket/addOrRemove", {
                product_id: product_id,
            }).done(function (result) {
                if (result.status == 1) {
                    window.open("./basket", "_self");
                    // $(event.target).toggleClass(" bg-danger");
                    // myApp.userBasketCount = result.all;
                }
            });
        },

        //#######################################################################################################
        // ###########################################   allBasketItems
        allBasketItems: function () {
            let myApp = this;
            $.get("./basket/all").done(
                function (result) {
                    if (result.status == 1) {
                        myApp.userBasketCount = result.count;
                    }
                }
            );
        },


        //#######################################################################################################
        // ###########################################   removeFromBasket
        removeFromBasket: function (basket_id, event) {
            let myApp = this;
            $.get("./basket/remove", {
                basket_id: basket_id
            }).done(function (result) {
                if (result.status == 1) {
                    toastr["success"](result.msg);
                    myApp.allBasketItems();
                    myApp.factor();
                    $(event.target).parents('tr').remove();
                }
            });
        },

        //#######################################################################################################
        // ###########################################  updateBasketItem
        updateBasketItem: function (id, event) {
            let myApp = this;
            $.get("./basket/update", {
                id: id,
                value: event.target.value,
            }).done(function (result) {
                if (result.status == 1) {
                    toastr["success"](result.msg);
                    myApp.factor();
                }
            });
        },


        //#######################################################################################################
        // ###########################################  addDiscountCode
        addDiscountCode: function (id, event) {
            let myApp = this;
            $.get("./basket/addDiscountCode", {
                id: id,
                code: event.target.value,
            }).done(function (result) {
                if (result.status == 1) {
                    toastr["success"](result.msg);
                    myApp.factor();
                }
                if (result.status == 2)
                    toastr["error"](result.msg);
            });
        },

        //#######################################################################################################
        // ###########################################  updateBasketAddress
        updateBasketAddress: function () {
            let myApp = this;
            $.get("./updateBasketAddress", {
                phone: $('#deliveryPhone').val(),
                address: $('#deliveryAddress').val(),
            }).done(function (result) {
                if (result.status == 1) {
                    //toastr["success"](result.msg);
                }
            });
        },


        //#######################################################################################################
        // ###########################################  like
        like: function (product_id, event) {
            let myApp = this;
            $.get("./user/like", {
                product_id: product_id
            }).done(function (result) {
                if (result.status == 1) {
                    $(event.target).addClass("font-weight-900");
                    toastr["success"](result.msg);
                }
                if (result.status == 0) {
                    $(event.target).removeClass("font-weight-900");
                    toastr["success"](result.msg);

                    $(event.target).parents("tr").remove();
                }
            });
        },


        //#######################################################################################################
        // ###########################################    removeComments
        removeComments: function (comment_id, event) {
            let myApp = this;
            $.get("./removeComments", {
                comment_id: comment_id
            }).done(function (result) {
                if (result.status == 1) {
                    toastr["success"](result.msg);
                    $(event.target).parents("tr").remove();
                }
            });
        },


        //#######################################################################################################
        // ###########################################    userContactUs
        userContactUs: function () {
            let myApp = this;
            if ($('#formContactUs .email').val() == '' || $('#formContactUs .msg').val() == '') {
                toastr["error"]("وارد کردن ایمیل و پیغام الزامی می باشد");
            } else {
                $.get("./userContactUs", {
                    user_name: $('#formContactUs .name').val(),
                    user_email: $('#formContactUs .email').val(),
                    user_phone: $('#formContactUs .phone').val(),
                    user_msg: $('#formContactUs .msg').val(),
                }).done(function (result) {
                    if (result.status == 1) {
                        toastr["success"](result.msg);
                        $('#formContactUs .name').val('');
                        $('#formContactUs .email').val('');
                        $('#formContactUs .phone').val('');
                        $('#formContactUs .msg').val('');
                    }
                    if (result.status == 0) {
                        toastr["success"](result.msg);
                    }
                });
            }
        },


        //#######################################################################################################
        // ###########################################
        userServiceRequests: function (service_id) {
            let myApp = this;
            if ($('.userServiceRequests #email').val() == '' || $('.userServiceRequests #summary').val() == '') {
                toastr["error"]("وارد کردن ایمیل و پیغام الزامی می باشد");
            } else {
                $.get("./userServiceRequests", {
                    name: $('.userServiceRequests #name').val(),
                    mail: $('.userServiceRequests #mail').val(),
                    phone: $('.userServiceRequests #phone').val(),
                    summary: $('.userServiceRequests #summary').val(),
                    service_id: service_id,
                }).done(function (result) {
                    if (result.status == 1) {
                        toastr["success"](result.msg);
                        $('.userServiceRequests #name').val('');
                        $('.userServiceRequests #mail').val('');
                        $('.userServiceRequests #phone').val('');
                        $('.userServiceRequests #summary').val('');
                    }
                    if (result.status == 0) {
                        toastr["success"](result.msg);
                    }
                });
            }
        },

    }
});
myApp.allBasketItems();
if ($("#factorUnit").length) myApp.factor();

