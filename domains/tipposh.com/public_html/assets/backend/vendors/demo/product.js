$(document).ready(function () {
    /*
     *
     *
     *
     *                    مشخص کردن یو آر ال جاری
     */
    var curentURL = "";
    if ($("#curentSidebarMenu").attr("data-page") == "edit") {
        curentURL = "../";
    }

    /*
     *
     *
     *
     *                   ساخت تگ برای وبلاگ با زدن دکمه اینتر
     */
    $("#productTag").keyup(function (e) {
        if (e.keyCode == 13) {
            var caption = $("#productTag").val();
            var product_id = $("#product_id").val();
            $.ajax({
                url: curentURL + "addTag",
                method: "get",
                data: {caption: caption, product_id: product_id},
                dataType: "json",
                success: function (response) {
                    if (response.status == true) {
                        $("#product_id").val(response.newTag.product_id);
                        $("#allProductTags").append(
                            '<b class="newTagAdd">' +
                            caption +
                            "<span data-id=" +
                            response.newTag.id +
                            ">x<span></b>"
                        );
                        $("#productTag").val("");
                        activeRemoveTag();
                    }
                }
            });
        }
    });

    /*
     *
     *
     *
     *                   حذف تگ
     */
    function activeRemoveTag() {
        $("#allProductTags .newTagAdd span").on("click", function () {
            var tag_id = $(this).attr("data-id");
            var element = $(this);
            $.ajax({
                url: curentURL + "removeTag",
                method: "get",
                data: {tag_id: tag_id},
                dataType: "json",
                success: function (response) {
                    element.parent().remove();
                }
            });
        });
    }

    /*
     *
     *
     *
     *                  فعال کردن تابع حذف
     */
    activeRemoveTag();

    /*
     *
     *
     *
     *                 برای فعال شده ادیتور سی کا
     */
    $(function () {
        CKEDITOR.replace("description");
    });

    /*
     *
     *
     *
     * چون اضافه کردن تگ با دکمه اینتر انجام میشود
     * و چون فرم ساخت وبلاگ جدید با اینتر ارسال میشود
     * پس نوع دکمه ارسال را باتون قرار داده و با این کد آن را ارسال میکنیم
     */
    $(".submit").on("click", function () {
        document.getElementById("addForm").submit();
    });

    /*
     *
     *
     *
     * جهت جلوگیری از ارسال فرم با دکمه اینتر
     * چون ساخت تگ با دکمه اینتر کار میکند
     */
    $("#addForm").on("submit", function () {
        return false;
    });


    $(".colorSection i").on('click', function () {
        var color_id = $(this).attr('data-id');
        var element = $(this);
        $.ajax({
            url: curentURL + "removeColor",
            method: "get",
            data: {color_id: color_id},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    element.prev().remove();
                }
            }
        });
    });

    $(".gallerySection i").on('click', function () {
        var photo_id = $(this).attr('data-id');
        var element = $(this);
        $.ajax({
            url: curentURL + "removImage",
            method: "get",
            data: {photo_id: photo_id},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    element.prev().remove();
                }
            }
        });
    });

});
