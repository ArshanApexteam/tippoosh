$(document).ready(function() {
    /*
     *
     *
     *
     *                    مشخص کردن یو آر ال جاری
     */
    var curentURL = "";
    if ($("#curentSidebarMenu").attr("data-page") == "edit") {
        curentURL = "../";
    }

    /*
     *
     *
     *
     *                   ساخت تگ برای وبلاگ با زدن دکمه اینتر
     */
    $("#blogTag").keyup(function(e) {
        if (e.keyCode == 13) {
            var caption = $("#blogTag").val();
            var blog_id = $("#blog_id").val();
            $.ajax({
                url: curentURL + "addTag",
                method: "get",
                data: { caption: caption, blog_id: blog_id },
                dataType: "json",
                success: function(response) {
                    if (response.status == true) {
                        $("#blog_id").val(response.newTag.blog_id);
                        $("#allBlogTags").append(
                            '<b class="newTagAdd">' +
                            caption +
                            "<span data-id=" +
                            response.newTag.id +
                            ">x<span></b>"
                        );
                        $("#blogTag").val("");
                        activeRemoveTag();
                    }
                }
            });
        }
    });

    /*
     *
     *
     *
     *                   حذف تگ
     */
    function activeRemoveTag() {
        $("#allBlogTags .newTagAdd span").on("click", function() {
            var tag_id = $(this).attr("data-id");
            var element = $(this);
            $.ajax({
                url: curentURL + "removeTag",
                method: "get",
                data: { tag_id: tag_id },
                dataType: "json",
                success: function(response) {
                    element.parent().remove();
                }
            });
        });
    }

    /*
     *
     *
     *
     *                  فعال کردن تابع حذف
     */
    activeRemoveTag();

    /*
     *
     *
     *
     *                 برای فعال شده ادیتور سی کا
     */
    $(function() {
        CKEDITOR.replace("description");
    });

    /*
     *
     *
     *
     * چون اضافه کردن تگ با دکمه اینتر انجام میشود
     * و چون فرم ساخت وبلاگ جدید با اینتر ارسال میشود
     * پس نوع دکمه ارسال را باتون قرار داده و با این کد آن را ارسال میکنیم
     */
    $(".submit").on("click", function() {
        document.getElementById("addForm").submit();
    });

    /*
     *
     *
     *
     * جهت جلوگیری از ارسال فرم با دکمه اینتر
     * چون ساخت تگ با دکمه اینتر کار میکند
     */
    $("#addForm").on("submit", function() {
        return false;
    });

});
