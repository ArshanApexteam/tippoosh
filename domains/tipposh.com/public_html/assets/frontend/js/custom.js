
(function($){
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	  if (!$(this).next().hasClass('show')) {
		$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');

	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
		$('.dropdown-submenu .show').removeClass("show");
	  });

	  return false;
	});
})(jQuery)
jQuery(document).ready(function($) {
    var mainSlider = $('#mainSlider .owl-carousel');
    mainSlider.owlCarousel({
        dots:true,
        loop: true,
        nav: true,
        lazyLoad: true,
        margin: 5,
        video: true,
        responsiveClass: true,
        autoplay:true,
        autoplayTimeout:60000,
        responsive: {
            0: {
                items: 1
            },
            540: {
                items: 1
            },
            769: {
                items: 1
            },
            992: {
                items: 1
            },
            1100: {
                items: 1
            }
        }
    });
    
    var blog = $('.owl-carousel-blog');
    blog.owlCarousel({
        dots:false,
        loop: true,
        nav: true,
        lazyLoad: true,
        margin: 5,
        video: true,
        responsiveClass: true,
        autoplay:false,
        autoplayTimeout:6000,
        responsive: {
            0: {
                items: 1
            },
            540: {
                items: 2
            },
            769: {
                items: 3
            },
            992: {
                items: 4
            },
            1100: {
                items: 5
            }
        }
    });
    
    var owlSale = $('#mostSaleProduct .owl-carousel');
    owlSale.owlCarousel({
        dots:false,
        loop: true,
        nav: true,
        lazyLoad: true,
        margin: 5,
        video: true,
        responsiveClass: true,
        autoplay:true,
        autoplayTimeout:6000,
        responsive: {
            0: {
                items: 1
            },
            540: {
                items: 2
            },
            769: {
                items: 3
            },
            992: {
                items: 4
            },
            1100: {
                items: 4
            }
        }
    });


    $('.owl-next').html("<i class='fa fa-angle-right'></i>")
    $('.owl-prev').html("<i class='fa fa-angle-left'></i>")


    $('#mainSlider .owl-carousel .owl-next').html("<i class='fa fa-angle-up'></i>")
    $('#mainSlider .owl-carousel .owl-prev').html("<i class='fa  fa-angle-down'></i>")
    $('#mainSlider .owl-carousel .owl-dots .owl-dot span').html("<i class='fa  fa-circle'></i>")

});
