(function ($) {
    'use strict';

    /*----------------------------------------*/
    /*  Category Menu
/*----------------------------------------*/
    $('.rx-parent').on('click', function () {
        $('.rx-child').slideToggle();
        $(this).toggleClass('rx-change');
    });
    //    category heading
    $('.category-heading').on('click', function () {
        $('.category-menu-list').slideToggle(900);
        $('.menu-expand').removeClass('active');
    });
    /*-- Category Menu Toggles --*/
    function categorySubMenuToggle() {
        var screenSize = $(window).width();
        if (screenSize <= 991) {
            $('#cate-toggle .right-menu > a').prepend('<i class="expand menu-expand"></i>');
            $('.category-menu .right-menu ul').slideUp();
        } else {
            $('.category-menu .right-menu > a i').remove();
            $('.category-menu .right-menu ul').slideDown();
        }
    }
    categorySubMenuToggle();
    $(window).resize(categorySubMenuToggle);
    /*-- Category Sub Menu --*/
    function categoryMenuHide() {
        var screenSize = $(window).width();
        if (screenSize <= 991) {
            $('.category-menu-list').hide();
        } else {
            $('.category-menu-list').show();
        }
    }
    categoryMenuHide();
    // $(window).resize(categoryMenuHide);
    $('.category-menu-hidden').find('.category-menu-list').hide();
    $('.category-menu-list').on('click', 'li a, li a .menu-expand', function (e) {
        var $a = $(this).hasClass('menu-expand') ? $(this).parent() : $(this);
        $(this).toggleClass('active');
        if ($a.parent().hasClass('right-menu')) {
            if ($a.attr('href') === '#' || $(this).hasClass('menu-expand')) {
                if ($a.siblings('ul:visible').length > 0) $a.siblings('ul').slideUp();
                else {
                    $(this).parents('li').siblings('li').find('ul:visible').slideUp();
                    $a.siblings('ul').slideDown();
                }
            }
        }
        if ($(this).hasClass('menu-expand') || $a.attr('href') === '#') {
            e.preventDefault();
            return false;
        }
    });

    /*----------------------------------------*/
    /*  Nice Select
/*----------------------------------------*/
    $(document).ready(function () {
        $('.nice-select').niceSelect();
    });
    /*----------------------------------------*/


    /*----------------------------------------*/
    /*  Umino's Scroll To Top
    /*----------------------------------------*/
    // $.scrollUp({
    //     scrollText: '<i class="fa fa-chevron-up"></i>',
    //     easingType: 'linear',
    //     scrollSpeed: 900
    // });



    /*----------------------------------------*/
    /*  Sidebar Categories Menu Activation
/*----------------------------------------*/
    $('.sidebar-categories_menu li.has-sub > a').on('click', function () {
        $(this).removeAttr('href');
        var element = $(this).parent('li');
        if (element.hasClass('open')) {
            element.removeClass('open');
            element.find('li').removeClass('open');
            element.find('ul').slideUp();
        } else {
            element.addClass('open');
            element.children('ul').slideDown();
            element.siblings('li').children('ul').slideUp();
            element.siblings('li').removeClass('open');
            element.siblings('li').find('li').removeClass('open');
            element.siblings('li').find('ul').slideUp();
        }
    });






})(jQuery);


var acc = document.getElementsByClassName("accordion-label-section");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("accordion-active");
        var panel = this.nextElementSibling;
        panel.classList.toggle("accordion-panel-open");
//     if (panel.style.maxHeight) {
        //  panel.style.maxHeight = null;
//     } else {
        //  panel.style.maxHeight = panel.scrollHeight + "px";
//     }
    });
}

function listFiltterFunction() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValueFa , txtValueEn;
    input = document.getElementsByClassName('fillterListInput')[0];
    filter = input.value.toUpperCase();
    ul = document.getElementsByClassName("companyCheckList")[0];
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        spanFa = li[i].getElementsByTagName("span")[0];
        txtValueFa = spanFa.textContent || spanFa.innerText;
        if (txtValueFa.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }

        spanEn = li[i].getElementsByTagName("span")[1];
        txtValueEn = spanEn.textContent || spanEn.innerText;
        if (txtValueEn.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}