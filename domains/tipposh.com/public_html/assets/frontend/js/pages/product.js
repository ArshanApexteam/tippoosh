(function ($) {
    'use strict';

    $(document).ready(function () {
        toastr.options = {
            closeButton: false,
            debug: false,
            newestOnTop: false,
            progressBar: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            showDuration: "300",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
        };
    });

    $(document).ready(function () {
        setTimeout(function () {
            /*----------------------------------------*/
            /*  Single Product Image Slider
            /*----------------------------------------*/
            $('.sp-img_slider').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                prevArrow: '<button class="slick-prev"><i class="fa fa-angle-left"></i></button>',
                nextArrow: '<button class="slick-next"><i class="fa fa-angle-right"></i></button>',
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.slick-track img').on('click' , function () {
                $(this).parent('a').addClass('active').siblings('a.active').removeClass('active');
            })
        }, 1000);


        setTimeout(function () {
            $('#gallery a.slick-slide').on('click', function () {
                $('.zoompro-border img.zoompro').attr('src', $(this).attr('ata-image'));
            });
        }, 3000);

    });

    $('.dec.qtybutton').on('click', function () {
        var dec = $(this).siblings('input').val();
        if (dec > 1)
            $(this).siblings('input').val(dec - 1);
    });
    $('.inc.qtybutton').on('click', function () {
        var inc = parseInt($(this).siblings('input').val()) + 1;
        $(this).siblings('input').val(inc)
    });


    
    /*----------------------------------------*/
    /*  Cart Plus Minus Button
    /*----------------------------------------*/
    $('.cart-plus-minus').append(
        '<div class="dec qtybutton"><i class="fa fa-minus"></i></div>' +
        '<div class="inc qtybutton"><i class="fa fa-plus"></i></div>'
    );
    $('.qtybutton').on('click', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }
        $button.parent().find('input').val(newVal);
    });


    /*--------------------------
    rating
    ---------------------------- */
    var $star_rating_1 = $('.star-rating .rating_1');
    var SetRatingStar_1 = function() {
        return $star_rating_1.each(function() {
            if (parseInt($star_rating_1.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_1.on('click', function() {
        $star_rating_1.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_1();
    });
    SetRatingStar_1();



    var $star_rating_2 = $('.star-rating .rating_2');
    var SetRatingStar_2 = function() {
        return $star_rating_2.each(function() {
            if (parseInt($star_rating_2.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_2.on('click', function() {
        $star_rating_2.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_2();
    });
    SetRatingStar_2();



    var $star_rating_3 = $('.star-rating .rating_3');
    var SetRatingStar_3 = function() {
        return $star_rating_3.each(function() {
            if (parseInt($star_rating_3.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_3.on('click', function() {
        $star_rating_3.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_3();
    });
    SetRatingStar_3();


    var $star_rating_4 = $('.star-rating .rating_4');
    var SetRatingStar_4 = function() {
        return $star_rating_4.each(function() {
            if (parseInt($star_rating_4.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_4.on('click', function() {
        $star_rating_4.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_4();
    });
    SetRatingStar_4();


    var $star_rating_5 = $('.star-rating .rating_5');
    var SetRatingStar_5 = function() {
        return $star_rating_5.each(function() {
            if (parseInt($star_rating_5.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_5.on('click', function() {
        $star_rating_5.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_5();
    });
    SetRatingStar_5();


    var $star_rating_6 = $('.star-rating .rating_6');
    var SetRatingStar_6 = function() {
        return $star_rating_6.each(function() {
            if (parseInt($star_rating_6.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('font-width-normal').addClass('fa-star');
            } else {
                return $(this).addClass('font-width-normal');
            }
        });
    };

    $star_rating_6.on('click', function() {
        $star_rating_6.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar_6();
    });
    SetRatingStar_6();
    

})(jQuery);